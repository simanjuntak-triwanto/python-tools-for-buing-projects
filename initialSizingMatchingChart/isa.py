import scipy as sp
import numpy as np


from inputReq import *


def isaModel1(alt1):
    """
    This function is to estimate the density at altitude 0 - 11,000 m,  The condition is assumed isentropic.
    alt is in meter.
    a is the temperature gradient is Kelvin/meter.
    TH is the temperature at certain altitude, alt.
    TSL, pSL are the reference temperature and pressure at sea-level, respectively.
    R is the specific gas constant (air).
    The output is the density at alt.
    """

    a = -0.0065
    TH1 = a * alt1 + TSL

    pH1 = pSL * (TH1 / TSL) ** (-g / (a * R))

    rhoH1 = pH1 / (R * TH1)

    return rhoH1, TH1, pH1


def isaModel2(alt2):
    """
    This function is to estimate the density at altitude 11,000 - 25,000 m,  The condition is assumed isentropic.
    alt is in meter.
    a is the temperature gradient is Kelvin/meter.
    TH is the temperature at certain altitude, alt.
    TSL, pSL are the reference temperature and pressure at sea-level, respectively.
    R is the specific gas constant (air).
    The output is the density at alt.
    """
    rhoH1, TH1, pH1 = isaModel1(11000)
    TH2 = TH1
    deltaH = alt2 - 11000

    pH2 = pH1 * np.exp(-(g / (R * TH2)) * deltaH)
    rhoH2 = rhoH1 * np.exp(-(g / (R * TH2)) * deltaH)

    return rhoH2, TH2, pH2
