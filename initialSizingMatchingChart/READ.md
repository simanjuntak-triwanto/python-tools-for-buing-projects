"grossMTOMJet.py" is to calculate to maximum take-off mass for corresponding mission profile using fsolve , for various W/S and T/W. 
The output is "massEst.csv" which is required to run the matplotlib command in " /matchingChart-contourMTOM/matchPlot.py".

The code has a dependency files:
1. inputReq.py is the python code inputs for all the constant variables to estimate the MTOM.
2. isa.py is the the function for ISA model up to altitude 25,000 m
3. missionJet.py is the function to estimate weight fractions for defined mission profile.
4. prandtLift.py is the function to calculate aerodynamics parameters for the corresponding wing shape and area using Prandtl Lifting Line theory.
5. aeroEst.py is the function for estimating the aerodynamics characteristics (CL and CL) at cruise based on prandtLift.py
