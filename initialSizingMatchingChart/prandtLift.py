import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import pandas as pd
from scipy.optimize import fsolve
import scipy.linalg as la


from inputReq import *


# This class is to calculate the CL for certain angle of attack


def chordEst(phii, taperRatio, cr):
    """
    Estimate the chord length of each station along the span (ci) by transformation to polar
    coordinate.
    phii in degrees.
    cr is meter or ft.
    """
    ct = cr * taperRatio
    ci = ct * np.cos(np.radians(phii)) + cr * (1 - np.cos(np.radians(phii)))

    return ci


def miuEst(cla, ar, S, phii, taperRatio, cr):
    """
    This function is to estimate the miu constant for lift distribution at each span station.
    ci is chord length at spanwise location in ft or meter
    S is wing planform area
    """

    ci = chordEst(phii, taperRatio, cr)
    b = np.sqrt(ar * S)
    miui = ci * cla / (4 * b)

    return miui


def matrixBi(cla, ar, S, phii, taperRatio, cr, alpha, alphaZL):
    """
    This function is to estimate the component of matrix B of AX= B
    """

    miui = miuEst(cla, ar, S, phii, taperRatio, cr)
    alpharad = np.radians(alpha)
    alphaZLrad = np.radians(alphaZL)

    Bi = miui * (alpharad - alphaZLrad) * np.sin(np.radians(phii))

    return Bi


def matrixAij(cla, ar, S, phii, taperRatio, cr, N):
    """
    This function is to calculate the matrix component of A.
    The same variable with the function estimating miui.
    N is the number of panels to be evaluated,
    """

    nn = np.arange(1, N, 2)

    miui = miuEst(cla, ar, S, phii, taperRatio, cr)
    phii = np.radians(phii)

    Aij = []
    for ii in range(len(nn)):
        aij = np.sin(nn[ii] * phii) * ((nn[ii] * miui) + np.sin(phii))
        Aij.append(aij)

    return Aij


def aeroAnalysis(phi1, phi2, N, taperRatio, ar, cla, cr, S, alpha, alphaZL, sweptHalf):
    """
    This function is to calculate the coefficient of aerodynamics for one  angle of attack
    for symmetric flight.
    phi1 and phi2 are the spanwise location ar polar coordinate (in degree). Tip-to tip is
    from 0 - 180 deg. The calculation is only half ( 0-90 deg ),
    N is the number of panels to be calculated ( for symmetric flight is odd number )
    taper ratio id from 0-1
    cla is dcl/d(alpha) is airfoil characteristic in 1/radian
    cr is the chord length at the root.
    S is the wing planform area.
    alpha is absolute angle of attack in degree.
    alphaZL is alpha at cl=0 in degree
    """

    panel = np.arange(1, N, 2)
    phii = np.linspace(phi1, phi2, len(panel))

    Bi = []
    Aij = []
    for pp in range(len(phii)):
        bi = matrixBi(cla, ar, S, phii[pp], taperRatio, cr, alpha, alphaZL)
        aij = matrixAij(cla, ar, S, phii[pp], taperRatio, cr, N)
        Bi.append(bi)
        Aij.append(aij)

    AA = np.reshape(Aij, (len(phii), len(panel)))
    BB = np.reshape(Bi, (len(phii), 1))
    consA = la.solve(AA, BB)

    deltaI = []
    for nn in range(len(panel)):
        deltai = panel[nn] * (consA[nn] / consA[0]) ** 2
        deltaI.append(deltai)

    inducedCoeff = np.sum(deltaI) - deltaI[0]
    CL = np.pi * ar * consA[0]
    e = 1 / (1 + inducedCoeff)
    CDI = CL**2 / (np.pi * ar * e)
    Cla = CL / (np.radians(alpha) - np.radians(alphaZL))

    # This is the correction of CLa for swept wing based on "Fundamental of Aerodynamics book"
    num0 = Cla * np.cos(np.radians(sweptHalf)) / (np.pi * ar)
    CLa = Cla * np.cos(np.radians(sweptHalf)) / (np.sqrt(1 + num0**2) + num0)
    # LiftCoeff = CLa * (np.radians(alpha) - np.radians(alphaZL))
    # CDI = LiftCoeff**2 / (np.pi * ar * e)

    # print(panel, consA)
    return CL, e, CDI, CLa, inducedCoeff  # LiftCoeff, e, CDI, CLa, inducedCoeff  #


# def alphaCruise(Alp):
#     """
#     This function is to estimate the angleof attack ar CL_cruise,
#     All the variable is given except of Alp.
#     Alp is the initial guess
#     """

#     CLa = aeroAnalysis(PHI1, PHI2, NN, taper_Ratio, aR, Cla, Cr, S, Alp, AlphaZL)[3]
#     AlphaEst = (clc / CLa) + np.radians(AlphaZL)
#     deltaAlpha = np.abs(Alp - np.degrees(AlphaEst))

#     return deltaAlpha


# aCruise = fsolve(alphaCruise, 1)
