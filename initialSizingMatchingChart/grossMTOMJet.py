import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import pandas as pd
from scipy.optimize import fsolve
from scipy.optimize import minimize

from isa import isaModel1, isaModel2
from aeroEst import clCruiseEst, cdCruiseEst
import prandtLift as pl
import missionJet as mest
from inputReq import *


def estimateMTOMfunc(
    mtom,
    tTaxi,
    tTO,
    sfc,
    twRatio,
    roc0,
    roc1,
    rod,
    vc,
    r0,
    r1,
    e0,
    e1,
    S,
    ar,
    taperRatio,
    alt0,
    alt1,
    alt2,
    alt3,
    tc,
    cla,
    cr,
    alphaZL,
    sweptHalf,
):
    """
    This function to estimate the mtom in kg
    """
    # sigma = isaModel1(alt0)[0] / rhoSL
    # vcTAS = vc / np.sqrt(sigma)

    vcTAS = vc
    clc = clCruiseEst(mtom, S, vcTAS, alt0)
    cdc = cdCruiseEst(
        S, ar, taperRatio, alt0, tc, cla, cr, alphaZL, mtom, vcTAS, sweptHalf
    )
    clcdCruise = clc / cdc

    a = mest.fracEmpty(mtom)

    wfracWeight = mest.fracWeight(
        tTaxi,
        tTO,
        sfc,
        twRatio,
        roc0,
        roc1,
        rod,
        clcdCruise,
        vc,
        r0,
        r1,
        e0,
        e1,
        hC0,
        hC1,
        hC2,
        hEnd,
    )

    dmtom = a + (MPayload / mtom) - wfracWeight
    print(clcdCruise, clc, cdc, dmtom, wfracWeight)
    return dmtom


def solveMTOM(
    x,
    tTaxi,
    tTO,
    sfc,
    twRatio,
    roc0,
    roc1,
    rod,
    vc,
    r0,
    r1,
    e0,
    e1,
    S,
    ar,
    taperRatio,
    alt0,
    alt1,
    alt2,
    alt3,
    tc,
    cla,
    cr,
    alphaZL,
    sweptHalf,
):
    """
    This function is to solve the MTOM
    """
    mtom = x
    MTOMgross = fsolve(
        estimateMTOMfunc,
        x0=x,
        args=(
            tTaxi,
            tTO,
            sfc,
            twRatio,
            roc0,
            roc1,
            rod,
            vc,
            r0,
            r1,
            e0,
            e1,
            S,
            ar,
            taperRatio,
            alt0,
            alt1,
            alt2,
            alt3,
            tc,
            cla,
            cr,
            alphaZL,
            sweptHalf,
        ),
    )

    return MTOMgross


tTaxi = t_Taxi
tTO = t_TO
sfc = SFC
roc0 = ROC0
roc1 = ROC1
rod = ROD
vc = vC
r0 = dMax
r1 = dLoit
e0 = E0
e1 = E1
ar = aR
taperRatio = taper_Ratio
alt0 = hC0
alt1 = hC1
alt2 = hC2
alt3 = hEnd
tc = airfoil_TC
cla = Cla
cr = Cr
alphaZL = AlphaZL
sweptHalf = SweptHalf


twratio_MAX = np.arange(0.01, 0.43, 0.02)  # np.arange(0.01, 0.4, 0.005)
area = np.arange(300, 1000, 20)  # np.arange(10, 30, 0.1)  # np.array([10, 25])
x = 10000.0

SS = []
TW = []
MTOMG = []
for kk in range(len(twratio_MAX)):
    for ii in range(len(area)):
        MTOM_Gross = solveMTOM(
            x,
            tTaxi,
            tTO,
            sfc,
            twratio_MAX[kk],
            roc0,
            roc1,
            rod,
            vc,
            r0,
            r1,
            e0,
            e1,
            area[ii],
            ar,
            taperRatio,
            alt0,
            alt1,
            alt2,
            alt3,
            tc,
            cla,
            cr,
            alphaZL,
            sweptHalf,
        )
        print(area[ii], twratio_MAX[kk], MTOM_Gross)
        SS.append(area[ii])
        TW.append(twratio_MAX[kk])
        MTOMG.append(MTOM_Gross)

a = np.reshape(SS, (len(SS), 1))
b = np.reshape(TW, (len(TW), 1))
c = np.reshape(MTOMG, (-1, 1))

out = np.hstack((a, b, c))
DF = pd.DataFrame(out)
DF.to_csv("massEst.csv")


# MTOM_Gross = solveMTOM(
#     10000,
#     tTaxi,
#     tTO,
#     sfc,
#     0.22,
#     roc0,
#     roc1,
#     rod,
#     vc,
#     r0,
#     r1,
#     e0,
#     e1,
#     580,
#     ar,
#     taperRatio,
#     alt0,
#     alt1,
#     alt2,
#     alt3,
#     tc,
#     cla,
#     cr,
#     alphaZL,
#     sweptHalf,
# )
