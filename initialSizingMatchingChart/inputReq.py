#!/usr/bin/env python3
import scipy as sp
import numpy as np
from matplotlib import rc
import pylab as pb
import numpy.ma as ma
import os
import pandas as pd


# ------------------Thermophysical properties-----------------------------#
g = 9.80665  # in m/s^2
gImp = 32.174
R = 287
rhoSL = 1.225  # density at sea level
pSL = 101325.0
TSL = 288.16
gamma = 1.4
viscousConst = 1.789e-5

# --------------------Data Requirements-----------------------------------------------------#

# C5 TO run is 2530 at SL, and 2987 m TO to 50 ft
# Ground Run for MTOM in meter
sG = 2743.2  # 2530

# C5 Landing Dist from 50 ft is 1164  m for MLW, and 725 m
# Landing distance
lDist = 2300  # 725  # 2743.2  # 417.2712

# Cruise Speed in m/s
vC = 272  # 70 knots
M = 0.8
vCMAX = 1.026 * vC

# C5 stall speed(40 deg fla, power off) is 193 km/h = 53.6 m/s
# Stall speed in m/s
vS = 65.0  #

# Turn speed at cruise altitude
vTurn = 272
bankAngle = 30.0
nTurn = 1 / np.cos(np.radians(bankAngle))

# Landing speed. usuallu less than takeoff speed
vLand = 58  # 20.5778

# Service ceiling in m
hS = 13110  # 8000

# Cruise altitude in m
hC0 = 9450
hC1 = 1524
hC2 = 6096
hEnd = 0

# Aspect ratio


# Drag minimum without HLD
cdMin = 0.025  # 0.0333


# Configuration for take-off, with flap extended
miu = 0.04
clTO = 0.8  # 0.5
cdTO = 0.035  # 0.038


# Configuration for landing  with flap extended
tau = 1.0
clLdg = 0.7
cdLdg = 0.07
miuLdg = 0.3
TgrToW = 0.0
hobs = 50.0  # 50  # 15.24  # equivalent to 50 ft


# Rate of climb in m/s
# LOCKHEED GALAXY, max ROC: 525 m/min at SL
ROC0 = 8.8  #
ROC1 = 9  # 8

# Rate of descent in m/s
ROD = -10  # -10  # -10

# Wing Loading during takeoff and landing in Newton/m^2
# Maximum wing loading is 6465 N/m^2
clMaxTO = 2.5
clMaxLand = 2.7


SFC = 0.342  # unit 1/hour, engine CF6-80C2


#
# Range in meter
dMax = 2500 * 1852  # 5000 * 1852  #
dLoit = 200 * 1852


# Airfoil and Wing parameters
aR = 8  # 9.0
Cla = 2 * np.pi / (np.sqrt(1 - M**2))
airfoil_TC = 0.12
SweptLE = 25.0  # sweep angle
SweptHalf = 22.0
taper_Ratio = 0.337
Cr = 15 #15  # For C5-M is 13.85 m
NN = 15
AlphaZL = 0.0  # -2.0  # -2.7
PHI1 = 2
PHI2 = 90
cdx = 0.025
NZ = 6


# Data Required for MTOM gross estimation using mission profile
t_Taxi = 20 / 60
t_TO = 1 / 60
E0 = 5 / 60
E1 = 30 / 60
MPayload = 146810 #195044.719  # 213600   133810.0  
