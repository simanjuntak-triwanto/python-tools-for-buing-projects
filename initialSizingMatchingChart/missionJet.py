import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import pandas as pd
from scipy.optimize import fsolve
from isa import isaModel1, isaModel2


from inputReq import *


def missionWeightRatioTO(tTaxi, tTMax, sfc, twRatio):
    """
    This function is to estimate the weight ratio for engine startup, taxi, and take-off mission
    tTaxi, tTmax is in hours.
    sfc in 1/hr.
    """
    rIdleRatio = 0.1 * twRatio
    rMaxRatio = 1 * twRatio
    ratioWeightTO = 1 - (tTaxi * rIdleRatio + tTMax * rMaxRatio) * sfc

    return ratioWeightTO


def missionWeightRatioClimb(hInit, hEnd, roc, sfc, twRatio):
    """
    This function is to estimate the weight ratio during the initial climb and end climb.

    """
    rClimb = 1 * twRatio
    ratioWeightClimb = 1 - ((hEnd - hInit) * sfc * rClimb / (roc * 3600))

    return ratioWeightClimb


def missionWeightRatioClimb1(hInit, hEnd, roc, sfc, twRatio):
    """
    This function is to estimate the weight ratio during the initial climb and end climb.

    """
    rClimb = 1.6 * twRatio
    ratioWeightClimb = 1 - ((hEnd - hInit) * sfc * rClimb / (roc * 3600))

    return ratioWeightClimb


def missionWeightRatioCruise(r, sfc, vc, ldRatioCruise):
    """
    This function is to estimate the weight ratio during the cruise at certain range.
    sfc in 1/hr
    r is range in meter

    """
    ratioWeightCruise = np.exp(-r * sfc / (3600 * vc * ldRatioCruise))

    return ratioWeightCruise


def missionWeightRatioDescent(hInit, hEnd, rod, sfc, twRatio):
    """
    This function is to estimate the weight ratio during descent from cruising altitude.

    """
    rDescent = 1 / 3 * twRatio
    ratioWeightDescent = 1 - ((hEnd - hInit) * sfc * rDescent / (rod * 3600))

    return ratioWeightDescent


def missionWeightRatioLoiter(e, sfc, ldRatioCruise):
    """
    This function is to estimate the weight ratio during a loitering (objective mission)
    about 30 minutes
    """
    ldRatioLoiter = 0.71428 * ldRatioCruise

    ratioWeightLoiter = np.exp(-e * sfc / (ldRatioLoiter))

    return ratioWeightLoiter


# ----------------------------------------------------------------------------
def fracEmpty(mtom):
    """
    This function is empirical equation to estimate empty mass from maximum take-off mass in kg
    Based on equation 6-24 (Gunmundsson) for business jets 
    """

    fracRatioEmpty = 0.8788 - 0.03163 * np.log(mtom)
    
    
    #Based on Raymer (Table 3.1), less mass
    #fracRatioEmpty = 0.88 * (mtom* g)**(-0.07)
 
    ## The below equation is from statistical data of benchmark aircraft.
    #fracRatioEmpty = 0.465

    ## The below equation is from statistical data of benchmark aircraft.
    # emtom = 241 * np.exp(9.5e-4 * mtom)
    # fracRatioEmpty = emtom / mtom

    return fracRatioEmpty


def fracWeight(
    tTaxi,
    tTO,
    sfc,
    twRatio,
    roc0,
    roc1,
    rod,
    ldRatioCruise,
    vc,
    r0,
    r1,
    e0,
    e1,
    hC0,
    hC1,
    hC2,
    hEnd,
):
    """
    This function is to estimate the fuel fraction for whole mission
    """
    # rIdleRatio = 0.03
    # rMaxRatio = 0.3
    # rClimb0 = 0.3
    # rDescent = 0.1
    # rClimb1 = 0.5

    w01 = missionWeightRatioTO(tTaxi, tTO, sfc, twRatio)
    w12 = missionWeightRatioClimb(0, hC0, roc0, sfc, twRatio)
    w23 = missionWeightRatioCruise(r0, sfc, vc, ldRatioCruise)
    w34 = missionWeightRatioDescent(hC0, hC1, rod, sfc, twRatio)
    w45 = missionWeightRatioLoiter(e0, sfc, ldRatioCruise)
    w56 = missionWeightRatioClimb1(hC1, hC2, roc1, sfc, twRatio)
    w67 = missionWeightRatioCruise(r1, sfc, 0.881 * vc, ldRatioCruise)
    w78 = missionWeightRatioDescent(hC2, hEnd, rod, sfc, twRatio)
    w89 = missionWeightRatioLoiter(e1, sfc, 1.4 * ldRatioCruise)
    w90 = 1

    wfracW = w01 * w12 * w23 * w34 * w45 * w56 * w67 * w78 * w89 * w90

    return wfracW


# def estimateMTOM(mtom):
#     """
#     This function to estimate the mtom in kg
#     """

#     a = fracEmpty(mtom)
#     wfracWeight = fracWeight(
#         t_Taxi,
#         t_TO,
#         pwRatio_MAX,
#         SFC,
#         ROC,
#         ROD,
#         miuP_cruise,
#         ldRatio_Cruise,
#         vC,
#         r,
#         e,
#         E,
#     )
#     deltaMtom = a + (MPayload / mtom) - wfracWeight

#     return deltaMtom


# MTOM = fsolve(estimateMTOM, 100)
# print(MTOM)


# def fracWeight():
#     """
#     This function is to estimate the fuel fraction for whole mission
#     """

#     w01 = missionWeightRatioTO(t_Taxi, t_TO, pwRatio, sfc0)
#     w12 = missionWeightRatioClimb(hobs * 0.3048, hC, pwRatio, roc, sfc0)
#     w23 = missionWeightRatioCruise(r, sfc0, miuP_cruise, ldRatio_Cruise)
#     w34 = missionWeightRatioLoiter(E, sfc0, 0.7 * vC, miuP_cruise, ldRatio_Cruise)
#     w45 = missionWeightRatioCruise(r, sfc0, miuP_cruise, ldRatio_Cruise)
#     w56 = missionWeightRatioDescent(hC2, 1000, pwRatio, rod, sfc0)
#     w67 = missionWeightRatioLoiter(e, sfc0, 0.8 * vC, miuP_cruise, ldRatio_Cruise)
#     w78 = 1  # mission weight due to landing

#     wfracW = w01 * w12 * w23 * w34 * w45 * w56 * w67 * w78
#     return wfracW


# def estimateMTOM(mtom):
#     """
#     This function to estimate the mtom in kg
#     """

#     a = fracEmpty(mtom)
#     wfracWeight = fracWeight()
#     deltaMtom = a + (MPayload / mtom) - wfracWeight

#     return deltaMtom
