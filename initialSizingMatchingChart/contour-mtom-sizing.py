#!/usr/bin/env python3
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import rc
import pylab as pb
import numpy.ma as ma
import matplotlib.tri as tri

import scipy.ndimage as ndimage


rc("text", usetex=True)
plt.style.use("fivethirtyeight")

# read the input files


data = np.genfromtxt(
    "massEst.csv",
    # "massEst_AR20_TR4tenth.csv",
    delimiter=",",
    skip_header=1,
)

mtom = data[:, 3]
S = data[:, 1]
T = data[:, 2]


Z2 = ndimage.gaussian_filter(mtom, sigma=0.1, order=0)

level = np.arange(420e3, 550e3, 10e3)
fig1 = plt.figure(figsize=(8, 6), constrained_layout=True)
ax1 = fig1.add_subplot(1, 1, 1)
# ax1.plot(mtom / S, P / mtom, "o", markersize=6, color="grey")
cs1 = ax1.tricontourf(Z2 * 9.81 / S, T, mtom, levels=level)
ax1.set_xlabel(r"$MTOM/S$ (Newton/m$^2$)", fontsize=16)
ax1.set_ylabel(r"$T/W$ ", fontsize=16)
ax1.set_xlim(0, 15000)
ax1.set_ylim(0.01, 0.4)
cbar = fig1.colorbar(cs1)
# cbar.minorticks_on()
cbar.set_label("MTOM (kg)")
# ax1.clabel(cs1, inline=True, fontsize=10)
plt.show()
