import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from scipy.optimize import fsolve
import sys
sys.path.insert(0,'../')

import matchFunc as mtc
from inputReq import *


wsRatioLand = fsolve(mtc.landingDistanceConstraint, 100)

ws_ratio = np.arange(1, 15100, 100)
cl_max = np.arange(1.5, 2.8, 0.01)

tw_ratio_to = mtc.twRatioTO(ws_ratio, vS, rhoSL)
tw_ratio_turn = mtc.twRatioTurn(ws_ratio, aR, hC0, SweptLE)
tw_ratio_climb = mtc.twRatioClimb(ws_ratio, aR, rhoSL, SweptLE)
tw_ratio_cruise = mtc.twRatioCruise(ws_ratio, aR, hC0, SweptLE)
tw_ratio_ceiling = mtc.twRatioCeiling(ws_ratio, aR, hS, SweptLE)


# ------------------------------------------------------
#
## To calculate the constraint regarding the stall
stallV = []
for nn in range(len(ws_ratio)):
    for kk in range(len(cl_max)):
        speedS = mtc.stallConstraint(ws_ratio[nn], cl_max[kk], rhoSL)
        stallV.append(speedS)

X, Y = np.meshgrid(ws_ratio, cl_max)
vStall = np.reshape(stallV, (len(cl_max), len(ws_ratio)), order="F")

# ----------------------------------------
#
rc("text", usetex=True)
plt.style.use("tableau-colorblind10")
# plt.style.use("fivethirtyeight")

fig1 = plt.figure(figsize=(12, 8), constrained_layout=True)
ax1 = fig1.add_subplot(1, 1, 1)
ax1.plot(
    ws_ratio,
    tw_ratio_to,
    "--",
    linewidth=2,
    color="saddlebrown",
    label=r"Take-off constraint, $S_T=${:,.0f} m".format(sG),
)
ax1.plot(
    ws_ratio,
    tw_ratio_turn,
    "-",
    linewidth=2,
    color="darkmagenta",
    label="Turn constraint, $\phi=${:,.0f} deg at $M =$ {:,.1f}".format(bankAngle, M),
)
ax1.plot(
    ws_ratio,
    tw_ratio_climb,
    "-",
    linewidth=2,
    color="olivedrab",
    label="Climb constraint,  $ROC =${:,.0f} m/s".format(ROC0),
)
ax1.plot(
    ws_ratio,
    tw_ratio_cruise,
    "-.",
    linewidth=2,
    color="orangered",
    label="Cruise constraint,  $h =${:,.0f} m at $M =${:,.1f}".format(hC0, M),
)
ax1.plot(
    ws_ratio,
    tw_ratio_ceiling,
    "-",
    linewidth=2,
    color="royalblue",
    label=r"Ceiling constraint, $h=${:,.0f} m".format(hS),
)
ax1.vlines(
    x=wsRatioLand,
    ymin=0,
    ymax=5,
    color="black",
    label="Landing Constraint,  $S_L=${:,.0f} m".format(lDist),
)
# ax1.plot(
#     6465,
#     0.2054,
#     "ms",
#     label="Lockheed C-5A",
# )

# ax1.plot(
#     6444.459,
#     0.24,
#     "mo",
#     label="Lockheed C-5M",
# )

# ax1.plot(
#     6503.049,
#     0.234,
#     "md",
#     label="An-225 Mriya",
# )

ax1.plot(
    6485,  # 6400,
    0.24,
    "ko",
    label="Current Design",
)
ax1.set_xlabel(r"Wing Loading, N/m$^2$", fontsize=14)
ax1.set_ylabel(r"Thrust-to-Weight Ratio, $T/W$", fontsize=14)
ax1.set_xlim(0, 15000)
ax1.set_ylim(0.01, 0.4)
ax1.tick_params(axis="both", labelsize=12)
ax1.legend(
    loc="upper center", bbox_to_anchor=(0.5, 1.3), ncol=2, fancybox=True, shadow=True
)


step = np.arange(50, 110, 10)
ax2 = ax1.twinx()
CS = ax2.contour(X, Y, vStall, colors="crimson", linewidths=[0.5], levels=step)
ax2.clabel(CS, inline=True, fontsize=10, fmt="$V_{stall}$ = %1.1f", use_clabeltext=True)
ax2.set_ylabel(r"$C_{L_{max}}$", fontsize=14, color="teal")
ax2.tick_params(axis="y", labelcolor="teal", labelsize=12)
ax2.yaxis.set_ticks_position("right")
ax2.grid(False)


data = np.genfromtxt(
    "../massEst.csv",
    delimiter=",",
    skip_header=1,
)

mtom = data[:, 3]
S = data[:, 1]
T = data[:, 2]
level = np.arange(420e3, 550e3, 10e3)
cs2 = ax1.tricontourf(
    mtom * 9.81 / S,
    T,
    mtom,
    levels=level,
    alpha=0.9,  # cmap=plt.get_cmap("ocean")
)
cbar = fig1.colorbar(cs2)
cbar.set_label("MTOM (kg)")


plt.show()
