import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import pandas as pd
from scipy.optimize import fsolve
import scipy.ndimage as ndimage
import sys
sys.path.insert(0,'../')

from isa import isaModel1, isaModel2

from inputReq import *

# import fuelMassRoskam as fm
# import emptyMassEst as eme

##############################################


def meterConvert(d):
    """
    This function converts meter to feet
    """

    dc = d * 3.28084

    return dc


def newtonConvert(f):
    """
    This function convert newton to lbf
    """
    fc = f * 0.224809

    return fc


def densityConvert(rho):
    """
    This function is to calculate the density from kg/m^3 to slugs/ft^3
    """
    rhoc = rho * 0.00194032

    return rhoc


def powerConvert(twRatio, v, etaProp, altS):
    """
    This function is to use to convert thrust-to-WeightRatio to power-to-Weight Ratio
    """
    sigma = isaModel1(altS)[0] / rhoSL

    # pwRatio = twRatio * v / (etaProp * 550)
    # pwRatio = pwRatio / (1.132 * sigma - 0.132)

    pwRatioWatt = twRatio * v / (etaProp * 1000)
    pwRatioWatt = pwRatioWatt * g / (1.132 * sigma - 0.132)

    return pwRatioWatt


#######################################################


def oswaldEff(arCorr, sweptLE):
    """
    This function is to calculate the effective span efficiency, e, based on empirical
    estimation for swept wing (Section 9.5.12 Snorri's ), equation 9-130.
    e = e_theoretical * correction * correction *...
    e_theoretical is the one calculated from Prandt'l Lifting Line Theory as merely a function
    of wing geometry (taper ratio, airfoil, span).
    """
    # eEff = 1.78 * (1 - 0.045 * arCorr**0.68) - 0.64
    eEff = (
        4.61 * (1 - 0.045 * arCorr**0.68) * (np.cos(np.radians(sweptLE))) ** 0.15
        - 3.1
    )

    return eEff


def kConstant(arCorr, sweptLE):
    """
    This function is to calculate the lift-induced drag constant, k, for the aircraft.
    """
    e = oswaldEff(arCorr, sweptLE)
    kAc = 1 / (np.pi * arCorr * e)

    return kAc


def clMaxFunc(wsRatio, vS, rhoSL):
    """
    This function is to calculate the CL_max of aircraft at seal level.
    """

    clMax = 2 * wsRatio / (rhoSL * vS**2)

    return clMax


def wsStall(clMax, vS, rhoSL):
    """
    THis function is to estimate the wsRatioStall
    """

    wsRatioStall = clMax * rhoSL * (vS**2) / 2
    return wsRatioStall


wsRatioStall = wsStall(clMaxTO, vS, rhoSL)


def vClimbFunc(wsRatio):
    """
    This function is to calculate climb speed for single engine piston and turboprop based on Table 3.
    The equation output is KCAS ans is convert to m/s.
    """

    vClimb = 79.016 + 1.2722 * (wsRatio * 0.0208854)
    vClimb = vClimb * 1.68781 * 0.3048
    return vClimb


def twRatioTO(wsRatio, vS, rhoSL):
    """
    This function is to calculate the thrust to weight ratio during the takeoff
    """

    clmax = clMaxFunc(wsRatioStall, vS, rhoSL)

    twTO = (
        1.21 * wsRatio / (g * rhoSL * clmax * sG)
        + 0.605 * (cdTO - miu * clTO) / clmax
        + miu
    )

    # wsRatio = wsRatio * 0.0208854
    # twTO = (
    #     1.21 * wsRatio / (32.174 * 0.002378 * clmax * 640)
    #     + 0.605 * (cdTO - miu * clTO) / clmax
    #     + miu
    # )
    return twTO


def twRatioClimb(wsRatio, ar, rhoSL, sweptLE):
    """
    This function s to calculate thrust-to-weight ratio during climb.
    Empirical equation for Vinf is in KCAS, convert to ft/s, convert to m/s.
    """

    Vclimb = vClimbFunc(wsRatio)
    q2 = 0.5 * rhoSL * Vclimb**2
    Vv = ROC0
    k = kConstant(ar, sweptLE)

    twClimb = Vv / Vclimb + q2 * cdMin / wsRatio + k * wsRatio / q2

    return twClimb


def twRatioClimbAngle(climbAngle, cdmin, ar, sweptLE):
    """
    This function is to calculate the tw ratio for desired climb angle
    """
    k = kConstant(ar, sweptLE)
    twClimbAngle = np.sin(np.radians(climbAngle)) + np.sqrt(4 * k * cdmin)

    return twClimbAngle


def twRatioTurn(wsRatio, ar, alt, sweptLE):
    """
    This function is to calculate the thrust-to-weight ratio at constant velocity turn
    at cruise altitude (operating altitute).
    Example case is at 2438.4 m
    """
    rhoH1 = isaModel1(alt)[0]
    vTurn = vC
    q3 = 0.5 * rhoH1 * vTurn**2
    k = kConstant(ar, sweptLE)

    twTurn = q3 * (cdMin / wsRatio + k * wsRatio * (nTurn / q3) ** 2)

    return twTurn


def twRatioCruise(wsRatio, ar, alt, sweptLE):
    """
    This function is to calculate the thrust-to-weight ratio at cruise.
    """

    rhoH1 = isaModel1(alt)[0]
    q4 = 0.5 * rhoH1 * vC**2
    k = kConstant(ar, sweptLE)

    twCruise = q4 * cdMin / wsRatio + k * wsRatio / q4

    return twCruise


def twRatioCeiling(wsRatio, ar, altS, sweptLE):
    """
    This function is to calculate the thrust-to-weight ratio at service ceiling.
    altS is the ceiling altitude
    """
    vClimb = 79.016 + 1.2722 * (wsRatio * 0.0208854)
    vClimbConv = vClimbFunc(wsRatio)
    sigma = isaModel2(altS)[0] / rhoSL
    vTAS = vClimb / np.sqrt(sigma)
    vTAS = vTAS * 1.68781 * 0.3048
    k = kConstant(ar, sweptLE)

    # q5 = 0.5 * rhoSL * vClimbConv**2
    q5 = 0.5 * rhoSL * vTAS**2

    twCeiling = 0.508 / vTAS + q5 * cdMin / wsRatio + k * wsRatio / q5

    return twCeiling


def landingDistanceConstraint(wsRatio):
    """
    This function is to calculate the wing loading required for landing at certain landing distance

    """

    # clMaxLand = clMaxFunc(wsRatioStall, vLand, rhoSL)
    A = rhoSL * 0.00194032 * clMaxLand
    A1 = 1.21 / (
        gImp * ((0.605 / clMaxLand * (cdLdg - miuLdg * clLdg)) + miuLdg - TgrToW)
    )

    # wsRatio = wsRatio * 0.0208854
    sldg = 19.08 * hobs + (
        (0.007923 + (1.556 * tau * (A**0.5) / ((wsRatio * 0.0208854) ** 0.5)) + A1)
        * (wsRatio * 0.0208854 / A)
    )

    deltaLd = np.abs(sldg - (lDist * 3.28084))

    return deltaLd


def stallConstraint(wsRatio, clMax, rhoSL):
    """
    To evaluate the stall speed for various wing loading
    """
    stallSpeed = np.sqrt(2 * wsRatio / (rhoSL * clMax))

    return stallSpeed


# # ------------------------------------------------------
