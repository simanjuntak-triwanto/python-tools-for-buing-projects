import scipy as sp
import numpy as np
from scipy.optimize import fsolve
from scipy.optimize import minimize

from isa import isaModel1
import prandtLift as pl
from inputReq import *


def alphaCruise(alp, taperRatio, ar, cla, cr, S, alphaZL, mtom, vc, alt, sweptHalf):
    """
    This function is to estimate the angle of attack at CL_cruise,
    All the variable is given except of Alp.
    Alp is the initial guess in degree
    CLc is estimated from the function "clCruiseEst"
    """
    CLa = pl.aeroAnalysis(
        PHI1, PHI2, NN, taperRatio, ar, cla, cr, S, alp, alphaZL, sweptHalf
    )[3]
    clc = clCruiseEst(mtom, S, vc, alt)
    AlphaEst = (clc / CLa) + np.radians(alphaZL)
    deltaAlpha = np.abs(alp - np.degrees(AlphaEst))

    return deltaAlpha


def clCruiseEst(mtom, S, vc, alt):
    """
    This function is to estimate the lift coefficient at cruise
    """

    rhoCruise = isaModel1(alt)[0]
    q = 0.5 * rhoCruise * (vc**2)
    clc = 0.95 * mtom * g / (q * S)

    return clc


def cdWingEst(S, ar, alt, tc):
    """
    This function to estimate the CD for wing
    """
    Swet = 1.8 * S
    rhoCruise = isaModel1(alt)[0]
    vC_alt = vC
    Re = rhoCruise * vC_alt * np.sqrt(S / ar) / viscousConst
    cf = 0.455 / ((np.log10(Re)) ** 2.58)
    FF = 1 + 1.2 * tc + 70 * (tc**4)
    IF = 1

    cdWing = (Swet / S) * cf * FF * IF

    return cdWing


def cdCruiseEst(S, ar, taperRatio, alt, tc, cla, cr, alphaZL, mtom, vc, sweptHalf):
    """
    This function is to calculate the coefficient of drag based on cruise configuration
    """

    alpCruise = fsolve(
        alphaCruise,
        x0=0.1,
        args=(taperRatio, ar, cla, cr, S, alphaZL, mtom, vc, alt, sweptHalf),
    )

    cdWing = cdWingEst(S, ar, alt, tc)
    cDMin = cdx + cdWing

    cdi = pl.aeroAnalysis(
        PHI1, PHI2, NN, taperRatio, ar, cla, cr, S, alpCruise, alphaZL, sweptHalf
    )[2]
    cdCruise = cDMin + cdi

    return cdCruise
