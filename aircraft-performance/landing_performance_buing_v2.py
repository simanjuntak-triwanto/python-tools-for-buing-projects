#!/usr/bin/env python3

import utils.uconverters as convert
import numpy as np
from core.isamodel import isa


GRAVITY = 9.81


def get_flare_height(landing_stall_speed, gamma_approach=3):
    """
    Calculate flare height during landing maneuver (m).

    Keyword Arguments:
    stall_seed_landing_conf    -- (m/s)
    gamma_approach (3 deg)     -- Flight path angle during approach
    """
    return (
        0.1512
        * 3.280839895013123  # additional constant conversion from imperial to metric
        * landing_stall_speed**2
        * (1 - np.cos(np.deg2rad(gamma_approach)))
    )


def approach_distance(h_obstacle, flare_height, gamma_approach):
    """
    Calculate the approach distance (m) during landing maneuver.

    Keyword Arguments:
    h_obstacle        -- obstaclle height (m)
    flare_height      -- flare height (m)
    gamma_approach (3 deg)     -- flight path angle during approach (deg)
    """
    gamma_radians = np.deg2rad(gamma_approach)
    return (h_obstacle - flare_height) / np.tan(gamma_radians)


def flare_distance(landing_stall_speed, gamma_approach):
    """
    Calculate flare distance (m) during landing maneuver.

    Keyword Arguments:
    landing_stall_speed --  (m/s)
    gamma_approach      --  flight path angle during approach (deg)
    """
    return (
        0.1512
        * 3.280839895013123  # additional constant conversion from imperial to metric
        * landing_stall_speed**2
        * np.sin(np.radians(gamma_approach))
    )


def free_roll_distance(touch_down_speed, size="large"):
    """
    Calculate free-roll distance during landing maneuver.

    Keyword Arguments:
    touch_down_speed  -- Touchdown speed (m/s)
    size -- Size of the aircraft ("large" or "small"), default is "large"
    (default "large")
    """
    free_roll_time = 3 if size == "large" else 1

    return touch_down_speed * free_roll_time


def braking_distance(
    breaking_speed, landing_weight, thrust, landing_drag, landing_lift, miu
):
    """
    Calculate braking distance (m) during landing maneuver.

    Keyword Arguments:
    breaking_speed   -- Braking speed (m/s)
    landing_weight   -- Landing weight (Newtons)
    thrust           -- Thrust (Newtons)
    landing_drag     -- Drag during landing (Newtons)
    landing_lift     -- Lift during landing (Newtons)
    miu              -- Airfield friction coefficient
    """
    numerator = breaking_speed**2 * landing_weight
    denominator = (
        2
        * GRAVITY
        * (thrust - landing_drag - miu * (landing_weight - landing_lift))
    )

    return -numerator / denominator


MIU = 0.3
LANDING_STALL_SPEED = convert.knot_to_meter_per_second(59)
GAMMA_APPROACH = 3
LANDING_WEIGHT = convert.pounds_force_to_newtons(3400)
PROP_EFFICIENCY = 0.45
IDLE_POWER = convert.brake_horsepower_to_kilowatts(50) * 1000
CD_LANDING = 0.040
CL_LANDING = 1.0
H_OBSTACLE = convert.feet_to_meters(50)
WING_AREA = convert.square_feet_to_square_meters(144.9)
LANDING_ALTITUDE = 1000

V_REF = 1.3 * LANDING_STALL_SPEED
V_FLR = 1.3 * LANDING_STALL_SPEED
V_TD = 1.1 * LANDING_STALL_SPEED
V_BR = 1.1 * LANDING_STALL_SPEED
_, _, rho, _, _ = isa(LANDING_ALTITUDE)


# h_f = get_flare_height(LANDING_STALL_SPEED, GAMMA_APPROACH)

# s_a = approach_distance(H_OBSTACLE, h_f, GAMMA_APPROACH)

# s_f = flare_distance(LANDING_STALL_SPEED, GAMMA_APPROACH)

# # at VBR_AVG
V_AVG = V_BR / np.sqrt(2)

LANDING_THRUST = PROP_EFFICIENCY * IDLE_POWER / V_AVG
LANDING_DRAG = 0.5 * rho * (V_AVG**2) * WING_AREA * CD_LANDING
LANDING_LIFT = 0.5 * rho * (V_AVG**2) * WING_AREA * CL_LANDING

# s_fr = free_roll_distance(V_BR, "small")


# s_br = braking_distance(
#     V_BR,
#     LANDING_WEIGHT,
#     LANDING_THRUST,
#     LANDING_DRAG,
#     LANDING_LIFT,
#     MIU,
# )

# s_landing = s_a + s_f + s_fr + s_br


def calculate_landing_distance(h_obstacle, landing_stall_speed, gamma_approach):
    """
    Calculate the total landing distance during a landing maneuver (m).

    Args:
    h_obstacle (float): Height of the obstacle (m)
    landing_stall_speed (float): Stall speed during landing configuration (m/s)
    gamma_approach (float): Flight path angle during approach (degrees)

    Returns:
    float: Total landing distance in meters
    """
    h_f = get_flare_height(landing_stall_speed, gamma_approach)
    s_a = approach_distance(h_obstacle, h_f, gamma_approach)
    s_f = flare_distance(landing_stall_speed, gamma_approach)

    s_fr = free_roll_distance(V_BR, "small")
    s_br = braking_distance(
        V_BR, LANDING_WEIGHT, LANDING_THRUST, LANDING_DRAG, LANDING_LIFT, MIU
    )
    s_landing = s_a + s_f + s_fr + s_br

    return s_landing


s_landing = calculate_landing_distance(
    H_OBSTACLE, LANDING_STALL_SPEED, GAMMA_APPROACH
)
