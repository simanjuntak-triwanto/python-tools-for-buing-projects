#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

from core.aerobasis import lift_drag_polar_using_k_factor
from core.steady_symmetry import compute_velocity_symmetric_flight
from core.isamodel import isa

from core.takeoff import takeoff_distance

matplotlib.rcParams["text.usetex"] = True
plt.style.use("bmh")

# Weights
MTOW = 5_399_943.93  # Maximum takeoff weight (N)
FUEL_WEIGHT_NOMINAL = 1_471_500  # Nominal fuel weight (N)
MAX_FUEL_WEIGHT = 2_280_000  # Maximum fuel weight (N)
MAX_PAYLOAD_WEIGHT = 1_912_950  # Maximum payload weight (N)
EMPTY_OPERATING_WEIGHT = 2_015_493.93  # Empty operating weight (N)
SIZE = "large"

# Takeoff condition
OBSTACLE_HEIGHT = 15.2
DELTA_STO = 200  # Inertia distance (m)
MIU = 0.04  # Runway friction coefficient --- breaking off
TAKEOFF_ALTITUDE = 0  # Takeoff altitude
_, _, RHO0, _, _ = isa(TAKEOFF_ALTITUDE)

# Aerodynamics takeoff
WING_AREA = 728.6  # Wing Area (m^2)
CL_MAX = 2.7
K_FACTOR = 0.04415
CD0 = 0.06117
STALL_SPEED = compute_velocity_symmetric_flight(MTOW, WING_AREA, RHO0, CL_MAX)

CL_TO = 2.5
CD_TO = lift_drag_polar_using_k_factor(CD0, K_FACTOR, CL_MAX)


# Propulsion
ENGINE_NAME = "Rolls-Royce Trent-1000"
NUMBER_OF_ENGINE = 4
MAX_THRUST_ONE_ENGINE_ASL = (
    360_400  # 39_000  # Max thrust from one engine at sea level
)
MAX_TOTAL_THRUST = (
    NUMBER_OF_ENGINE * MAX_THRUST_ONE_ENGINE_ASL
)  # Maximum static thrust from all engines (N)
BPR = 10  # Bypass Ratio
THRUST_AT_REDUCED_VR = MAX_TOTAL_THRUST  # During ground round
THRUST_AT_TRANSITION = MAX_TOTAL_THRUST


# Generate altitudes
Hs = np.linspace(0, 1500, 5)

# Set up plot
fig, ax = plt.subplots(figsize=(10, 10))

# Define markers for each altitude
markers = ["o", "v", "^", "x", "8", "+"]

# Plot BFL vs ISA+Temperature for each altitude
for index, H in enumerate(Hs):
    dTs = np.linspace(0, 20, 500)
    s_takeoffs = []
    marker = markers[index]
    for dT in dTs:
        # Calculate ISA at current altitude and temperature
        _, _, rho, _, _ = isa(H, dT)

        # Calculate balanced field length
        s_takeoff = takeoff_distance(
            MTOW,
            rho,
            WING_AREA,
            CL_MAX,
            CL_TO,
            CD_TO,
            THRUST_AT_REDUCED_VR,
            THRUST_AT_TRANSITION,
            MIU,
            CD0,
            K_FACTOR,
            STALL_SPEED,
            SIZE,
            OBSTACLE_HEIGHT,
        )
        s_takeoffs.append(s_takeoff)

    # Plot BFL vs ISA+Temperature
    ax.plot(
        dTs,
        s_takeoffs,
        # linestyle="--",
        label=f"H = {int(H)} m",
        # marker=marker,
        # markersize=10,
    )

# # Add vertical line at ISA+20K
ax.vlines(
    15,
    2_200,
    2_700,
    linestyles="dashed",
    label="BFL at T= ISA + 15 K",
    color="tab:gray",
)

# Set plot labels and legend
# ax.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
ax.legend()
ax.set_xlabel(r"Temperature (ISA + $\Delta T$), K")
ax.set_ylabel(r"Takeoff Distance, m")

# Adjust layout and save plot
plt.tight_layout()
fig.savefig("takeoff_distance_vs_temp_vs_altitude_buing.pdf", dpi=300)

# Show plot
plt.show()
