#!/usr/bin/env python3

import numpy as np

from core.steady_symmetry import (
    compute_velocity_symmetric_flight,
    power_available_from_thrust,
    power_required_from_drag,
)
from core.aerobasis import aero_max_ratios_using_AR, compute_CL_at_CL3CD2max
from core.isamodel import isa
from core.propulsion import maximum_thrust_turbofan

from utils.uconverters import (
    pounds_to_kilograms,
    square_feet_to_square_meters,
    feet_to_meters,
    pounds_force_to_newtons,
)

GRAVITY = 9.81

WING_AREA = square_feet_to_square_meters(318)  # m^2
WING_SPAN = feet_to_meters(53.3)  # m
AIRCRAFT_WEIGHT = pounds_to_kilograms(2950) * GRAVITY  # Newton
FUEL_WEIGHT = pounds_force_to_newtons(400)
TOTAL_THRUST = 2 * pounds_force_to_newtons(3650)

BPR = 5

CD0 = 0.02
OSWALD_EFFICIENCY = 0.81
ASPECT_RATIO = WING_SPAN**2 / WING_AREA
K_FACTOR = 1 / (np.pi * ASPECT_RATIO * OSWALD_EFFICIENCY)
CL_CD_MAX, CL3_CD2_MAX, CL_CD2_MAX = aero_max_ratios_using_AR(
    CD0, ASPECT_RATIO, OSWALD_EFFICIENCY
)

CL_at_CL3CD2max = compute_CL_at_CL3CD2max(CD0, K_FACTOR)
CD_at_CL3CD2max = CD0 + K_FACTOR * CL_at_CL3CD2max**2


altitude = 0  # meter

_, _, air_density, sound_speed, _ = isa(altitude, 0)
velocity_at_symmetric_flight = compute_velocity_symmetric_flight(
    AIRCRAFT_WEIGHT, WING_AREA, air_density, CL_at_CL3CD2max
)
drag_at_altitude = (
    0.5
    * air_density
    * velocity_at_symmetric_flight**2
    * WING_AREA
    * CD_at_CL3CD2max
)

# Calculate Mach number at altitude
mach_at_altitude = velocity_at_symmetric_flight / sound_speed

# Calculate thrust at altitude
thrust_at_altitude = maximum_thrust_turbofan(
    TOTAL_THRUST, BPR, altitude, mach_at_altitude
)

# Calculate power available and power required
power_available = power_available_from_thrust(
    thrust_at_altitude, velocity_at_symmetric_flight
)
power_required = power_required_from_drag(
    drag_at_altitude, velocity_at_symmetric_flight
)

roc_max = (power_available - power_required) / AIRCRAFT_WEIGHT
