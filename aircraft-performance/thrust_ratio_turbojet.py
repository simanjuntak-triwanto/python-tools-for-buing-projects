#!/usr/bin/env python3

from core.propulsion import maximum_thrust_turbojet
import numpy as np
import matplotlib.pyplot as plt

# Set matplotlib parameters for better visualization
plt.rcParams["text.usetex"] = True
plt.style.use("bmh")

# Constants
TR = 1.072
THRUST_SL = 436000  # Maximum Sea Level thrust of P&W PW4000
# BPR = 6.4


def plot_thrust_ratio_vs_mach_vs_altitude():
    altitudes = np.linspace(0, 40000, 5)
    machs = np.linspace(0, 1, 2000)

    # Set up plot
    fig, ax = plt.subplots(figsize=(10, 10))

    # Define markers for each altitude
    markers = ["o", "v", "^", "x", "+"]

    # Plot BFL vs ISA+Temperature for each altitude
    for index, altitude in enumerate(altitudes):
        marker = markers[
            index % len(markers)
        ]  # Ensure markers repeat if needed
        thrusts = [
            maximum_thrust_turbojet(THRUST_SL, altitude, mach, kind="civil")
            / THRUST_SL
            for mach in machs
        ]
        ax.plot(
            machs,
            thrusts,
            # linestyle="None",
            label=f"H = {int(altitude)} m",
            # marker=marker,
            # markersize=10,
        )

    # Set plot labels and legend
    ax.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
    ax.set_xlabel(r"Mach Number")
    ax.set_ylabel(r"Thrust Ratio")

    # Adjust layout and save plot
    plt.tight_layout()
    fig.savefig("thrusts_vs_mach_vs_altitude_turbojet.pdf", dpi=300)

    # Show plot
    plt.show()


if __name__ == "__main__":
    plot_thrust_ratio_vs_mach_vs_altitude()
