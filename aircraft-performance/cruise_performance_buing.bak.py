#!/usr/bin/env python3

import numpy as np

from core.aerobasis import aero_max_ratios_using_AR, compute_CL_at_CLCD2max
from core.isamodel import isa
from core.cruise import (
    compute_jet_range,
    compute_jet_endurance,
    compute_final_weight_ratio_from_endurance_jet,
    compute_initial_weight_from_range_jet,
)
from utils.uconverters import (
    pounds_to_kilograms,
    square_feet_to_square_meters,
    feet_to_meters,
    gallons_to_kilograms,
    pounds_force_to_newtons,
    nautical_miles_to_meters,
)


GRAVITY = 9.81

WING_AREA = square_feet_to_square_meters(318)  # m^2
WING_SPAN = feet_to_meters(53.3)  # m
MTOW = pounds_to_kilograms(2950) * GRAVITY  # Newton
# FUEL_WEIGHT = gallons_to_kilograms(779) * GRAVITY  # Newton
FUEL_WEIGHT = pounds_force_to_newtons(400)
TOTAL_THRUST = 2 * pounds_force_to_newtons(3650)


C_T = 0.6 / 3600  # specific fuel consumption (1/s)
INITIAL_WEIGHT = pounds_to_kilograms(19895) * GRAVITY  # Newton
FINAL_WEIGHT = pounds_to_kilograms(12352) * GRAVITY  # Newton
FST_CRUISING_ALTITUDE = feet_to_meters(22000)
SND_CRUISNG_ALTITUDE = feet_to_meters(15000)
_, _, AIR_DENSITY, _, _ = isa(FST_CRUISING_ALTITUDE, 0)
FINAL_WEIGHT_RATIO = FINAL_WEIGHT / INITIAL_WEIGHT


CD0 = 0.02
OSWALD_EFFICIENCY = 0.81
ASPECT_RATIO = WING_SPAN**2 / WING_AREA
K_FACTOR = 1 / (np.pi * ASPECT_RATIO * OSWALD_EFFICIENCY)
CL_CD_MAX, CL3_CD2_MAX, CL_CD2_MAX = aero_max_ratios_using_AR(
    CD0, ASPECT_RATIO, OSWALD_EFFICIENCY
)

CL_at_CLCD2max = compute_CL_at_CLCD2max(CD0, K_FACTOR)


jet_range = compute_jet_range(
    C_T, WING_AREA, AIR_DENSITY, CL_CD2_MAX, INITIAL_WEIGHT, FINAL_WEIGHT
)

jet_endurance = compute_jet_endurance(C_T, CL_CD_MAX, FINAL_WEIGHT_RATIO)


# Weight Fractions During Nominal Mission


def compute_nominal_range(
    mtow,
    fuel_weight,
    wing_area,
    cl_cd_ratio,
    cl_cd2_ratio,
    fst_cruising_altitude,
    snd_cruising_altitude,
    c_t,
):
    """
    Compute nominal cruising distance of BUING (m)

    Keyword Arguments:
    mtow                  -- Maximum takeoff weight (N)
    fuel_weight           -- Total fuel weight (N)
    wing_area             -- Wing area (m^2)
    cl_at_clcd2_max       -- CL at maxsimum CL/CD^2
    clcd2_max_ratio       -- Ratio of CL/CD^2
    fst_cruising_altitude -- Altitude of the first cruising (m)
    snd_cruising_altitude -- Altitude of the second cruising (m)
    thrust_asl            -- Total thrust at sea level (N)
    c_t                   -- Specific fuel consumption (1/s) at sea level
    """
    _, _, air_density_1, _, _ = isa(fst_cruising_altitude, 0)
    _, _, air_density_2, _, _ = isa(snd_cruising_altitude, 0)

    w11 = mtow - fuel_weight  # empty weight
    w11_w0_ratio = w11 / mtow

    w1_w0_ratio = 0.990  # Engine Start, Warm-up
    w2_w1_ratio = 0.990  # Taxy
    w3_w2_ratio = 0.995  # Takeoff
    w4_w3_ratio = 0.980  # Climb
    w4_w0_ratio = w1_w0_ratio * w2_w1_ratio * w3_w2_ratio * w4_w3_ratio

    w6_w5_ratio = 0.990  # Descent for landing attempt
    w7_w6_ratio = 0.980  # Re-climb
    w9_w8_ratio = 0.990  # Re-descent for landing

    # Loitering 45 minutes at 5000 ft
    w10_w9_ratio = compute_final_weight_ratio_from_endurance_jet(
        c_t, cl_cd_ratio, 45 * 3600
    )

    w11_w10_ratio = 0.992  # Lading, Taxi, Shutdown

    w11_w8_ratio = w9_w8_ratio * w10_w9_ratio * w11_w10_ratio

    w8 = (1 / w11_w8_ratio) * w11

    # Cruising at 15,000 ft for 100 nmi
    w7 = compute_initial_weight_from_range_jet(
        c_t,
        wing_area,
        air_density_2,
        cl_cd2_ratio,
        w8,
        nautical_miles_to_meters(100),
    )
    w8_w7_ratio = w8 / w7

    w11_w5_ratio = (
        w6_w5_ratio
        * w7_w6_ratio
        * w8_w7_ratio
        * w9_w8_ratio
        * w10_w9_ratio
        * w11_w10_ratio
    )
    w5 = (1 / w11_w5_ratio) * w11

    w5_w4_ratio = w11_w0_ratio / (w4_w0_ratio * w11_w5_ratio)

    w4 = (1 / w5_w4_ratio) * w5

    buing_range = compute_jet_range(
        c_t, wing_area, air_density_1, cl_cd2_ratio, w5, w4
    )

    return buing_range


buing_range = compute_nominal_range(
    MTOW,
    FUEL_WEIGHT,
    WING_AREA,
    CL_CD_MAX,
    CL_CD2_MAX,
    FST_CRUISING_ALTITUDE,
    SND_CRUISNG_ALTITUDE,
    C_T,
)
