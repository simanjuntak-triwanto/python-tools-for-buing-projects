#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from core.takeoff import balanced_field_length
from core.isamodel import isa

# Set matplotlib parameters for better visualization
plt.rcParams["text.usetex"] = True
plt.style.use("bmh")

GRAVITY_ACCELERATION = 9.81  # Gravity acceleration (m/s^2)

# Weights
MTOW = 5_399_943.93  # Maximum takeoff weight (N)
FUEL_WEIGHT_NOMINAL = 1_471_500  # Nominal fuel weight (N)
MAX_FUEL_WEIGHT = 1_765_800  # Maximum fuel weight (N)
MAX_PAYLOAD_WEIGHT = 1_912_950  # Maximum payload weight (N)
EMPTY_OPERATING_WEIGHT = 2_015_493.93  # Empty operating weight (N)

# Aerodynamics takeoff
WING_AREA = 728.6  # Wing Area (m^2)
CL_MAX = 2.7
K_FACTOR = 0.04415
CD0 = 0.06117

# Takeoff condition
OBSTACLE_HEIGHT = 15.2  # Obstacle height for GA aircraft (m)
DELTA_STO = 200  # Inertia distance (m)

# Propulsion
ENGINE_NAME = "Rolls-Royce Trent-1000"
NUMBER_OF_ENGINE = 4
MAX_THRUST_ONE_ENGINE_ASL = 360_400
MAX_TOTAL_THRUST = (
    NUMBER_OF_ENGINE * MAX_THRUST_ONE_ENGINE_ASL
)  # Maximum static thrust from all engines (N)
BPR = 10  # Bypass Ratio


# Calculate ISA at sea level
[T0, p0, RHO0, c0, miu0] = isa(0)

# Generate altitudes
Hs = np.linspace(0, 1500, 5)

# Set up plot
fig, ax = plt.subplots(figsize=(10, 10))

# Define markers for each altitude
markers = ["o", "v", "^", "x", "8", "+"]

# Plot BFL vs ISA+Temperature for each altitude
for index, H in enumerate(Hs):
    dTs = np.linspace(0, 20, 500)
    s_bfls = []
    marker = markers[index]
    for dT in dTs:
        # Calculate ISA at current altitude and temperature
        [T, p, rho, c, miu] = isa(H, dT)

        # Calculate balanced field length
        s_bfl = balanced_field_length(
            MTOW,
            WING_AREA,
            rho,
            CD0,
            K_FACTOR,
            CL_MAX,
            OBSTACLE_HEIGHT,
            MAX_TOTAL_THRUST,
            BPR,
            NUMBER_OF_ENGINE,
            rho / RHO0,
            DELTA_STO,
        )
        s_bfls.append(s_bfl)

    # Plot BFL vs ISA+Temperature
    ax.plot(
        dTs,
        s_bfls,
        # linestyle="--",
        label=f"H = {int(H)} m",
        # marker=marker,
        # markersize=10,
    )

# Add vertical line at ISA+20K
ax.vlines(
    15,
    3_000,
    3_700,
    linestyles="dashed",
    label="BFL at T= ISA + 15 K",
    color="tab:gray",
)

# Set plot labels and legend
# ax.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
ax.legend()
ax.set_xlabel(r"Temperature (ISA + $\Delta T$), K")
ax.set_ylabel(r"Balanced Field Lenght (BFL), m")

# Adjust layout and save plot
plt.tight_layout()
fig.savefig("bfl_vs_temp_vs_altitude_buing.pdf", dpi=300)

# Show plot
plt.show()
