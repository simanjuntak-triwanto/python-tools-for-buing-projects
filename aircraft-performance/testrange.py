#!/usr/bin/env python3
import numpy as np

from core.aerobasis import aero_max_ratios_using_AR, compute_CL_at_CLCD2max
from core.isamodel import isa
from core.cruise import (
    compute_jet_range,
    compute_jet_endurance,
    compute_final_weight_ratio_from_endurance_jet,
    compute_initial_weight_from_range_jet,
)
from utils.uconverters import (
    pounds_to_kilograms,
    square_feet_to_square_meters,
    feet_to_meters,
    gallons_to_kilograms,
    pounds_force_to_newtons,
    nautical_miles_to_meters,
)


GRAVITY = 9.81


# Define the first version of the function
def compute_nominal_range_v1(
    max_takeoff_weight,
    total_fuel_weight,
    wing_area,
    cl_cd2_ratio,
    cruising_altitude_1,
    cruising_altitude_2,
    specific_fuel_consumption,
):
    """
    Compute the nominal cruising distance of an aircraft.

    Parameters:
        max_takeoff_weight (float): Maximum takeoff weight (N).
        total_fuel_weight (float): Total fuel weight (N).
        wing_area (float): Wing area (m^2).
        cl_cd2_ratio (float): Ratio of CL/CD^2.
        cruising_altitude_1 (float): Altitude of the first cruising phase (m).
        cruising_altitude_2 (float): Altitude of the second cruising phase (m).
        specific_fuel_consumption (float): Specific fuel consumption (1/s) at sea level.

    Returns:
        float: Nominal cruising distance of the aircraft (m).
    """
    # Calculate air density at cruising altitudes
    _, _, air_density_1, _, _ = isa(cruising_altitude_1, 0)
    _, _, air_density_2, _, _ = isa(cruising_altitude_2, 0)

    # Calculate empty weight
    empty_weight = max_takeoff_weight - total_fuel_weight

    # Calculate weight ratios for different phases
    start_ratio = 0.990
    taxi_ratio = 0.995
    takeoff_ratio = 0.980
    climb_ratio = 0.992

    # Calculate weights at different phases
    landing_weight = (
        empty_weight * start_ratio * taxi_ratio * takeoff_ratio * climb_ratio
    )
    cruising_distance = 100 * 1852  # Convert 100 nautical miles to meters
    cruising_weight = initial_weight_from_range_jet(
        specific_fuel_consumption,
        wing_area,
        air_density_2,
        cl_cd2_ratio,
        landing_weight,
        cruising_distance,
    )
    cruising_weight_ratio = landing_weight / cruising_weight
    descent_weight = cruising_weight * cruising_weight_ratio
    landing_descent_weight = descent_weight / climb_ratio
    landing_weight_final = empty_weight / (landing_descent_weight * start_ratio)

    # Calculate range
    buing_range = jet_range(
        specific_fuel_consumption,
        wing_area,
        air_density_1,
        cl_cd2_ratio,
        landing_weight_final,
        landing_weight,
    )

    return buing_range


# Define the second version of the function
def compute_nominal_range_v2(
    mtow,
    fuel_weight,
    wing_area,
    cl_cd_ratio,
    cl_cd2_ratio,
    fst_cruising_altitude,
    snd_cruising_altitude,
    c_t,
):
    """
    Compute nominal cruising distance of BUING (m)

    Keyword Arguments:
    mtow                  -- Maximum takeoff weight (N)
    fuel_weight           -- Total fuel weight (N)
    wing_area             -- Wing area (m^2)
    cl_at_clcd2_max       -- CL at maximum CL/CD^2
    clcd2_max_ratio       -- Ratio of CL/CD^2
    fst_cruising_altitude -- Altitude of the first cruising (m)
    snd_cruising_altitude -- Altitude of the second cruising (m)
    thrust_asl            -- Total thrust at sea level (N)
    c_t                   -- Specific fuel consumption (1/s) at sea level
    """
    _, _, air_density_1, _, _ = isa(fst_cruising_altitude, 0)
    _, _, air_density_2, _, _ = isa(snd_cruising_altitude, 0)

    w11 = mtow - fuel_weight  # empty weight
    w11_w0_ratio = w11 / mtow

    w1_w0_ratio = 0.990  # Engine Start, Warm-up
    w2_w1_ratio = 0.990  # Taxi
    w3_w2_ratio = 0.995  # Takeoff
    w4_w3_ratio = 0.980  # Climb
    w4_w0_ratio = w1_w0_ratio * w2_w1_ratio * w3_w2_ratio * w4_w3_ratio

    w6_w5_ratio = 0.990  # Descent for landing attempt
    w7_w6_ratio = 0.980  # Re-climb
    w9_w8_ratio = 0.990  # Re-descent for landing

    # Loitering 45 minutes at 5000 ft
    w10_w9_ratio = compute_final_weight_ratio_from_endurance_jet(
        c_t, cl_cd_ratio, 45 * 3600
    )

    w11_w10_ratio = 0.992  # Landing, Taxi, Shutdown

    w11_w8_ratio = w9_w8_ratio * w10_w9_ratio * w11_w10_ratio

    w8 = (1 / w11_w8_ratio) * w11

    # Cruising at 15,000 ft for 100 nmi
    w7 = compute_initial_weight_from_range_jet(
        c_t,
        wing_area,
        air_density_2,
        cl_cd2_ratio,
        w8,
        nautical_miles_to_meters(10),
    )
    w8_w7_ratio = w8 / w7

    w11_w5_ratio = (
        w6_w5_ratio
        * w7_w6_ratio
        * w8_w7_ratio
        * w9_w8_ratio
        * w10_w9_ratio
        * w11_w10_ratio
    )
    w5 = (1 / w11_w5_ratio) * w11

    w5_w4_ratio = w11_w0_ratio / (w4_w0_ratio * w11_w5_ratio)

    w4 = (1 / w5_w4_ratio) * w5

    buing_range = compute_jet_range(
        c_t, wing_area, air_density_1, cl_cd2_ratio, w5, w4
    )

    return buing_range


# Compare the results
result_v1 = compute_nominal_range_v1(
    1000000, 200000, 150, 0.5, 5000, 15000, 0.00002
)
result_v2 = compute_nominal_range_v2(
    1000000, 200000, 150, 0.6, 0.5, 5000, 15000, 0.00002
)

if result_v1 == result_v2:
    print("Both versions produce the same result.")
else:
    print("There is a difference between the results.")
