#!/usr/bin/env python3


class ImperialToSI:
    # Constants for imperial to SI conversion
    FT2M = 0.3048  # feet to meter
    LBM2KG = 0.453592  # pound mass to kg
    KNOT2MPS = 0.514444  # knot to m/s
    LBF2N = 4.44822  # lbf to N
    IN2M = 0.0254  # inch to m
    IN2MM = 25.4  # inch to mm
    NMI2M = 1852  # nautical mile to m
    FPM2MPS = 0.00508  # Feet per minute to m/s
    FTSQ2MSQ = 0.092903  # Feet square to meter square
    FPS2MPS = 0.3048  # Feet per second to m/s
    BHP2KW = 0.7457  # BHP to kW
    GAL2KG = 3.04  # US Gallons fuel jet A to kg

    @staticmethod
    def feet_to_meters(feet):
        """Convert from feet to meters."""
        return ImperialToSI.FT2M * feet

    @staticmethod
    def pounds_to_kilograms(pounds):
        """Convert from pounds to kilograms."""
        return ImperialToSI.LBM2KG * pounds

    @staticmethod
    def knot_to_meter_per_second(knot):
        """Convert from knot to meter per second."""
        return ImperialToSI.KNOT2MPS * knot

    @staticmethod
    def inches_to_meters(inches):
        """Convert from inches to meters."""
        return ImperialToSI.IN2M * inches

    @staticmethod
    def inches_to_millimeters(inches):
        """Convert from inches to millimeters."""
        return ImperialToSI.IN2MM * inches

    @staticmethod
    def nautical_miles_to_meters(nautical_miles):
        """Convert from nautical miles to meters."""
        return ImperialToSI.NMI2M * nautical_miles

    @staticmethod
    def feet_per_minute_to_meters_per_second(feet_per_minute):
        """Convert from feet per minute to meters per second."""
        return ImperialToSI.FPM2MPS * feet_per_minute

    @staticmethod
    def square_feet_to_square_meters(square_feet):
        """Convert from square feet to square meters."""
        return ImperialToSI.FTSQ2MSQ * square_feet

    @staticmethod
    def feet_per_second_to_meters_per_second(feet_per_second):
        """Convert from feet per second to meters per second."""
        return ImperialToSI.FPS2MPS * feet_per_second

    @staticmethod
    def brake_horsepower_to_kilowatts(brake_horsepower):
        """Convert from brake horsepower to kilowatts."""
        return ImperialToSI.BHP2KW * brake_horsepower

    @staticmethod
    def gallons_to_kilograms(gallons):
        """Convert from US gallons of jet fuel A to kilograms."""
        return ImperialToSI.GAL2KG * gallons


class SIToImperial:
    # Constants for SI to imperial conversion
    M2FT = 1 / ImperialToSI.FT2M
    KG2LBM = 1 / ImperialToSI.LBM2KG
    MPS2KNOT = 1 / ImperialToSI.KNOT2MPS
    N2LBF = 1 / ImperialToSI.LBF2N
    M2IN = 1 / ImperialToSI.IN2M
    MM2IN = 1 / ImperialToSI.IN2MM
    M2NMI = 1 / ImperialToSI.NMI2M
    MPS2FPM = 1 / ImperialToSI.FPM2MPS
    MSQ2FTSQ = 1 / ImperialToSI.FTSQ2MSQ
    MPS2FPS = 1 / ImperialToSI.FPS2MPS
    KW2BHP = 1 / ImperialToSI.BHP2KW
    KG2GAL = 1 / ImperialToSI.GAL2KG

    @staticmethod
    def meters_to_feet(meters):
        """Convert from meters to feet."""
        return SIToImperial.M2FT * meters

    @staticmethod
    def kilograms_to_pounds(kilograms):
        """Convert from kilograms to pounds."""
        return SIToImperial.KG2LBM * kilograms

    @staticmethod
    def meters_per_second_to_knots(meters_per_second):
        """Convert from meters per second to knots."""
        return SIToImperial.MPS2KNOT * meters_per_second

    @staticmethod
    def newtons_to_pounds_force(newtons):
        """Convert from newtons to pounds force."""
        return SIToImperial.N2LBF * newtons

    @staticmethod
    def meters_to_inches(meters):
        """Convert from meters to inches."""
        return SIToImperial.M2IN * meters

    @staticmethod
    def millimeters_to_inches(millimeters):
        """Convert from millimeters to inches."""
        return SIToImperial.MM2IN * millimeters

    @staticmethod
    def meters_to_nautical_miles(meters):
        """Convert from meters to nautical miles."""
        return SIToImperial.M2NMI * meters

    @staticmethod
    def meters_per_second_to_feet_per_minute(meters_per_second):
        """Convert from meters per second to feet per minute."""
        return SIToImperial.MPS2FPM * meters_per_second

    @staticmethod
    def square_meters_to_square_feet(square_meters):
        """Convert from square meters to square feet."""
        return SIToImperial.MSQ2FTSQ * square_meters

    @staticmethod
    def meters_per_second_to_feet_per_second(meters_per_second):
        """Convert from meters per second to feet per second."""
        return SIToImperial.MPS2FPS * meters_per_second

    @staticmethod
    def kilowatts_to_brake_horsepower(kilowatts):
        """Convert from kilowatts to brake horsepower."""
        return SIToImperial.KW2BHP * kilowatts

    @staticmethod
    def kilograms_to_gallons(kilograms):
        """Convert from kilograms to US gallons of jet fuel A."""
        return SIToImperial.KG2GAL * kilograms
