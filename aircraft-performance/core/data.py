#!/usr/bin/env python3

from dataclasses import dataclass


@dataclass
class Parabolic:
    """
    Class for parabolic lift-drag polar.
    """

    zero_lift_drag_coefficient: float
    k_factor: float
