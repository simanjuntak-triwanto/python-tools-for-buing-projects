import numpy as np


def lift_drag_polar(CD0, AR, e, CL):
    """
    Compute the lift-drag polar of aircraft.
    Ref: Ruijgrok Eq. 4.36

    Keyword Arguments:
    CD0 -- Zero-lift drag
    AR  -- Aspect ratio
    e   -- Oswald efficiency
    CL  -- A/C lift coefficient
    """
    k = np.pi * AR * e
    CD = CD0 + k * CL ** 2

    return CD


def lift_drag_polar2(CD0, k, CL):
    """
    Compute the lift-drag polar of aircraft.
    Ref: Ruijgrok Eq. 4.36

    Keyword Arguments:
    CD0 -- Zero-lift drag
    k   -- Gradient lift-drag polar
    """
    CD = CD0 + k * CL ** 2

    return CD


#  TODO Use k = (1/ (pi * AR * e))


def aero_max_ratios(CD0, AR, e):
    """
    Computes the aerodynamics ratios:
    - (CL/CD)max
    - (CL3/CD2)max
    - (CL/CD2)max

    Ref: Rijgrook Eqs. 4.42, 4.46, 4.50
    Keyword Arguments:
    CD0 -- Zero-lift drag
    AR  -- Aspect ratio
    e   -- Oswald efficiency
    """
    CLCD_max = 0.5 * np.sqrt(np.pi * AR * e / CD0)
    CL3CD2_max = (
        3 * (np.sqrt(3) / 16) * np.pi * AR * e * np.sqrt(np.pi * AR * e / CD0)
    )
    CLCD2_max = (3 * np.sqrt(3) / 16) * np.sqrt(np.pi * AR * e / CD0 ** 3)

    return CLCD_max, CL3CD2_max, CLCD2_max


def aero_max_ratios2(CD0, k):
    """
    Computes the aerodynamics ratios for given CD0 and k:
    - (CL/CD)max
    - (CL3/CD2)max
    - (CL/CD2)max

    Ref: Rijgrook Eqs. 4.42, 4.46, 4.50
    Keyword Arguments:
    CD0 -- Zero-lift drag
    AR  -- Aspect ratio
    e   -- Oswald efficiency
    """
    CLCD_max = 0.5 * np.sqrt(1 / (k * CD0))
    CL3CD2_max = 3 * (np.sqrt(3) / 16) * (1 / k) * np.sqrt(1 / (k * CD0))
    CLCD2_max = (3 * np.sqrt(3) / 16) * np.sqrt(1 / (k * CD0 ** 3))

    return CLCD_max, CL3CD2_max, CLCD2_max


def CL_at_CLCDmax(CD0, k):
    """
    Compute CL at CLCDmax.

    Keyword Arguments:
    CDO -- Zero-lift Drag coeeficient
    k   -- Parabolic lift-drag polar constant
    """
    CL = np.sqrt(CD0 / k)

    return CL


def CL_at_CL3CD2max(CD0, k):
    """
    Compute CL at CL3CD2max.

    Keyword Arguments:
    CD0 -- Zero-lift Drag coeeficient
    k   -- Parabolic lift-drag polar constant
    """
    CL = np.sqrt(3 * CD0 / k)

    return CL


def CL_at_CLCD2max(CD0, k):
    """
    Compute CL at CL3CD2max.

    Keyword Arguments:
    CD0 -- Zero-lift Drag coeeficient
    k   -- Parabolic lift-drag polar constant
    """
    CL = np.sqrt(CD0 / (3 * k))

    return CL


def alpha_at_CL(alpha0, CL_alpha, CL):
    """
    Return alpha for given CL
    Keyword Arguments:
    alpha0   -- AOA (deg)
    CL_alpha -- CL gradient (1/deg)
    CL       -- CL
    """
    alpha = CL / CL_alpha + alpha0

    return alpha
