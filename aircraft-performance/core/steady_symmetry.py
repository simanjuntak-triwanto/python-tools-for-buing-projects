#!/usr/bin/env python3

import numpy as np


def lift_coefficient_symmetric_flight(
    weight, air_density, air_speed, wing_area, flight_path_angle=0
):
    """
    Calculate the lift coefficient for symmetric flight conditions.

    Parameters:
        weight (float): Weight of the aircraft (N).
        air_density (float): Air density (kg/m^3).
        air_speed (float): Air speed (m/s).
        wing_area (float): Wing area (m^2).
        flight_path_angle (float): Flight path angle (degrees). Default is 0 for level flight.

    Returns:
        float: Lift coefficient.
    """
    lift_coefficient = (
        (weight / wing_area)
        * (2 / air_density)
        * (1.0 / air_speed**2) ** np.cos(np.radians(flight_path_angle))
    )
    return lift_coefficient


def compute_velocity_symmetric_flight(
    weight, wing_area, air_density, lift_coefficient, flight_path_angle=0
):
    """
    Calculate the velocity for symmetric flight conditions.

    Parameters:
        weight (float): Weight of the aircraft (N).
        wing_area (float): Wing area (m^2).
        air_density (float): Air density (kg/m^3).
        lift_coefficient (float): Lift coefficient.
        flight_path_angle (float): Flight path angle (degrees). Default is 0 for level flight.

    Returns:
        float: Velocity (m/s).
    """
    velocity = np.sqrt(
        (weight / wing_area)
        * (2 / air_density)
        * (1 / lift_coefficient)
        * np.cos(np.radians(flight_path_angle))
    )
    return velocity


def drag_symmetric_flight(
    drag_coefficient, lift_coefficient, weight, flight_path_angle=0
):
    """
    Calculate the drag for symmetric flight conditions.

    Parameters:
        drag_coefficient (float): Drag coefficient of the aircraft.
        lift_coefficient (float): Lift coefficient.
        weight (float): Weight of the aircraft (N).
        flight_path_angle (float): Flight path angle (degrees). Default is 0 for level flight.

    Returns:
        float: Drag (N).
    """
    drag = (
        (drag_coefficient / lift_coefficient)
        * weight
        * np.cos(np.radians(flight_path_angle))
    )
    return drag


def power_required_from_drag(air_drag, air_speed):
    """
    Calculate the power required based on drag.

    Parameters:
        air_drag (float): Drag (N).
        air_speed (float): Air speed (m/s).

    Returns:
        float: Power required (watt).
    """
    power_required = air_drag * air_speed
    return power_required


def power_required_clcd_ratio(
    weight,
    wing_area,
    air_density,
    lift_coefficient,
    drag_coefficient,
    flight_path_angle=0,
):
    """
    Calculate the power required based on lift.

    Parameters:
        weight (float): Weight of the aircraft (N).
        wing_area (float): Wing area (m^2).
        air_density (float): Air density (kg/m^3).
        lift_coefficient (float): Lift coefficient.
        drag_coefficient (float): Drag coefficient.
        flight_path_angle (float): Flight path angle (degrees). Default is 0 for level flight.

    Returns:
        float: Power required (watt).
    """
    cd2cl3 = lift_coefficient**3 / (drag_coefficient**2)
    power_required = weight * np.sqrt(
        (weight / wing_area)
        * (2 / air_density)
        * cd2cl3
        * np.cos(np.radians(flight_path_angle)) ** 3
    )
    return power_required


def power_required_from_cd0(
    weight, wing_area, air_density, k_factor, air_speed, zero_lift_drag
):
    """
    Calculate the total power required for symmetric flight.

    Parameters:
        weight (float): Weight of the aircraft (N).
        wing_area (float): Wing area (m^2).
        air_density (float): Air density (kg/m^3).
        k_factor (float): Lift-drag polar gradient.
        air_speed (float): Air speed (m/s).
        zero_lift_drag (float): Zero-lift drag.

    Returns:
        float: Total power required (watt).
    """
    parasitic_power = (
        zero_lift_drag * 0.5 * air_density * air_speed**3 * wing_area
    )
    induced_power = (2 * weight**2 * k_factor) / (
        air_density * air_speed * wing_area
    )
    total_power_required = parasitic_power + induced_power
    return total_power_required


def power_available_from_thrust(thrust, air_speed):
    """
    Calculate the available power.

    Parameters:
        thrust (float): Thrust (N).
        air_speed (float): Air speed (m/s).

    Returns:
        float: Available power (watt).
    """
    power_available = thrust * air_speed
    return power_available
