#!/usr/bin/env python3

import numpy as np

GRAVITY = 9.81


def get_flare_height(landing_stall_speed, gamma_approach=3):
    """
    Calculate flare height during landing maneuver (m).

    Keyword Arguments:
    stall_seed_landing_conf    -- (m/s)
    gamma_approach (3 deg)     -- Flight path angle during approach
    """
    return (
        0.1512
        * 3.280839895013123  # additional constant conversion from imperial to metric
        * landing_stall_speed**2
        * (1 - np.cos(np.deg2rad(gamma_approach)))
    )


def approach_distance(h_obstacle, flare_height, gamma_approach):
    """
    Calculate the approach distance (m) during landing maneuver.

    Keyword Arguments:
    h_obstacle        -- obstaclle height (m)
    flare_height      -- flare height (m)
    gamma_approach (3 deg)     -- flight path angle during approach (deg)
    """
    gamma_radians = np.deg2rad(gamma_approach)
    return (h_obstacle - flare_height) / np.tan(gamma_radians)


def flare_distance(landing_stall_speed, gamma_approach):
    """
    Calculate flare distance (m) during landing maneuver.

    Keyword Arguments:
    landing_stall_speed --  (m/s)
    gamma_approach      --  flight path angle during approach (deg)
    """
    return (
        0.1512
        * 3.280839895013123  # additional constant conversion from imperial to metric
        * landing_stall_speed**2
        * np.sin(np.radians(gamma_approach))
    )


def free_roll_distance(touch_down_speed, size="large"):
    """
    Calculate free-roll distance during landing maneuver.

    Keyword Arguments:
    touch_down_speed  -- Touchdown speed (m/s)
    size -- Size of the aircraft ("large" or "small"), default is "large"
    (default "large")
    """
    free_roll_time = 3 if size == "large" else 1

    return touch_down_speed * free_roll_time


def braking_distance(
    breaking_speed, landing_weight, thrust, landing_drag, landing_lift, miu
):
    """
    Calculate braking distance (m) during landing maneuver.

    Keyword Arguments:
    breaking_speed   -- Braking speed (m/s)
    landing_weight   -- Landing weight (Newtons)
    thrust           -- Thrust (Newtons)
    landing_drag     -- Drag during landing (Newtons)
    landing_lift     -- Lift during landing (Newtons)
    miu              -- Airfield friction coefficient
    """
    numerator = breaking_speed**2 * landing_weight
    denominator = (
        2
        * GRAVITY
        * (thrust - landing_drag - miu * (landing_weight - landing_lift))
    )

    return -numerator / denominator


def landing_distance(
    landing_weight,
    landing_thrust,
    landing_drag,
    landing_lift,
    h_obstacle,
    landing_stall_speed,
    gamma_approach,
    miu,
    size="large",
):
    """
    Calculate the total landing distance during a landing maneuver (m).

    Keyword Arguments:
    landing_weight (float) -- Landing weight (Newtons)
    landing_thrust (float) -- Thrust during landing (Newtons)
    landing_drag (float) -- Drag during landing (Newtons)
    landing_lift (float) -- Lift during landing (Newtons)
    h_obstacle (float) --  Height of the obstacle (m)
    landing_stall_speed (float) -- Stall speed during landing configuration (m/s)
    gamma_approach (float) -- Flight path angle during approach (degrees)
    miu (float) -- Airfield friction coefficient
    size (str) --  Size of the aircraft ("large" or "small"), default is "large"
    """
    braking_speed = 1.1 * landing_stall_speed

    h_f = get_flare_height(landing_stall_speed, gamma_approach)
    s_a = approach_distance(h_obstacle, h_f, gamma_approach)
    s_f = flare_distance(landing_stall_speed, gamma_approach)

    s_fr = free_roll_distance(braking_speed, size)
    s_br = braking_distance(
        braking_speed,
        landing_weight,
        landing_thrust,
        landing_drag,
        landing_lift,
        miu,
    )
    s_landing = s_a + s_f + s_fr + s_br

    return s_landing
