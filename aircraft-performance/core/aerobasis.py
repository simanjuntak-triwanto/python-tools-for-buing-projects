import numpy as np


def lift_drag_polar_using_AR(
    zero_lift_drag, aspect_ratio, oswald_efficiency, lift_coefficient
):
    """
    Compute the lift-drag polar of an aircraft.

    Parameters:
        zero_lift_drag (float): Zero-lift drag coefficient (CD0).
        aspect_ratio (float): Aspect ratio of the wing (AR).
        oswald_efficiency (float): Oswald efficiency factor (e).
        lift_coefficient (float): Lift coefficient (CL).

    Returns:
        float: Total drag coefficient (CD).
    """
    k_factor = np.pi * aspect_ratio * oswald_efficiency
    total_drag_coefficient = zero_lift_drag + k_factor * lift_coefficient**2

    return total_drag_coefficient


def lift_drag_polar_using_k_factor(zero_lift_drag, k_factor, lift_coefficient):
    """
    Compute the lift-drag polar of an aircraft using gradient lift-drag polar.

    Parameters:
        zero_lift_drag (float): Zero-lift drag coefficient (CD0).
        k_factor (float): Gradient lift-drag polar.
        lift_coefficient (float): Lift coefficient (CL).

    Returns:
        float: Total drag coefficient (CD).
    """
    total_drag_coefficient = zero_lift_drag + k_factor * lift_coefficient**2

    return total_drag_coefficient


def aero_max_ratios_using_AR(zero_lift_drag, aspect_ratio, oswald_efficiency):
    """
    Compute the aerodynamic ratios for given parameters.

    Parameters:
        zero_lift_drag (float): Zero-lift drag coefficient (CD0).
        aspect_ratio (float): Aspect ratio of the wing (AR).
        oswald_efficiency (float): Oswald efficiency factor (e).

    Returns:
        tuple: (CL/CD)max, (CL^3/CD^2)max, (CL/CD^2)max.
    """
    CL_CD_max = 0.5 * np.sqrt(
        np.pi * aspect_ratio * oswald_efficiency / zero_lift_drag
    )
    CL3_CD2_max = (
        3
        * (np.sqrt(3) / 16)
        * np.pi
        * aspect_ratio
        * oswald_efficiency
        * np.sqrt(np.pi * aspect_ratio * oswald_efficiency / zero_lift_drag)
    )
    CL_CD2_max = (3 * np.sqrt(3) / 16) * np.sqrt(
        np.pi * aspect_ratio * oswald_efficiency / zero_lift_drag**3
    )

    return CL_CD_max, CL3_CD2_max, CL_CD2_max


def aero_max_ratios_using_k_factor(zero_lift_drag, k_factor):
    """
    Compute the aerodynamic ratios for given zero-lift drag and k-factor.

    Parameters:
        zero_lift_drag (float): Zero-lift drag coefficient (CD0).
        k_factor (float): Parabolic lift-drag polar constant.

    Returns:
        tuple: (CL/CD)max, (CL^3/CD^2)max, (CL/CD^2)max.
    """
    CL_CD_max = 0.5 * np.sqrt(1 / (k_factor * zero_lift_drag))
    CL3_CD2_max = (
        3
        * (np.sqrt(3) / 16)
        * (1 / k_factor)
        * np.sqrt(1 / (k_factor * zero_lift_drag))
    )
    CL_CD2_max = (3 * np.sqrt(3) / 16) * np.sqrt(
        1 / (k_factor * zero_lift_drag**3)
    )

    return CL_CD_max, CL3_CD2_max, CL_CD2_max


def compute_CL_at_CLCDmax(zero_lift_drag, k_factor):
    """
    Compute CL at (CL/CD)max.

    Parameters:
        zero_lift_drag (float): Zero-lift drag coefficient (CD0).
        k_factor (float): Parabolic lift-drag polar constant.

    Returns:
        float: Lift coefficient (CL) at (CL/CD)max.
    """
    CL = np.sqrt(zero_lift_drag / k_factor)

    return CL


def compute_CL_at_CL3CD2max(zero_lift_drag, k_factor):
    """
    Compute CL at (CL^3/CD^2)max.

    Parameters:
        zero_lift_drag (float): Zero-lift drag coefficient (CD0).
        k_factor (float): Parabolic lift-drag polar constant.

    Returns:
        float: Lift coefficient (CL) at (CL^3/CD^2)max.
    """
    CL = np.sqrt(3 * zero_lift_drag / k_factor)

    return CL


def compute_CL_at_CLCD2max(zero_lift_drag, k_factor):
    """
    Compute CL at (CL/CD^2)max.

    Parameters:
        zero_lift_drag (float): Zero-lift drag coefficient (CD0).
        k_factor (float): Parabolic lift-drag polar constant.

    Returns:
        float: Lift coefficient (CL) at (CL/CD^2)max.
    """
    CL = np.sqrt(zero_lift_drag / (3 * k_factor))

    return CL


def compute_alpha_at_CL(alpha_0, CL_alpha, CL):
    """
    Return the angle of attack for a given lift coefficient.

    Parameters:
        alpha_0 (float): Angle of attack at zero lift (AOA).
        CL_alpha (float): Lift coefficient gradient (1/deg).
        CL (float): Lift coefficient (CL).

    Returns:
        float: Angle of attack (alpha).
    """
    alpha = CL / CL_alpha + alpha_0

    return alpha
