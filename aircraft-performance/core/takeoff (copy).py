#!/usr/bin/env python3

"""
Reference. Gudmusson, 2022, Chapter 18
"""


import numpy as np

g_si = 9.81  # m/s^2
g_imp = 32.174  # ft/s^2

# a = 8.74  # ft/s^2
# v = 120  # ft/s


# p = 310  # BHP
# eta_j = 0.65  # Propeller efficiency
# V1 = 100  # ft/s
# V2 = 120  # ft/s
# W = 3400  # lbf
#


# W = 21500  # lbf
# S = 311.6  # ft2
# CLT0 = 0.90
# CDT0 = 0.045
# CLmax = 1.65
# # T at V R /√2 = 7000 lbf
# T = 7000
# miu = 0.02
# rho = 0.002378

# W = 3400
# PBHP = 310  # bhp
# S = 144.9  # ft^2
# CLmax = 1.69
# CLTO= 0.5
# CDTO = 0.0417
# miu = 0.04
# eta_j = 0.5


# rho = 1.225

# W = 15123.95
# PBHP = 228005
# S = 13.46165
# CLmax = 1.69
# CLTO = 0.5
# CDTO = 0.0417
# miu = 0.04
# eta_j = 0.5


def dist_const_accel_imp(accel, vf, vi=0):
    """
    Approximate the ground roll distance when average acceleration
    and airspeed is known; the units must be consistent.

    Keyword Arguments:
    accel -- accelarition (m/s^2 or ft/s^2)
    vf   -- final speed (m/s or ft/s)
    vi   -- initial speed (m/s or ft/s)
    """
    SG = vf**2 / (2 * accel)

    return SG


def thrust_prop_imp(eta_j, php, vel):
    """
    (Imperial Unit) Compute the thrust of propeller enginer for a given airspeed.

    Keyword Arguments:
    eta_j -- propeller efficiency
    php   -- power (HP)
    vel   -- airspeed (ft/s)
    """
    T = eta_j * 550 * php / vel

    return T


def thrust_prop_si(eta_j, power, vel):
    """
    (Imperial Unit) Compute the thrust of propeller enginer for a given airspeed.
    Keyword Arguments:

    eta_j -- propeller efficiency
    power -- power (Watt)
    vel   -- airspeed (m/s)
    """
    T = eta_j * power / vel

    return T


def rotation_speed(W, rho, S, CLmax):
    """
    Calculate rotation speed during take-off.

    Keyword Arguments:
    W     -- aircraft weight (N or lbf)
    rho   -- air density (kg/m^3 or lbs/ft^3)
    S     -- wing area (m^2 pr ft^2)
    CLmax -- maximum CL
    """
    VR = 1.556 * np.sqrt(W / (rho * S * CLmax))

    return VR


def ground_roll_distance_T_imp(W, rho, S, CLmax, CLTO, CDTO, T, miu):
    """
    Computer the ground roll distance (feet) and acceleration (ft/s^2)
    when thrust is given in imperial units.

    Keyword Arguments:
    W     -- aircraft weight (lbf)
    rho   -- air density (slug/ft^3)
    S     -- wing area (ft^2)
    CLmax -- maximum CL
    T     -- thrust at VR/sqrt(2) (lbf)
    miu   -- ground rolling resistance
    """
    VR = rotation_speed(W, rho, S, CLmax)

    VR_reduced = VR / np.sqrt(2)

    D = 0.5 * rho * VR_reduced**2 * S * CDTO
    L = 0.5 * rho * VR_reduced**2 * S * CLTO

    dVdt = (g_imp / W) * (T - D - miu * (W - L))

    SG = dist_const_accel_imp(dVdt, VR)

    return SG


def ground_roll_distance_P_imp(W, rho, S, CLmax, CLTO, CDTO, P, eta_j, miu):
    """
    Compute the ground roll distance (feet) and acceleration (ft/s^2)
    when power is given in SI  units.

    Keyword Arguments:
    W     -- aircraft weight (lbf)
    rho   -- air density (slug/ft^3)
    S     -- wing area (ft^2)
    CLmax -- maximum CL
    P     -- power (BHP)
    eta_j -- propeller efficiency
    miu   -- ground rolling resistance
    """
    VR = rotation_speed(W, rho, S, CLmax)

    VR_reduced = VR / np.sqrt(2)

    Th = thrust_prop_imp(eta_j, P, VR_reduced)

    D = 0.5 * rho * VR_reduced**2 * S * CDTO
    L = 0.5 * rho * VR_reduced**2 * S * CLTO

    dVdt = (g_imp / W) * (Th - D - miu * (W - L))

    SG = dist_const_accel_imp(dVdt, VR)

    return SG


def ground_roll_distance_T_si(W, rho, S, CLmax, CLTO, CDTO, T, miu):
    """
    Compute the ground roll distance (meter) and acceleration (m/s^2)
    when thrust is given in SI units.

    Keyword Arguments:
    W     -- aircraft weight (N)
    rho   -- air density (kg/m^3)
    S     -- wing area (m^2)
    CLmax -- maximum CL
    T     -- thrust at VR/sqrt(2) (N)
    miu   -- ground rolling resistance
    """
    VR = rotation_speed(W, rho, S, CLmax)

    VR_reduced = VR / np.sqrt(2)

    D = 0.5 * rho * VR_reduced**2 * S * CDTO
    L = 0.5 * rho * VR_reduced**2 * S * CLTO

    dVdt = (g_si / W) * (T - D - miu * (W - L))

    SG = dist_const_accel_imp(dVdt, VR)

    return SG


def ground_roll_distance_P_si(W, rho, S, CLmax, CLTO, CDTO, P, eta_j, miu):
    """
    Compute the ground roll distance (meter) and acceleration (m/s^2)
    when thrust is given in SI units.

    Keyword Arguments:
    W     -- aircraft weight (N)
    rho   -- air density (kg/m^3)
    S     -- wing area (m^2)
    CLmax -- maximum CL
    P     -- power (watt))
    eta_j -- propeller efficiency
    miu   -- ground rolling resistance
    """
    VR = rotation_speed(W, rho, S, CLmax)

    VR_reduced = VR / np.sqrt(2)

    D = 0.5 * rho * VR_reduced**2 * S * CDTO
    L = 0.5 * rho * VR_reduced**2 * S * CLTO

    Th = thrust_prop_si(eta_j, P, VR_reduced)

    dVdt = (g_si / W) * (Th - D - miu * (W - L))

    SG = dist_const_accel_imp(dVdt, VR)

    return SG


def time_to_liftoff(SG, accell):
    """
    Calculate time to lift-off (second).

    Keyword Arguments:
    SG     -- ground roll distance (ft or meter)
    accell -- accelelation (ft/s^ or m/s^2)
    """
    t_lof = np.sqrt(2 * SG / accell)

    return t_lof


def thrust_average_jet(Tmax, BPR):
    """
    Compute the average of thrust for jet engine (lbf or N);
    the unit must be consistent.

    Keyword Arguments:
    Tmax -- maximum static thrust
    BPR  -- turbofan bypass ratio
    """
    T_avg = 0.75 * Tmax * (5 + BPR) / (4 + BPR)

    return T_avg


def thrust_average_prop_imp(Dp, Pmax, Ne, sigma):
    """
    Compute the average of thrust for turboprop in imperial units.

    Keyword Arguments:
    Dp    -- propeller diameter (ft)
    Pmax  -- maximum engine power, in BHP
    Ne    -- number of engines
    sigma -- density ratio

    Kp :
    = 5.75 for P max in BHP
    = 0.321 for P max in kg*m/s
    """
    Kp = 5.75

    a = (sigma * Ne * Dp**2 / Pmax) ** (1 / 3)

    Tbar = Kp * Pmax * a

    return Tbar


def thrust_average_prop_si(Dp, Pmax, Ne, sigma):
    """
    Compute the average of thrust (Newton) for turboprop in SI units.

    Keyword Arguments:
    Dp    -- propeller diameter (m)
    Pmax  -- maximum total engine power, in Watt.
    Ne    -- number of engines
    sigma -- density ratio

    Kp :
    = 5.75 for P max in BHP
    = 0.321 for P max in kg*m/s
    """
    Kp = 0.321

    Pmax = Pmax / g_si  # Convert power to kg*m/s
    alpha = ((sigma * Ne * Dp**2) / (Pmax)) ** (1 / 3)

    Tbar = Kp * Pmax * alpha * g_si

    return Tbar


def distance_bfl_jet_imp(
    WTO,
    S,
    rho,
    CD0,
    k,
    CLmax,
    h_obst,
    Tmax,
    BPR,
    sigma,
    delta_sto=655,
):
    """
    Compute balance-field lenght for turbojet aircraft in imperial units (ft).

    Ref. Gudmunsoon and Torrenbeek

    Keyword Arguments:
    WTO          -- maximum take-off wight (lbf)
    S            -- wing area (ft^2)
    rho          -- air density (slug/ft^3)
    CD0          -- zero-lift drag
    h_obst       -- obstacle height = 35 ft (10.7 m) for commercial
                    jetliners and 50 ft (15.2 m) for GA aircraft,
    Tmax         -- the maximum static thrust all engine
    BPR          -- the turbofan bypass ratio.
    sigma        -- air-density ratio
    delta_sto    -- inertia distance = 655 ft (200 m)

    gamma2_min:
    = 0.024 for 2 engines
    = 0.027 for 3 engines
    = 0.030 for 4 engines

    """
    g = g_imp  # gravity acceleration in imperial unit

    Vs = np.sqrt(2 * WTO / (rho * S * CLmax))
    V2 = 1.2 * Vs

    miu_prime = 0.01 * CLmax + 0.02
    CL2 = 0.694 * CLmax

    CD2 = CD0 + k * CL2**2
    D2 = 0.5 * rho * V2**2 * S * CD2

    Tbar = thrust_average_jet(Tmax, BPR)
    T_oei = 0.5 * Tbar  # approximation
    gamma2 = np.arcsin((T_oei - D2) / WTO)
    gamma2_min = 0.024  # two engines
    delta_gamma2 = gamma2 - gamma2_min

    a = 0.863 / (1 + 2.3 * delta_gamma2)
    b = ((WTO / S) / (rho * g * CL2)) + h_obst
    c = 2.7 + 1 / ((Tbar / WTO) - miu_prime)
    d = delta_sto / np.sqrt(sigma)

    s_bfl = a * b * c + d

    return s_bfl


import numpy as np


def balanced_field_length_si(
    wto: float,
    s: float,
    rho: float,
    cd0: float,
    k: float,
    clmax: float,
    h_obstacle: float,
    tmax: float,
    bpr: float,
    n_engines: int,
    sigma: float = 1.0,
    delta_sto: float = 200.0,
) -> float:
    """
    Calculates the balanced field length (S_BFL) for a turbofan aircraft in SI units (meters).

    Based on the reference by Gudmundsson and  Torrenbeek.

    Args:
        wto (float): Maximum take-off weight (N).
        s (float): Wing area (m^2).
        rho (float): Air density (kg/m^3).
        cd0 (float): Zero-lift drag coefficient.
        k (float): Lift coefficient slope.
        clmax (float): Maximum lift coefficient.
        h_obstacle (float): Obstacle height (m).
        tmax (float): Total maximum static thrust from all engines (N).
        bpr (float): Bypass ratio of the turbofan engine.
        n_engines (int): Number of engines.
        sigma (float, optional): Air density ratio (default: 1.0).
        delta_sto (float, optional): Inertia distance (m) (default: 200.0).

    Returns:
        float: Balanced field length (S_BFL) in meters.
    """

    g = 9.81  # Gravitational acceleration (m/s^2)

    # Minimum climb angle based on number of engines
    gamma2_min = {
        2: 0.024,
        3: 0.027,
        4: 0.030,
    }.get(n_engines, np.nan)

    if np.isnan(gamma2_min):
        raise ValueError("Invalid number of engines. Must be 2, 3, or 4.")

    # Calculate key parameters
    vs = np.sqrt(2 * wto / (rho * s * clmax))
    v2 = 1.2 * vs

    miu_prime = 0.01 * clmax + 0.02
    cl2 = 0.694 * clmax

    cd2 = cd0 + k * cl2**2
    d2 = 0.5 * rho * v2**2 * s * cd2

    # Average thrust
    t_avg = thrust_average_jet(tmax, bpr)

    # One-engine inoperative thrust (approximation)
    t_oei = 0.5 * t_avg

    gamma2 = np.arcsin((t_oei - d2) / wto)
    delta_gamma2 = gamma2 - gamma2_min

    # Coefficients for S_BFL calculation
    a = 0.863 / (1 + 2.3 * delta_gamma2)
    b = ((wto / s) / (rho * g * cl2)) + h_obstacle
    c = 2.7 + 1 / ((t_avg / wto) - miu_prime)
    d = delta_sto / np.sqrt(sigma)

    # Balanced field length
    s_bfl = a * b * c + d

    return s_bfl


def thrust_average_jet(tmax: float, bpr: float) -> float:
    """
    Calculates the average thrust of a turbofan engine (N).

    This function is assumed to be used within the balanced_field_length_si function.

    Args:
        tmax (float): Total maximum static thrust from all engines (N).
        bpr (float): Bypass ratio of the turbofan engine.

    Returns:
        float: Average thrust of a single engine (N).
    """

    return 0.75 * tmax * ((5 + bpr) / (4 + bpr))


def distance_bfl_prop_si(
    WTO,
    S,
    rho,
    CD0,
    k,
    CLmax,
    h_obst,
    Pmax,
    Dp,
    Ne,
    sigma,
    delta_sto=200,
):
    """
    Compute balance-field lenght for turboprop aircraft in SI units (meter).

    Ref. Gudmunsoon and Torrenbeek

    Keyword Arguments:
    WTO          -- maximum take-off wight (N)
    S            -- wing area (m^2)
    rho          -- air density (kg/m^3)
    CD0          -- zero-lift drag
    h_obst       -- obstacle height = 35 ft (10.7 m) for commercial
                    jetliners and 50 ft (15.2 m) for GA aircraft,
    Pmax         -- the maximum engine power
    Dp           -- propeller diameter (m)
    Ne           -- number of engine
    sigma        -- air-density ratio
    delta_sto    -- inertia distance = 655 ft (200 m)

    gamma2_min:
    = 0.024 for 2 engines
    = 0.027 for 3 engines
    = 0.030 for 4 engines

    """
    g = g_si

    if Ne == 2:
        gamma2_min = 0.024
    elif Ne == 3:
        gamma2_min = 0.027
    elif Ne == 4:
        gamma2_min = 0.030

    Vs = np.sqrt(2 * WTO / (rho * S * CLmax))
    V2 = 1.2 * Vs

    miu_prime = 0.01 * CLmax + 0.02
    CL2 = 0.694 * CLmax

    # CD2 = CD0 + k * CL2 ** 2
    CD2 = 0.04513 + 0.04794 * (CL2 - 0.02704) ** 2
    D2 = 0.5 * rho * V2**2 * S * CD2

    Tbar = thrust_average_prop_si(Dp, Pmax, Ne, sigma)
    T_oei = ((Ne - 1) / Ne) * Tbar  # approximation
    gamma2 = np.arcsin((T_oei - D2) / WTO)
    delta_gamma2 = gamma2 - gamma2_min

    a = 0.863 / (1 + 2.3 * delta_gamma2)
    b = ((WTO / S) / (rho * g * CL2)) + h_obst
    c = 2.7 + 1 / ((Tbar / WTO) - miu_prime)
    d = delta_sto / np.sqrt(sigma)

    s_bfl = a * b * c + d

    return s_bfl


# Imperial Units
# WTO = 21500
# rho = 0.002378
# S = 311.6
# CLmax = 1.60
# Tmax = 7000
# BPR = 3.9
# h_obst = 50
# sigma = 1
# deltaSto = 655
# CD0 = 0.045
# k = 0.05236


# test = distance_bfl_jet_imp(
#     WTO, S, rho, CD0, k, CLmax, h_obst, Tmax, BPR, sigma
# )

# # SI Units
# WTO = 95636.764  # 21500
# rho = 1.225  # 0.002378
# S = 28.948587  # 311.6
# CLmax = 1.60
# Tmax = 31137.55  # 7000
# BPR = 3.9
# h_obst = 15.2  # 50
# sigma = 1
# deltaSto = 200  # 655
# CD0 = 0.045
# k = 0.05236

# test2 = distance_bfl_jet_si(
#     WTO, S, rho, CD0, k, CLmax, h_obst, Tmax, BPR, sigma
# )

# # SI Units
# WTO = 150e3 * 9.81
# rho = 1.225
# S = 216.32
# CLmax = 3
# Pmax = 8251e3 * 4  # four engines
# h_obst = 15.2  # 50
# sigma = 1
# deltaSto = 200  # 655
# CD0 = 0.02662
# k = 0.0436
# Ne = 4
# Dp = 5.334
# Dp2 = 17.5  # ft

# eta_j = 0.8


# s_bfl = distance_bfl_prop_si(
#     WTO, S, rho, CD0, k, CLmax, h_obst, Pmax, Dp, Ne, sigma, delta_sto=200,
# )
def transition_distance_prop_imp(
    WTO, S, CD0, k, CLmax, rho, power, eta_j, h_obst
):
    """
    Compute the transition (feet) distance in imperial units.

    Keyword Arguments:
    WTO     -- take-off weight
    S       -- wing area
    CD0     -- zero-lift drag
    k       -- polar-drag coeefient
    CLmax   -- maximum lift-coefficient
    rho     -- air density (slug/ft^3)
    power   -- HP
    eta_j   -- propeller efficiency
    h_obst -- obstacle height = 35 ft (10.7 m) for commercial
               jetliners and 50 ft (15.2 m) for GA aircraft,
    """
    Vs = np.sqrt(2 * WTO / (rho * S * CLmax))
    n_tr = 1.1903

    Vtr = 1.15 * Vs
    CL_vtr = 2 * WTO / (rho * Vtr**2 * S)
    CD_vtr = CD0 + k * CL_vtr**2

    LD_rat = CL_vtr / CD_vtr

    T = thrust_prop_imp(eta_j, power, Vtr)

    gamma_c = np.arcsin((T / WTO) - (1 / LD_rat))

    R_tr = Vtr**2 / (g_imp * (n_tr - 1))

    h_tr = R_tr * (1 - np.cos(gamma_c))

    if h_tr < h_obst:
        s_tr = R_tr * np.sin(gamma_c)
        s_c = (h_obst - h_tr) / np.tan(gamma_c)
        s_a = s_tr + s_c
    elif h_tr >= h_obst:
        s_tr = np.sqrt(R_tr**2 - (R_tr - h_obst) ** 2)
        s_a = s_tr

    return s_a


def transition_distance_prop_si(
    WTO, S, CD0, k, CLmax, rho, power, eta_j, h_obst
):
    """
    Compute the transition (meter) distance in si units.

    Keyword Arguments:
    WTO     -- take-off weight
    S       -- wing area
    CD0     -- zero-lift drag
    k       -- polar-drag coeefient
    CLmax   -- maximum lift-coefficient
    rho     -- air density (slug/ft^3)
    power   -- HP
    eta_j   -- propeller efficiency
    h_obst -- obstacle height = 35 ft (10.7 m) for commercial
               jetliners and 50 ft (15.2 m) for GA aircraft,
    """
    Vs = np.sqrt(2 * WTO / (rho * S * CLmax))
    n_tr = 1.1903

    Vtr = 1.15 * Vs
    CL_vtr = 2 * WTO / (rho * Vtr**2 * S)
    CD_vtr = CD0 + k * CL_vtr**2

    LD_rat = CL_vtr / CD_vtr

    T = thrust_prop_si(eta_j, power, Vtr)

    gamma_c = np.arcsin((T / WTO) - (1 / LD_rat))

    R_tr = Vtr**2 / (g_si * (n_tr - 1))

    h_tr = R_tr * (1 - np.cos(gamma_c))

    if h_tr < h_obst:
        s_tr = R_tr * np.sin(gamma_c)
        s_c = (h_obst - h_tr) / np.tan(gamma_c)
        s_a = s_tr + s_c
    elif h_tr >= h_obst:
        s_tr = np.sqrt(R_tr**2 - (R_tr - h_obst) ** 2)
        s_a = s_tr

    return s_a


def rotation_distance_si(WTO, rho, S, CLmax, size="large"):
    """
    Compute the rotation distance (m) in SI unit, by using
    V_LOF and estimated  rotation time.

    Keyword Arguments:
    WTO   -- take-off weight (N)
    S     -- wing area (m^2)
    rho   -- air density (kg/m^3)
    CLmax -- maximum CL
    size  -- small => t_rot 1 = 1 s
             large => t_rot 1 = 3 s
    """
    if size == "large":
        t_rot = 3
    else:
        t_rot = 1

    VLOF = 1.1 * np.sqrt(2 * WTO / (rho * S * CLmax))

    s_rot = VLOF * t_rot

    return s_rot


def rotation_distance_imp(WTO, S, rho, CLmax, size="large"):
    """
    Compute the rotation distance (ft) in Imperial unit, by using
    V_LOF and estimated  rotation time.

    Keyword Arguments:
    WTO   -- take-off weight (lbf)
    S     -- wing area (ft^2)
    rho   -- air density (slug/ft^3)
    CLmax -- maximum CL
    size  -- small => t_rot 1 = 1 s
             large => t_rot 1 = 3 s
    """
    if size == "large":
        t_rot = 3
    else:
        t_rot = 1

    VLOF = 1.1 * np.sqrt(2 * WTO / (rho * S * CLmax))

    s_rot = VLOF * t_rot

    return s_rot


def takeoff_distance(
    WTO, S, CLmax, CD0, k, rho, power, eta_j, miu, h_obst=15.2, size="large"
):
    """
    Keyword Arguments:
    WTO   --
    S     --
    CLmax --
    CD0   --
    k     --
    rho   --
    power --
    """


def takeoff_distance_prop_imp(
    WTO, S, CLmax, CD0, CLTO, k, rho, power, eta_j, miu, h_obst, size
):
    """
    Computes take-off distance (ft) in imperial unit for turboprop aircraft.

    Keyword Arguments:
    WTO    --
    S      --
    CLmax  --
    CD0    --
    k      --
    rho    --
    power  --
    eta_j  --
    miu    --
    h_obst --
    size   --
    """
    CDTO = CD0 + k * CLTO**2

    s_g = ground_roll_distance_P_imp(
        WTO, rho, S, CLmax, CLTO, CDTO, power, eta_j, miu
    )

    s_rot = rotation_distance_imp(WTO, S, rho, CLmax, size=size)

    s_tr = transition_distance_prop_imp(
        WTO, S, CD0, k, CLmax, rho, power, eta_j, h_obst
    )

    s_to = s_g + s_tr + s_rot

    return s_to


def takeoff_distance_prop_si(
    WTO, S, CLmax, CD0, CLTO, k, rho, power, eta_j, miu, h_obst, size
):
    """
    Computes take-off distance (meter) in imperial unit for turboprop aircraft.

    Keyword Arguments:
    WTO    --
    S      --
    CLmax  --
    CD0    --
    k      --
    rho    --
    power  --
    eta_j  --
    miu    --
    h_obst --
    size   --
    """
    CDTO = CD0 + k * CLTO**2
    s_g = ground_roll_distance_P_si(
        WTO, rho, S, CLmax, CLTO, CDTO, power, eta_j, miu
    )

    s_rot = rotation_distance_si(WTO, S, rho, CLmax, size=size)

    s_tr = transition_distance_prop_si(
        WTO, S, CD0, k, CLmax, rho, power, eta_j, h_obst
    )

    s_to = s_g + s_tr + s_rot

    return s_to


# # Imp
# WTO = 3400
# S = 144.9
# CLmax = 1.69
# CD0 = 0.0350
# k = 0.04207
# power = 310
# eta_j = 0.5
# h_obst = 50
# rho = 0.002378
# miu = 0.04


# SI

# WTO = 15123.95
# S = 13.46165
# CLmax = 1.69
# CD0 = 0.0350
# k = 0.04207
# power = 228005
# eta_j = 0.5
# h_obst = 15.2
# miu = 0.04
# rho = 1.225

# test5 = takeoff_distance_prop_si(
#     WTO, S, CLmax, CD0, k, rho, power, eta_j, miu, h_obst, size="large"
# )

# test = transition_distance_prop_imp(
#     WTO, S, CD0, k, CLmax, rho, power, eta_j, h_obst
# )

# test = transition_distance_prop_si(
#     WTO, S, CD0, k, CLmax, rho, power, eta_j, h_obst
# )


# Vs = 108.0

# Vs = np.sqrt(2 * WTO / (rho * S * CLmax))
# n_tr = 1.1903

# Vtr = 1.15 * Vs
# CL_vtr = 2 * WTO / (rho * Vtr ** 2 * S)
# CD_vtr = CD0 + k * CL_vtr ** 2

# LD_rat = CL_vtr / CD_vtr

# T = 825
# T = thrust_prop_imp(eta_j, power, Vtr)

# gamma_c = np.arcsin((T / WTO) - (1 / LD_rat))

# R_tr = Vtr ** 2 / (g_imp * (n_tr - 1))

# h_tr = R_tr * (1 - np.cos(gamma_c))

# if h_tr < h_obst:
#     s_tr = R_tr * np.sin(gamma_c)
#     s_c = (h_obst - h_tr) / np.tan(gamma_c)
#     s_a = s_tr + s_c
# elif h_tr >= h_obst:
#     s_tr = np.sqrt(R_tr ** 2 - (R_tr - h_obst) ** 2)
#     s_a = s_tr


# test2 = rotation_distance_imp(WTO, rho, CLmax, size="large")
