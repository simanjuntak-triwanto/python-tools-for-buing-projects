#!/usr/bin/env python3

from core.isamodel import isa
import numpy as np

# Constants
GAMMA = 1.4
TR = 1.072  # Throttle Ratio
T0, p0, RHO0, c0, miu0 = isa(0)


def temperature_ratio(altitude, mach):
    """
    Calculate the temperature ratio based on altitude and Mach number.

    Keyword Arguments:
    altitude    -- Altitude (meters).
    mach -- Mach number.
    """
    T, _, _, _, _ = isa(altitude)
    theta0 = (T / T0) * (1 + (GAMMA - 1) / 2 * mach**2)

    return theta0


def pressure_ratio(altitude, mach):
    """
    Calculate the pressure ratio based on altitude and Mach number.

    Keyword Arguments:
    altitude    -- Altitude (meters).
    mach -- Mach number.
    """
    _, p, _, _, _ = isa(altitude)

    theta0 = (p / p0) * (1 + (GAMMA - 1) / 2 * mach**2) ** (
        GAMMA / (GAMMA - 1)
    )

    return theta0


def maximum_thrust_turbofan(thrust_sl, bpr, altitude, mach, kind="civil"):
    """
    Calculate maximum thrust of tubofan enginer based on altitude,
    bypass ratio, and Mach number.

    Keyword Arguments:
    thrust_sl  -- Static thrust at sea level.
    bpr        -- Bypass ratio.
    altitude   -- Altitude.
    mach       -- Mach number.
    kind       -- Engine type "civil" or "military".
    """
    theta0 = temperature_ratio(altitude, mach)
    delta0 = pressure_ratio(altitude, mach)

    if bpr < 1:
        if kind == "civil":
            if theta0 <= TR:
                thrust = thrust_sl * delta0
            else:
                thrust = thrust_sl * delta0 * (1 - 3.5 * (theta0 - TR) / theta0)
        elif kind == "military":
            if theta0 <= TR:
                thrust = 0.6 * thrust_sl * delta0
            else:
                thrust = (
                    0.6
                    * thrust_sl
                    * delta0
                    * (1 - 3.5 * (theta0 - TR) / theta0)
                )
    elif bpr > 1 and kind == "civil":
        if theta0 <= TR:
            thrust = thrust_sl * delta0 * (1 - 0.49 * np.sqrt(mach))
        else:
            thrust = (
                thrust_sl
                * delta0
                * (
                    1
                    - 0.49 * np.sqrt(mach)
                    - (3 * (theta0 - TR) / (1.5 + mach))
                )
            )
    else:
        raise ValueError("Invalid engine type. Choose 'civil' or 'military'.")

    return thrust


def maximum_thrust_turbojet(thrust_sl, altitude, mach, kind="civil"):
    """
    Calculate maximum thrust of turbojet based on altitude,
    bypass ratio, and Mach number.

    Keyword Arguments:
    thrust_sl  -- Static thrust at sea level
    altitude   -- Altitude (m)
    mach       -- Mach number
    kind       -- "civil" or "military"
    """
    theta0 = temperature_ratio(altitude, mach)
    delta0 = pressure_ratio(altitude, mach)

    if kind == "civil":
        if theta0 <= TR:
            thrust = (
                thrust_sl
                * delta0
                * (1 - 0.3 * (theta0 - 1) - 0.1 * np.sqrt(mach))
            )
        else:
            thrust = (
                thrust_sl
                * delta0
                * (
                    1
                    - 0.3 * (theta0 - 1)
                    - 0.1 * np.sqrt(mach)
                    - 1.5 * (theta0 - TR) / theta0
                )
            )
    elif kind == "military":
        if theta0 <= TR:
            thrust = 0.8 * thrust_sl * delta0 * (1 - 0.16 * np.sqrt(mach))
        else:
            thrust = (
                0.8
                * thrust_sl
                * delta0
                * (
                    1
                    - 0.16 * np.sqrt(mach)
                    - 24 * (theta0 - TR) / ((9 + mach) * theta0)
                )
            )
    else:
        raise ValueError("Invalid engine type. Choose 'civil' or 'military'.")

    return thrust


def maximum_thrust_turboprop(thrust_sl, altitude, mach):
    """
    Calculate maximum thrust of tuborborpop engine based on altitude,
    bypass ratio, and Mach number.

    Keyword Arguments:
    thrust_sl  -- Static thrust at sea level
    altitude   -- Altitude (m)
    mach       -- Mach number
    """
    theta0 = temperature_ratio(altitude, mach)
    delta0 = pressure_ratio(altitude, mach)

    if mach <= 0.1:
        thrust = thrust_sl * delta0
    elif mach > 0.1 and theta0 <= TR:
        thrust = thrust_sl * delta0 * (1 - 0.96 * (mach - 0.1) ** 0.25)
    else:
        thrust = (
            thrust_sl
            * delta0
            * (
                1
                - 0.96 * (mach - 1) ** 0.25
                - 3 * (theta0 - TR) / (8.13 * (mach - 0.1))
            )
        )
    return thrust


def thrust_jet(th0, rho):
    """
    Compute thrust of jet (N).

    Ref. Ruijgrook, Eq. 6.80
    Keyword Arguments:
    th0  -- Thrust at sea level (N)
    rho  -- Air density
    """
    thrust = th0 * (rho / RHO0) ** 0.75

    return thrust


def tsfc_from_density_ratio(theta, mach, mode=6):
    """
    Estimates TSFC (1/h) when density ratio is given for engines developed after 2010.
    This is useful if an engine is not selected yet or unknown.

    Parameters:
        theta (float): Density ratio.
        mach (float): Mach number.
        mode (int): Engine mode. Possible values:
            1: Turboprop
            2: Turbojet military power
            3: Turbojet with afterburner (max w=AB)
            4: Low bypass turbofan military power
            5: Low bypass turbofan with afterburner (max power w/AB)
            6: High bypass turbofan (M < 0.9)

    Returns:
        float: TSFC (1/h).
    """
    # Define TSFC equations for each mode
    tsfc_equations = {
        1: lambda m, t: (0.18 + 0.8 * m) * np.sqrt(t),
        2: lambda m, t: (1.1 + 0.3 * m) * np.sqrt(t),
        3: lambda m, t: (1.5 + 0.23 * m) * np.sqrt(t),
        4: lambda m, t: (0.9 + 0.3 * m) * np.sqrt(t),
        5: lambda m, t: (1.6 + 0.27 * m) * np.sqrt(t),
        6: lambda m, t: (0.45 + 0.54 * m) * np.sqrt(t),
    }

    # Check if mode is valid
    if mode not in tsfc_equations:
        raise ValueError("Invalid engine mode. Please choose a valid mode.")

    # Calculate TSFC using the corresponding equation
    tsfc = tsfc_equations[mode](mach, theta)

    return tsfc
