#!/usr/bin/env python3

import numpy as np
from core.isamodel import isa


def glide_velocity(W, S, rho, CD0, CL, gamma_d):
    """
    Compute airspeed velocity of gliding flight

    Ref. Ruijgrok Eq. 13.7
    Keyword Arguments:
    W       -- Weight (N)
    S       -- Area (m^2)
    rho     -- Air density
    CD0     -- Zero-lift drag coefficient
    CL      -- A/C Lift Coefficient
    gamma_d -- Angle of descent (rad)
    """
    if CL == 0:
        V = glide_terminal(W, S, rho, CD0)
    else:
        V = np.sqrt((W / S) * (2 / rho) * (1 / CL) * np.cos(gamma_d))
    return V


def glide_angle_descent(CD, CL):
    """
    Compute glide angle of descent (rad).

    Ref. Ruijgrok Eq. 13.8
    Keyword Arguments:
    CD -- A/C Drag coefficient
    CL -- A/C Lift coefficient
    """
    gam_d = np.arctan2(CD, CL)

    return gam_d


def glide_RD(W, S, rho, CD, CL, gamma_d, CD0):
    """
    Compute glide rate of descent (m/s) of gliding flight.

    Ref. Ruijgrok Eq. 13.9
    Keyword Arguments:
    W       -- Weight (N)
    S       -- Are of surface (m^2)
    rho     -- Air density (m^3/kg)
    CD      -- A/C Drag coefficient
    CL      -- A/C Lift coeffcient
    gamma_d -- Angle of descent (rad)
    """
    V = glide_velocity(W, S, rho, CD0, CL, gamma_d)

    # RD = np.sqrt(
    #     (W / S) * (2 / rho) * (CD ** 2 / CL ** 3) * np.cos(gamma_d) ** 3
    # )
    RD = V * np.sin(gamma_d)

    return RD


def glide_terminal(W, S, rho, CD0):
    """
    Compute velocity for terminal (Vmax), dive (m/s).

    Ref. Ruijgrok Eq. 13.10
    Keyword Arguments:
    W   -- Weight (N)
    S   -- Are (m^2)
    rho -- Air density (kg/m^3)
    CD0 -- Zero-lift drag coefficient
    """
    Vmax = np.sqrt((W / S) * (2 / rho) * (1 / CD0))

    return Vmax


def glide_time(RD, H):
    """
    Compute glide time (hour) of gliding flight.

    Ref. Ruijgrok Eq. 13.11
    Keyword Arguments:
    RD -- Rate of descent (m/s)
    H  -- Altitude (km)
    """
    time = (H * 100 / RD) / 3600

    return time


def glide_distance(H, gamma_d):
    """
    Compute glide distance (km).

    Ref. Ruijgrok Eq. 13.12
    Keyword Arguments:
    H       -- Altitude (km)
    gamma_d -- angle of descent (rad)
    """
    distance = H / np.tang(gamma_d)

    return distance


def glide_Vh(V, gamma_d):
    """
    Compute the glide horizontal velocity (m/s)

    Ref. Ruijgrok Eq. 13.9
    Keyword Arguments:
    V       -- Glide airspeed velocity
    gamma_d -- Glide angle
    """
    Vh = V * np.cos(gamma_d)

    return Vh


# # Clean configuration
# CD0 = 0.012
# k = 0.02

# W = 4000  # Newton
# S = 10  # m^2
# H = 2000  # m


# T, p, rho, c, miu = isa(H)

# CLS = np.linspace(1.5, 0.0, 10000)

# Vhs = np.array([])
# RDs = np.array([])
# for CL in CLS:
#     CD = CD0 + k * CL ** 2
#     CLCD = CL / CD
#     CL3CD2 = CL ** 3 / CD ** 2
#     gamma_d = glide_angle_descent(CD, CL)
#     gamma_d_deg = np.rad2deg(gamma_d)
#     V = glide_velocity(W, S, rho, CD0, CL, gamma_d)
#     V_km_h = V * 3.6
#     RD = glide_RD(W, S, rho, CD, CL, gamma_d)
#     Vh = glide_Vh(V, gamma_d)
#     Vh_km_h = Vh * 3.6
#     Vhs = np.append(Vhs, Vh)
#     RDs = np.append(RDs, RD)


# import matplotlib.pyplot as plt

# plt.plot(Vhs * 3.6, -RDs * 3.6)
# plt.axes().set_aspect("equal", "datalim")

# plt.show()
