#!/usr/bin/env python3

import numpy as np


def CL_sym_flight(W, rho, V, S, gamma=0):
    """
    Compute the lift coefficient at equilibrium condition -- Lift = Weight.

    Ref: Rijgrook Eqs
    Keyword Arguments:
    W     -- Weight (N)
    rho   -- Air density (kg/m^2)
    V     -- Air speed (m/s)
    S     -- Area of wing (m^2)
    gamma -- (default 0) Flight path angle (deg); zero for level flight
    """
    CL = (W / S) * (2 / rho) * (1.0 / V**2) ** np.cos(gamma)

    return CL


def Vel_sym_flight(W, S, rho, CL, gamma=0):
    """
    Compute the velocity (m/s) at equilibrium condition -- Lift = Weight.

    Ref: Rijgrook Eqs. 9.15
    Keyword Arguments:
    W     -- Weight (N)
    S     -- Area (m^2)
    rho   -- Air density (kg/m^2)
    CL    -- A/C Lift coefficient
    gamma -- (default 0) Flight path angle (deg); zero for level flight
    """
    Vel = np.sqrt((W / S) * (2 / rho) * (1 / CL) * np.cos(gamma))

    return Vel


def drag_sym_flight(CD, CL, W, gamma=0):
    """
    Compute drag of symmetric flight
    Keyword Arguments:
    CD    -- Drag-coefficient of A/C
    CL    -- Lift-coefficient of A/C
    W     -- Weight (N)
    gamma -- (default 0) Flight path angle (deg); zero for level flight
    """
    Drag = (CD / CL) * W * np.cos(gamma)

    return Drag


def power_req(D, V):
    """
    Compute Power Required (watt).

    Ref: Ruijgrook, Eq. 9.9
    Keyword Arguments:
    D -- Drag (N)
    V -- Velocity (m/s)
    """
    Pr = D * V

    return Pr


def power_req2(W, S, rho, CL, CD, gamma=0):
    """
    Compute Power Required (watt).

    Ref: Ruijgrook, Eq. 9.14
    Keyword Arguments:
    W     -- Weight (N)
    S     -- Area of wing (m^2)
    rho   -- Air density (kg/m^2)
    CL    -- Lift-coefficient of A/C
    CD    -- Drag-coefficient of A/C
    gamma -- (default 0)
    """
    Pr = W * np.sqrt(
        (W / S) * (2 / rho) * (CD**2 / CL**3) * np.cos(gamma) ** 3
    )

    return Pr


def power_req3(W, S, rho, k, V, CD0):
    """
    Compute power required for steady symmetric flight.

    Keyword Arguments:
    W   -- Weight (N)
    S   -- Area of wing (m^2)
    rho -- Air density (kg/m^2)
    k   -- lift-drag polar gradient
    V   -- Velocity (m/s)
    CD0 -- Zero-lift drag
    """
    P0 = CD0 * (1 / 2) * rho * V**3 * S
    Pi = (2 * W**2 * k) / (rho * V * S)

    Pr = P0 + Pi

    return Pr


def power_available(Th, V):
    """
    Compute Power Available (watt).

    Ref: Ruijgrook, Eq. 9.8
    Keyword Arguments:
    Th -- Thrust (N)
    V  -- Velocity (m/s)
    """
    Pa = Th * V

    return Pa
