#!/usr/bin/env python3

import numpy as np
from core.isamodel import isa, rho0

from utils.uconverters import nautical_miles_to_meters
import matplotlib.pyplot as plt

# Set matplotlib parameters for better visualization
plt.rcParams["text.usetex"] = True
plt.style.use("bmh")

GRAVITY_ACCELERATION = 9.81


def compute_propeller_range(
    propulsive_efficiency,
    specific_fuel_consumption,
    cl_cd_ratio,
    final_weight_ratio,
):
    """
    Compute the range of an aircraft under propeller propulsion using the Breguet formula.

    Ref. Ruijgrok Eq. 15.13

    Parameters:
    propulsive_efficiency (float): Propulsive efficiency
    specific_fuel_consumption (float): Specific fuel consumption
    cl_cd_ratio (float): CL/CD
    final_weight_ratio (float): Ratio of final weight to initial weight

    Returns:
    float: Range of the aircraft (meter)
    """
    initial_weight_ratio = 1 / final_weight_ratio

    prop_range = (
        (propulsive_efficiency / specific_fuel_consumption)
        * cl_cd_ratio
        * np.log(initial_weight_ratio)
    )
    return prop_range


def compute_propeller_endurance(
    propulsive_efficiency,
    specific_fuel_consumption,
    air_density,
    cl3_cd2_ratio,
    wing_area,
    initial_weight,
    final_weight,
):
    """
    Compute the endurance of an aircraft under propeller propulsion using the Breguet formula.

    Ref. Ruijgrok Eq. 15.14

    Parameters:
    propulsive_efficiency (float): Propulsive efficiency
    specific_fuel_consumption (float): Specific fuel consumption
    air_density (float): Air density
    cl3_cd2_ratio (float): CL^3/CD^2
    wing_area (float): Wing area
    initial_weight (float): Initial weight
    final_weight (float): Final weight

    Returns:
    float: Endurance of the aircraft
    """
    prop_endurance = (
        (propulsive_efficiency / specific_fuel_consumption)
        * np.sqrt(cl3_cd2_ratio / ((1 / wing_area) * (2 / air_density)))
        * (2 / np.sqrt(final_weight) - 2 / np.sqrt(initial_weight))
    )
    return prop_endurance


def compute_propeller_endurance_using_initial_speed(
    propulsive_efficiency,
    specific_fuel_consumption,
    air_density,
    cl_cd_ratio,
    wing_area,
    initial_speed,
    final_weight_ratio,
):
    """
    Compute the endurance of an aircraft under propeller propulsion using the Breguet formula.

    Ref. Ruijgrok Eq. 15.14

    Parameters:
    propulsive_efficiency (float): Propulsive efficiency
    specific_fuel_consumption (float): Specific fuel consumption
    air_density (float): Air density
    cl_cd_ratio (float): CL/CD
    wing_area (float): Wing area
    initial_speed (float): The airspeed at the starting point of the cruising ﬂight
    final_weight_ratio (float): Ratio of initial weight to final weight

    Returns:
    float: Endurance of the aircraft (s)
    """
    initial_weight_ratio = 1 / final_weight_ratio
    prop_endurance = (
        2
        * (propulsive_efficiency / specific_fuel_consumption)
        * cl_cd_ratio
        * (1 / initial_speed)
        * (np.sqrt(initial_weight_ratio) - 1)
    )
    return prop_endurance


def compute_jet_range(
    specific_fuel_consumption,
    wing_area,
    air_density,
    cl_cd2_ratio,
    initial_weight,
    final_weight,
):
    """
    Compute the range of an aircraft under jet propulsion using the Breguet formula.

    Ref. Ruijgrok Eq. 15.22

    Parameters:
    specific_fuel_consumption (float): Specific fuel consumption (1/s)
    wing_area (float): Wing area (m^2)
    air_density (float): Air density (kg/m^3)
    cl_cd2_ratio (float): CL/CD^2
    initial_weight (float): Initial weight (N)
    final_weight (float): Final weight (N)

    Returns:
    float: Range of the aircraft (meter)
    """
    jet_range = (
        (2 / specific_fuel_consumption)
        * np.sqrt(2 / (wing_area * air_density) * cl_cd2_ratio)
        * (np.sqrt(initial_weight) - np.sqrt(final_weight))
    )
    return jet_range


def compute_jet_range_using_initial_speed(
    specific_fuel_consumption,
    wing_area,
    air_density,
    cl_cd_ratio,
    initial_speed,
    final_weight_ratio,
):
    """
    Compute the range of an aircraft under jet propulsion using the Breguet formula.

    Ref. Ruijgrok Eq. 15.22

    Parameters:
    specific_fuel_consumption (float): Specific fuel consumption (1/s)
    wing_area (float): Wing area (m^2)
    air_density (float): Air density (kg/m^3)
    cl_cd_ratio (float): CL/CD
    initial speed (float): Initial speed (m/s)
    final_weight_ratio (float): Ratio of final  weight to initial weight

    Returns:
    float: Range of the aircraft (meter)
    """
    jet_range = (
        2
        * (initial_speed / specific_fuel_consumption)
        * cl_cd_ratio
        * (1 - np.sqrt(final_weight_ratio))
    )
    return jet_range


def compute_jet_range_using_overall_efficiency(
    total_efficiency, cl_cd_ratio, initial_weight, final_weight
):
    """
    Compute the range of an aircraft under jet propulsion using a variant of the Breguet formula.

    Ref. Ruijgrok Eq. 15.29

    Parameters:
    total_efficiency (float): Total efficiency
    cl_cd_ratio (float): CL/CD ratio
    initial_weight (float): Initial weight
    final_weight (float): Final weight

    Returns:
    float: Range of the aircraft (meter)
    """
    heating_value = 4.3e7  # Heating value for hydrocarbon (J/kg)
    gravity = 9.81

    jet_range = (
        total_efficiency
        * (heating_value / gravity)
        * cl_cd_ratio
        * np.log(initial_weight / final_weight)
    )
    return jet_range


def compute_jet_endurance(
    specific_fuel_consumption, cl_cd_ratio, final_weight_ratio
):
    """
    Compute the endurance of an aircraft under jet propulsion using the Breguet formula.

    Ref. Ruijgrok Eq. 15.30

    Parameters:
    specific_fuel_consumption (float): Specific fuel consumption
    cl_cd_ratio (float): CL/CD ratio
    final_weight_ratio (float): Ratio of final weight to initial weight

    Returns:
    float: Endurance of the aircraft (second)
    """
    initial_weight_ratio = 1 / final_weight_ratio

    jet_endurance = (
        (1 / specific_fuel_consumption)
        * cl_cd_ratio
        * np.log(initial_weight_ratio)
    )
    return jet_endurance


def compute_final_weight_ratio_from_endurance_jet(
    specific_fuel_consumption, cl_cd_ratio, jet_endurance
):
    """
    Compute final weight ratio when endurance of a jetis known.

    Keyword Arguments:
    specific_fuel_consumtion -- Specific fuel consumption (1/s)
    cl_cd_ratio              -- CL/CD ratio
    endurance                -- flight endurance (s)
    """
    w1_w2_ratio = np.exp(
        jet_endurance * specific_fuel_consumption / cl_cd_ratio
    )

    return 1 / w1_w2_ratio


def compute_final_weight_ratio_from_range_and_speed_jet(
    specific_fuel_consumption, cl_cd_ratio, initial_speed, jet_range
):
    """
    Compute final weight ratio when the range and initial speed of a jet is known.

    Keyword Arguments:
    specific_fuel_consumption -- Specific fuel consumption (1/s)
    cl_cd_ratio               -- CL/CD
    initial_speed             -- Initial speed (m/s)
    jet_range                 -- Range of the jet (m)
    """
    return (
        1
        - (
            jet_range
            * specific_fuel_consumption
            / (initial_speed * cl_cd_ratio)
        )
    ) ** 2


def compute_initial_weight_from_endurance_jet(
    specific_fuel_consumption, cl_cd_ratio, jet_endurance, final_weight
):
    """
    Compute initial weight from endurance for a jet.

    Keyword Arguments:
    specific_fuel_consumption -- Specific fuel consumption (1/s)
    cl_cd_ratio               -- CL/CD
    jet_endurance             -- Range of the jet (m)
    final_weight              -- Final weight (N)
    """
    final_weight_ratio = compute_final_weight_ratio_from_endurance_jet(
        specific_fuel_consumption, cl_cd_ratio, jet_endurance
    )

    return final_weight * final_weight_ratio


def compute_initial_weight_from_range_jet(
    specific_fuel_consumption,
    wing_area,
    air_density,
    cl_cd2_ratio,
    final_weight,
    jet_range,
):
    """
    Compute initial weight from endurance for a jet.

    Keyword Arguments:
    specific_fuel_consumption -- Specific fuel consumption (1/s)
    wing_are                  -- Wing area (m^2)
    air_density               -- Air density (kg/m^3)
    cl_cd2_ratio              -- CL/CD^2
    final_weight              -- Final weight (N)
    jet_endurance             -- Range of the jet (m)


    """
    p_1 = (2 / specific_fuel_consumption) * np.sqrt(
        2 * cl_cd2_ratio / (wing_area * air_density)
    )
    return ((jet_range / p_1) + np.sqrt(final_weight)) ** 2


def compute_effective_range_jet(
    mtow,
    fuel_weight,
    wing_area,
    cl_cd_ratio,
    cl_cd2_ratio,
    fst_cruising_altitude,
    snd_cruising_altitude,
    specific_fuel_consumption,
):
    """
    Compute nominal cruising distance of BUING (m)

    Keyword Arguments:
    mtow                      -- Maximum takeoff weight (N)
    fuel_weight               -- Total fuel weight (N)
    wing_area                 -- Wing area (m^2)
    clcd_max_ratio            -- Ratio of CL/CD
    clcd2_max_ratio           -- Ratio of CL/CD^2
    fst_cruising_altitude     -- Altitude of the first cruising (m)
    snd_cruising_altitude     -- Altitude of the second cruising (m)
    specific_fuel_consumption -- Specific fuel consumption (1/s) at sea level
    """
    _, _, air_density_1, _, _ = isa(fst_cruising_altitude, 0)
    _, _, air_density_2, _, _ = isa(snd_cruising_altitude, 0)

    w11 = mtow - fuel_weight  # empty weight
    w11_w0_ratio = w11 / mtow

    startup_ratio = 0.990  # Engine Start, Warm-up
    taxy_ratio = 0.990  # Taxy
    takeoff_ratio = 0.995  # Takeoff
    fst_climb_ratio = 0.980  # Climb
    w4_w0_ratio = startup_ratio * taxy_ratio * takeoff_ratio * fst_climb_ratio

    fst_descent = 0.990  # Descent for landing attempt
    snd_climb_ratio = 0.980  # Re-climb
    snd_descent_ratio = 0.990  # Re-descent for landing

    # Loitering 45 minutes at 5000 ft
    loiter_time = 45 * 60
    loiter_ratio = compute_final_weight_ratio_from_endurance_jet(
        specific_fuel_consumption, cl_cd_ratio, loiter_time
    )

    landing_to_shutdown_ratio = 0.992  # Lading, Taxi, Shutdown

    w11_w8_ratio = snd_descent_ratio * loiter_ratio * landing_to_shutdown_ratio

    final_weight_snd_cruising = (1 / w11_w8_ratio) * w11

    # Cruising at 15,000 ft for 100 nmi
    initial_weight_snd_cruising = compute_initial_weight_from_range_jet(
        specific_fuel_consumption,
        wing_area,
        air_density_2,
        cl_cd2_ratio,
        final_weight_snd_cruising,
        nautical_miles_to_meters(100),
    )
    snd_cruise_ratio = final_weight_snd_cruising / initial_weight_snd_cruising

    w11_w5_ratio = (
        fst_descent
        * snd_climb_ratio
        * snd_cruise_ratio
        * snd_descent_ratio
        * loiter_ratio
        * landing_to_shutdown_ratio
    )
    final_weight_fst_cruising = (1 / w11_w5_ratio) * w11

    fst_cruise_ratio = w11_w0_ratio / (w4_w0_ratio * w11_w5_ratio)

    assert (
        fst_cruise_ratio < 1
    ), "No fuel for the frist cruising"  # check if the cruise ratio < 1

    initial_weight_fst_cruising = (
        1 / fst_cruise_ratio
    ) * final_weight_fst_cruising

    effective_range = compute_jet_range(
        specific_fuel_consumption,
        wing_area,
        air_density_1,
        cl_cd2_ratio,
        initial_weight_fst_cruising,
        final_weight_fst_cruising,
    )

    return effective_range


def compute_effective_endurance_jet(
    mtow,
    fuel_weight,
    wing_area,
    cl_cd_ratio,
    cl_cd2_ratio,
    fst_cruising_altitude,
    snd_cruising_altitude,
    specific_fuel_consumption,
):
    """
    Compute nominal cruising distance of BUING (m)

    Keyword Arguments:
    mtow                      -- Maximum takeoff weight (N)
    fuel_weight               -- Total fuel weight (N)
    wing_area                 -- Wing area (m^2)
    clcd_max_ratio            -- Ratio of CL/CD
    clcd2_max_ratio           -- Ratio of CL/CD^2
    fst_cruising_altitude     -- Altitude of the first cruising (m)
    snd_cruising_altitude     -- Altitude of the second cruising (m)
    specific_fuel_consumption -- Specific fuel consumption (1/s) at sea level
    """
    _, _, air_density_1, _, _ = isa(fst_cruising_altitude, 0)
    _, _, air_density_2, _, _ = isa(snd_cruising_altitude, 0)

    w11 = mtow - fuel_weight  # empty weight
    w11_w0_ratio = w11 / mtow

    startup_ratio = 0.990  # Engine Start, Warm-up
    taxy_ratio = 0.990  # Taxy
    takeoff_ratio = 0.995  # Takeoff
    fst_climb_ratio = 0.980  # Climb
    w4_w0_ratio = startup_ratio * taxy_ratio * takeoff_ratio * fst_climb_ratio

    fst_descent = 0.990  # Descent for landing attempt
    snd_climb_ratio = 0.980  # Re-climb
    snd_descent_ratio = 0.990  # Re-descent for landing

    # Loitering 45 minutes at 5000 ft
    loiter_time = 45 * 60
    loiter_ratio = compute_final_weight_ratio_from_endurance_jet(
        specific_fuel_consumption, cl_cd_ratio, loiter_time
    )

    landing_to_shutdown_ratio = 0.992  # Lading, Taxi, Shutdown

    w11_w8_ratio = snd_descent_ratio * loiter_ratio * landing_to_shutdown_ratio

    final_weight_snd_cruising = (1 / w11_w8_ratio) * w11

    # Cruising at 15,000 ft for 100 nmi
    initial_weight_snd_cruising = compute_initial_weight_from_range_jet(
        specific_fuel_consumption,
        wing_area,
        air_density_2,
        cl_cd2_ratio,
        final_weight_snd_cruising,
        nautical_miles_to_meters(100),
    )
    snd_cruise_ratio = final_weight_snd_cruising / initial_weight_snd_cruising

    w11_w5_ratio = (
        fst_descent
        * snd_climb_ratio
        * snd_cruise_ratio
        * snd_descent_ratio
        * loiter_ratio
        * landing_to_shutdown_ratio
    )
    final_weight_fst_cruising = (1 / w11_w5_ratio) * w11

    fst_cruise_ratio = w11_w0_ratio / (w4_w0_ratio * w11_w5_ratio)

    assert (
        fst_cruise_ratio < 1
    ), "No fuel for the frist cruising"  # check if the cruise ratio < 1

    initial_weight_fst_cruising = (
        1 / fst_cruise_ratio
    ) * final_weight_fst_cruising

    effective_endurance = compute_jet_endurance(
        specific_fuel_consumption,
        cl_cd_ratio,
        final_weight_fst_cruising / initial_weight_fst_cruising,
    )

    return effective_endurance


def plot_payload_range_points_jet(
    max_payload,
    mtow,
    nominal_fuel_weight,
    max_fuel_weight,
    wing_area,
    cl_cd_max,
    cl_cd2_max,
    fst_cruising_altitude,
    snd_cruisng_altitude,
    tsfc,
    fname_out="payload_range_jet_simple.pdf",
):
    """
    Function to compute and plot payload points on a payload diagram.

    Args:
    - max_payload: Maximum Payload (N)
    - mtow: Maximum Takeoff Weight (N)
    - nominal_fuel_weight: Nominal Fuel Weight (N)
    - max_fuel_weight: Maximum Fuel Weight Capacity (N)
    - wing_area: Wing Area (m^2)
    - cl_cd_max: Maximum Lift-to-Drag Ratio
    - cl_cd2_max: Lift-to-Drag Ratio at Second Maximum
    - fst_cruising_altitude: First Cruising Altitude (m)
    - snd_cruisng_altitude: Second Cruising Altitude (m)
    - c_t: Specific Fuel Consumption (1/s)
    - fname_out: File name out.

    Returns:
    - None
    """
    operating_empty_weight = mtow - max_payload - nominal_fuel_weight

    # Compute points for payload diagram
    point_a = (0, max_payload)
    point_b = (
        compute_effective_range_jet(
            mtow,
            nominal_fuel_weight,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        ),
        max_payload,
    )
    point_c = (
        compute_effective_range_jet(
            mtow,
            max_fuel_weight,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        ),
        mtow - (operating_empty_weight + max_fuel_weight),
    )
    point_d = (
        compute_effective_range_jet(
            operating_empty_weight + max_fuel_weight,
            max_fuel_weight,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        ),
        0,
    )

    # Plotting
    fig, ax = plt.subplots(figsize=(10, 10))
    plt.plot(
        np.array([point_a[0], point_b[0], point_c[0], point_d[0]]) / 1000,
        np.array([point_a[1], point_b[1], point_c[1], point_d[1]])
        / GRAVITY_ACCELERATION,
        "-o",
    )
    offset_y_axis = 3.0
    for point, label in zip(
        [point_a, point_b, point_c, point_d], ["A", "B", "C", "D"]
    ):
        plt.annotate(
            label,
            (point[0] / 1000, point[1] / GRAVITY_ACCELERATION + offset_y_axis),
        )

    ax.set_xlabel(r"Range (km)")
    ax.set_ylabel(r"Payload (kg)")

    # Save and display plot
    plt.tight_layout()
    fig.savefig(fname_out, dpi=300)
    plt.show()


def plot_payload_range_curve_jet(
    max_payload,
    mtow,
    nominal_fuel_weight,
    max_fuel_weight,
    wing_area,
    cl_cd_max,
    cl_cd2_max,
    fst_cruising_altitude,
    snd_cruisng_altitude,
    tsfc,
    fname_out="payload_range_jet_curve.pdf",
):
    """
    Function to compute and plot payload points on a payload diagram.

    Args:
    - max_payload: Maximum Payload (N)
    - mtow: Maximum Takeoff Weight (N)
    - nominal_fuel_weight: Nominal Fuel Weight (N)
    - max_fuel_weight: Maximum Fuel Weight Capacity (N)
    - wing_area: Wing Area (m^2)
    - cl_cd_max: Maximum Lift-to-Drag Ratio
    - cl_cd2_max: Lift-to-Drag Ratio at Second Maximum
    - fst_cruising_altitude: First Cruising Altitude (m)
    - snd_cruisng_altitude: Second Cruising Altitude (m)
    - c_t: Specific Fuel Consumption (1/s)
    - fname_out: File name out.

    Returns:
    - None
    """
    operating_empty_weight = mtow - max_payload - nominal_fuel_weight

    # compute points for payload diagram
    point_a = (0, max_payload)
    point_b = (
        compute_effective_range_jet(
            mtow,
            nominal_fuel_weight,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        ),
        max_payload,
    )

    point_c = (
        compute_effective_range_jet(
            mtow,
            max_fuel_weight,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        ),
        mtow - (operating_empty_weight + max_fuel_weight),
    )

    point_d = (
        compute_effective_range_jet(
            operating_empty_weight + max_fuel_weight,
            max_fuel_weight,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        ),
        0,
    )

    # segment a -> b
    fuel_weights_ab = np.linspace(
        0.5 * nominal_fuel_weight, nominal_fuel_weight, 10
    )

    ranges_segment_ab = [0]
    for fuel_weight_segment_ab in fuel_weights_ab:
        takeoff_weight = (
            operating_empty_weight + max_payload + fuel_weight_segment_ab
        )
        range_segment_ab = compute_effective_range_jet(
            takeoff_weight,
            fuel_weight_segment_ab,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        )
        ranges_segment_ab.append(range_segment_ab)

    # payload in segment ab (n)
    payload_weights_segment_ab = max_payload * np.ones(
        fuel_weights_ab.shape[0] + 1
    )

    # segment b - > c
    fuel_weights_bc = np.linspace(nominal_fuel_weight, max_fuel_weight, 50)
    ranges_segment_bc = []
    payload_weights_segment_bc = []
    for fuel_weight_segment_bc in fuel_weights_bc:
        fuel_weight_extra = fuel_weight_segment_bc - nominal_fuel_weight
        payload_weight_bc = max_payload - fuel_weight_extra
        payload_weights_segment_bc.append(payload_weight_bc)
        takeoff_weight = (
            operating_empty_weight
            + (max_payload - fuel_weight_extra)
            + fuel_weight_segment_bc
        )
        range_segment_bc = compute_effective_range_jet(
            takeoff_weight,
            fuel_weight_segment_bc,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        )
        ranges_segment_bc.append(range_segment_bc)

    # segment c - > d
    initial_payload_weight_segment_cd = (
        mtow - operating_empty_weight - max_fuel_weight
    )
    payload_weights_segment_cd = np.linspace(
        initial_payload_weight_segment_cd, 0, 50
    )

    ranges_segment_cd = []
    for payload_weight_segment_cd in payload_weights_segment_cd:
        takeoff_weight = (
            operating_empty_weight + max_fuel_weight + payload_weight_segment_cd
        )
        range_segment_cd = compute_effective_range_jet(
            takeoff_weight,
            max_fuel_weight,
            wing_area,
            cl_cd_max,
            cl_cd2_max,
            fst_cruising_altitude,
            snd_cruisng_altitude,
            tsfc,
        )
        ranges_segment_cd.append(range_segment_cd)

    # plotting
    fig, ax = plt.subplots(figsize=(10, 10))
    plt.plot(
        np.array([point_a[0], point_b[0], point_c[0], point_d[0]]) / 1000,
        np.array([point_a[1], point_b[1], point_c[1], point_d[1]])
        / GRAVITY_ACCELERATION,
        "o",
    )

    offset_y_axis = 1200
    plt.annotate(
        "A",
        (point_a[0] / 1000, point_a[1] / GRAVITY_ACCELERATION + offset_y_axis),
    )
    plt.annotate(
        "B",
        (point_b[0] / 1000, point_b[1] / GRAVITY_ACCELERATION + offset_y_axis),
    )

    plt.annotate(
        "C",
        (point_c[0] / 1000, point_c[1] / GRAVITY_ACCELERATION + offset_y_axis),
    )
    plt.annotate(
        "D",
        (point_d[0] / 1000, point_d[1] / GRAVITY_ACCELERATION + offset_y_axis),
    )
    plt.plot(
        np.array(ranges_segment_ab) / 1000,
        payload_weights_segment_ab / GRAVITY_ACCELERATION,
    )
    plt.plot(
        np.array(ranges_segment_bc) / 1000,
        np.array(payload_weights_segment_bc) / GRAVITY_ACCELERATION,
    )
    plt.plot(
        np.array(ranges_segment_cd) / 1000,
        payload_weights_segment_cd / GRAVITY_ACCELERATION,
    )

    ax.set_xlabel(r"Range (km)")
    ax.set_ylabel(r"Payload (kg)")

    # save and display plot
    plt.tight_layout()
    fig.savefig(fname_out, dpi=300)
    plt.show()
