#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from core.takeoff import balanced_field_length
from core.isamodel import isa

# Set matplotlib parameters for better visualization
plt.rcParams["text.usetex"] = True
plt.style.use("bmh")

# Constants
CL_MAX = 1.6  # Maximum coefficient lift for takeoff configuration
GRAVITY_ACCELERATION = 9.81  # Gravity acceleration (m/s^2)

# Aircraft Parameters
MTOW = 95636.764  # Take-Off weight (N)
S = 28.948587  # Wing Area (m^2)
H_OBST = 15.2  # Obstacle height for GA aircraft (m)
N_ENG = 2  # Number of Engines
TMAX = 31137.55  # Maximum static thrust from all engines (N)
BPR = 3.9  # Turbofan bypass ratio
CD0 = 0.045
K = 0.05236
DELTA_STO = 200  # Inertia distance (m)

# Calculate ISA at sea level
[T0, p0, RHO0, c0, miu0] = isa(0)

# Generate altitudes
Hs = np.linspace(0, 1500, 5)

# Set up plot
fig, ax = plt.subplots(figsize=(10, 10))

# Define markers for each altitude
markers = ["o", "v", "^", "x", "8", "+"]

# Plot BFL vs ISA+Temperature for each altitude
for index, H in enumerate(Hs):
    dTs = np.linspace(0, 20, 500)
    s_bfls = []
    marker = markers[index]
    for dT in dTs:
        # Calculate ISA at current altitude and temperature
        [T, p, rho, c, miu] = isa(H, dT)

        # Calculate balanced field length
        s_bfl = balanced_field_length(
            MTOW,
            S,
            rho,
            CD0,
            K,
            CL_MAX,
            H_OBST,
            TMAX,
            BPR,
            N_ENG,
            rho / RHO0,
            DELTA_STO,
        )
        s_bfls.append(s_bfl)

    # Plot BFL vs ISA+Temperature
    ax.plot(
        dTs,
        s_bfls,
        # linestyle="--",
        label=f"H = {int(H)} m",
        # marker=marker,
        # markersize=10,
    )

# Add vertical line at ISA+20K
ax.vlines(
    15,
    1700,
    2100,
    linestyles="dashed",
    label="BFL at T= ISA + 20 K",
    color="tab:red",
)

# Set plot labels and legend
ax.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
ax.set_xlabel(r"ISA + $\Delta T$ (K)")
ax.set_ylabel(r"BFL (m)")

# Adjust layout and save plot
plt.tight_layout()
fig.savefig("bfl_vs_temp_vs_altitude.pdf", dpi=300)

# Show plot
plt.show()
