#!/usr/bin/env python3

import numpy as np

import matplotlib.pyplot as plt
from core.steady_symmetry import (
    compute_velocity_symmetric_flight,
    power_available_from_thrust,
    power_required_from_drag,
)
from core.isamodel import isa
from core.propulsion import maximum_thrust_turbofan
from core.aerobasis import (
    aero_max_ratios_using_k_factor,
    compute_CL_at_CLCD2max,
    compute_CL_at_CL3CD2max,
)
from core.climbing import maximum_level_flight_speed

from utils.uconverters import (
    pounds_to_kilograms,
    square_feet_to_square_meters,
    feet_to_meters,
    pounds_force_to_newtons,
)


# Set matplotlib parameters for better visualization
plt.rcParams["text.usetex"] = True
plt.style.use("bmh")


GRAVITY_ACCCELERATION = 9.81

# Weights
MTOW = 5_399_943.93  # Maximum takeoff weight (N)
FUEL_WEIGHT_NOMINAL = 1_471_500  # Nominal fuel weight (N)
MAX_FUEL_WEIGHT = 1_765_800  # Maximum fuel weight (N)
MAX_PAYLOAD_WEIGHT = 1_912_950  # Maximum payload weight (N)
EMPTY_OPERATING_WEIGHT = 2_015_493.93  # Empty operating weight (N)

# Aerodynamics cruising
WING_AREA = 728.6  # Wing Area (m^2)
K_FACTOR = 0.0419
CD0 = 0.01941

TSFC = 0.5050188 / 3600  # specific fuel consumption (1/s)
# FST_CRUISING_ALTITUDE = feet_to_meters(22000)
# SND_CRUISNG_ALTITUDE = feet_to_meters(15000)

CL_CD_MAX, _, CL_CD2_MAX = aero_max_ratios_using_k_factor(CD0, K_FACTOR)
CL_at_CLCD2max = compute_CL_at_CLCD2max(CD0, K_FACTOR)


# Climbing conditions
AIRCRAFT_WEIGHT = 0.75 * MTOW  # at 65% MTOW

# Propulsion
ENGINE_NAME = "Rolls-Royce Trent-1000"
NUMBER_OF_ENGINE = 4
MAX_THRUST_ONE_ENGINE_ASL = 360_400  # Max thrust from one engine at sea level
MAX_TOTAL_THRUST = (
    NUMBER_OF_ENGINE * MAX_THRUST_ONE_ENGINE_ASL
)  # Maximum static thrust from all engines (N)
BPR = 10  # Bypass Ratio

CL_at_CL3CD2max = compute_CL_at_CL3CD2max(CD0, K_FACTOR)
CD_at_CL3CD2max = CD0 + K_FACTOR * CL_at_CL3CD2max**2


ALTITUDES = np.linspace(0, 12000, 1000)
vels_max = []
rocs_max = []
machs = []
for altitude in ALTITUDES:
    _, _, air_density, sound_speed, _ = isa(altitude, 0)
    velocity_at_symmetric_flight = compute_velocity_symmetric_flight(
        AIRCRAFT_WEIGHT, WING_AREA, air_density, CL_at_CL3CD2max
    )
    drag_at_altitude = (
        0.5
        * air_density
        * velocity_at_symmetric_flight**2
        * WING_AREA
        * CD_at_CL3CD2max
    )

    # Calculate Mach number at altitude
    mach_at_altitude = velocity_at_symmetric_flight / sound_speed

    # Calculate thrust at altitude
    thrust_at_altitude = maximum_thrust_turbofan(
        MAX_TOTAL_THRUST, BPR, altitude, mach_at_altitude
    )

    # Calculate power available and power required
    power_available = power_available_from_thrust(
        thrust_at_altitude, velocity_at_symmetric_flight
    )
    power_required = power_required_from_drag(
        drag_at_altitude, velocity_at_symmetric_flight
    )

    roc_max = (power_available - power_required) / AIRCRAFT_WEIGHT

    _, vel_max = maximum_level_flight_speed(
        AIRCRAFT_WEIGHT,
        WING_AREA,
        thrust_at_altitude,
        CD0,
        K_FACTOR,
        air_density,
    )
    mach = vel_max / sound_speed

    rocs_max.append(roc_max)
    vels_max.append(vel_max)
    machs.append(mach)

# Set up plot
fig1, ax1 = plt.subplots(figsize=(10, 10))
ax1.plot(rocs_max, ALTITUDES)
# Add vertical line at 0.5 m/s
ax1.vlines(
    0.5,
    0,
    ALTITUDES.max(),
    linestyles="dashed",
    label="Service ceiling limit (0.5 m/s)",
    color="tab:red",
)

# Set plot labels and legend
ax1.legend()
ax1.set_xlabel(r"Maximum Rate of Climb (m/s)")
ax1.set_ylabel(r"Altitude (m)")


# Adjust layout and save plot
plt.tight_layout()
fig1.savefig("altitude_vs_roc_max_buing.pdf", dpi=300)

# Set up plot
fig2, ax2 = plt.subplots(figsize=(10, 10))
ax2.plot(vels_max, ALTITUDES)
# Add vertical line at 0.5 m/s


# Set plot labels and legend
ax2.set_xlabel(r"Maximum level flight speed (m/s)")
ax2.set_ylabel(r"Altitude (m)")


# Adjust layout and save plot
plt.tight_layout()
fig2.savefig("altitude_vs_vel_max_buing.pdf", dpi=300)


# Set up plot
fig3, ax3 = plt.subplots(figsize=(10, 10))
ax3.plot(machs, ALTITUDES)
# Add vertical line at 0.5 m/s


# Set plot labels and legend
ax3.set_xlabel(r"Maximum level flight speed (Mach)")
ax3.set_ylabel(r"Altitude (m)")


# Adjust layout and save plot
plt.tight_layout()
fig3.savefig("altitude_vs_mach_max_buing.pdf", dpi=300)


# Show plot
plt.show()
