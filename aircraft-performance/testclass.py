#!/usr/bin/env python3

from data.types import AircraftParameters
import utils.uconverters as convert
from core.takeoff import *

NAME = "Learjet 45"

# Aerodynamics
WING_AREA = convert.square_feet_to_square_meters(144.9)
ASPECT_RATIO = 12
MAX_COEFFICIENT_LIFT = 1.69
DRAG_COEFFICIENT_ZERO_LIFT = 0.0350
INDUCED_DRAG_FACTOR = 0.04207

# Weights
MAX_TAKEOFF_WEIGHT = convert.pounds_force_to_newtons(3400)
EMPTY_WEIGHT = 5000
FUEL_CAPACITY = 2000

# Propulsion
MAX_THRUST = convert.pounds_force_to_newtons(825)

learjet45 = AircraftParameters(
    NAME,
    WING_AREA,
    ASPECT_RATIO,
    MAX_COEFFICIENT_LIFT,
    INDUCED_DRAG_FACTOR,
    DRAG_COEFFICIENT_ZERO_LIFT,
    MAX_TAKEOFF_WEIGHT,
    EMPTY_WEIGHT,
    FUEL_CAPACITY,
    MAX_THRUST,
)

STALL_SPEED = convert.feet_per_second_to_meters_per_second(108)

trans_dist = transition_distance2(learjet45, 1.225, STALL_SPEED)
