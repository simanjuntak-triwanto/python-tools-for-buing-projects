#!/usr/bin/env python3

from dataclasses import dataclass


@dataclass
class AircraftParameters:
    """Class to hold aircraft parameters for computing performance."""

    name: str  # Aircraft model name

    # Aerodynamics
    wing_area: float  # in square meters
    aspect_ratio: float
    max_coefficient_lift: float
    induced_drag_factor: float
    drag_coefficient_zero_lift: float

    # Weights
    max_takeoff_weight: float  # in Newtons
    empty_weight: float  # in Newtons
    fuel_capacity: float  # in Newtons

    # Propulsion
    max_thrust: float  # in Newtons

    def __post_init__(self):
        """Assertion checks for valid parameter values."""
        assert self.wing_area > 0, "Wing area must be greater than zero."
        assert self.aspect_ratio > 0, "Aspect ratio must be greater than zero."
        assert self.empty_weight > 0, "Empty weight must be greater than zero."
        assert (
            self.max_takeoff_weight > 0
        ), "Max takeoff weight must be greater than zero."
        assert self.max_thrust > 0, "Max thrust must be greater than zero."
        assert (
            self.max_coefficient_lift > 0
        ), "Max coefficient of lift must be greater than zero."
