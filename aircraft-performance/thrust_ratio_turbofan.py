#!/usr/bin/env python3

from core.propulsion import maximum_thrust_turbofan
import numpy as np
import matplotlib.pyplot as plt

# Set matplotlib parameters for better visualization
plt.rcParams["text.usetex"] = True
plt.style.use("bmh")


# Propulsion
ENGINE_NAME = "Rolls-Royce Trent-1000"
NUMBER_OF_ENGINE = 4
MAX_THRUST_ONE_ENGINE_ASL = (
    360_400  # 39_000  # Max thrust from one engine at sea level
)
MAX_TOTAL_THRUST = (
    NUMBER_OF_ENGINE * MAX_THRUST_ONE_ENGINE_ASL
)  # Maximum static thrust from all engines (N)
BPR = 10  # Bypass Ratio
TR = 1.07  # NOT SIRE


def plot_thrust_ratio_vs_mach_vs_altitude():
    altitudes = np.linspace(0, 40000, 5)
    machs = np.linspace(0, 1, 2000)

    # Set up plot
    fig, ax = plt.subplots(figsize=(10, 10))

    # Define markers for each altitude
    markers = ["o", "v", "^", "x", "+"]

    # Plot BFL vs ISA+Temperature for each altitude
    for index, altitude in enumerate(altitudes):
        marker = markers[
            index % len(markers)
        ]  # Ensure markers repeat if needed
        thrusts = [
            maximum_thrust_turbofan(
                MAX_THRUST_ONE_ENGINE_ASL, BPR, altitude, mach, kind="civil"
            )
            / MAX_THRUST_ONE_ENGINE_ASL
            for mach in machs
        ]
        ax.plot(
            machs,
            thrusts,
            # linestyle="None",
            label=f"H = {int(altitude)} m",
            # marker=marker,
            # markersize=10,
        )

    # Set plot labels and legend
    ax.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
    ax.set_xlabel(r"Mach Number")
    ax.set_ylabel(r"Thrust Ratio")

    # Adjust layout and save plot
    plt.tight_layout()
    fig.savefig("thrusts_vs_mach_vs_altitude_turbofan.pdf", dpi=300)

    # Show plot
    plt.show()


if __name__ == "__main__":
    plot_thrust_ratio_vs_mach_vs_altitude()
