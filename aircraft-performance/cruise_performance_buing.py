#!/usr/bin/env python3

import numpy as np

from core.aerobasis import aero_max_ratios_using_AR, compute_CL_at_CLCD2max
from core.isamodel import isa
from core.cruise import (
    compute_jet_range,
    compute_jet_endurance,
    compute_effective_range_jet,
)
from utils.uconverters import (
    pounds_to_kilograms,
    square_feet_to_square_meters,
    feet_to_meters,
    pounds_force_to_newtons,
)


GRAVITY = 9.81

WING_AREA = square_feet_to_square_meters(318)  # m^2
WING_SPAN = feet_to_meters(53.3)  # m
MTOW = pounds_to_kilograms(2950) * GRAVITY  # Newton
FUEL_WEIGHT = pounds_force_to_newtons(800)
TOTAL_THRUST = 2 * pounds_force_to_newtons(3650)


C_T = 0.6 / 3600  # specific fuel consumption (1/s)
INITIAL_WEIGHT = MTOW
FINAL_WEIGHT = MTOW - FUEL_WEIGHT
FST_CRUISING_ALTITUDE = feet_to_meters(22000)
SND_CRUISNG_ALTITUDE = feet_to_meters(15000)
_, _, AIR_DENSITY, _, _ = isa(FST_CRUISING_ALTITUDE, 0)
FINAL_WEIGHT_RATIO = FINAL_WEIGHT / INITIAL_WEIGHT


CD0 = 0.02
OSWALD_EFFICIENCY = 0.81
ASPECT_RATIO = WING_SPAN**2 / WING_AREA
K_FACTOR = 1 / (np.pi * ASPECT_RATIO * OSWALD_EFFICIENCY)
CL_CD_MAX, CL3_CD2_MAX, CL_CD2_MAX = aero_max_ratios_using_AR(
    CD0, ASPECT_RATIO, OSWALD_EFFICIENCY
)

CL_at_CLCD2max = compute_CL_at_CLCD2max(CD0, K_FACTOR)


jet_range = compute_jet_range(
    C_T, WING_AREA, AIR_DENSITY, CL_CD2_MAX, INITIAL_WEIGHT, FINAL_WEIGHT
)

jet_endurance = compute_jet_endurance(C_T, CL_CD_MAX, FINAL_WEIGHT_RATIO)


# Weight Fractions During Nominal Mission

buing_range = compute_effective_range_jet(
    MTOW,
    FUEL_WEIGHT,
    WING_AREA,
    CL_CD_MAX,
    CL_CD2_MAX,
    FST_CRUISING_ALTITUDE,
    SND_CRUISNG_ALTITUDE,
    C_T,
)
