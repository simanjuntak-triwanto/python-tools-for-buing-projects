#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import utils.uconverters as convert

from core.isamodel import isa
from core.landing import landing_distance
from core.steady_symmetry import compute_velocity_symmetric_flight
from core.aerobasis import lift_drag_polar_using_k_factor

matplotlib.rcParams["text.usetex"] = True
plt.style.use("bmh")


GRAVITY_ACCELERATION = 9.81

# Weights
MTOW = 5_399_943.93  # Maximum takeoff weight (N)
FUEL_WEIGHT_NOMINAL = 1_471_500  # Nominal fuel weight (N)
MAX_FUEL_WEIGHT = 1_765_800  # Maximum fuel weight (N)
MAX_PAYLOAD_WEIGHT = 1_912_950  # Maximum payload weight (N)
EMPTY_OPERATING_WEIGHT = 2_015_493.93  # Empty operating weight (N)
SIZE = "large"

LANDING_WEIGHT = 0.9 * MTOW  # at 90% MTOW

# Landing condition
LANDING_ALTITUDE = 0
OBSTACLE_HEIGHT = 15.2
MIU = 0.3  # Runway friction coefficient --- breaking on
LANDING_ALTITUDE = 0  # Takeoff altitude
_, _, RHO0, _, _ = isa(LANDING_ALTITUDE)
GAMMA_APPROACH = 3  # Deg, flight path angle when approaching

# Aerodynamics Landing
WING_AREA = 728.6  # Wing Area (m^2)
CL_MAX = 2.7
K_FACTOR = 0.04028
CD0 = 0.05957
STALL_SPEED = compute_velocity_symmetric_flight(
    LANDING_WEIGHT, WING_AREA, RHO0, CL_MAX
)
LANDING_STALL_SPEED = convert.knot_to_meter_per_second(59)
CL_LANDING = 2.5
CD_LANDING = lift_drag_polar_using_k_factor(CD0, K_FACTOR, CL_LANDING)


# Landing speed references
V_REF = 1.3 * LANDING_STALL_SPEED
V_FLR = 1.3 * LANDING_STALL_SPEED
V_TD = 1.1 * LANDING_STALL_SPEED
V_BR = 1.1 * LANDING_STALL_SPEED
V_AVG = V_BR / np.sqrt(2)


# Propulsion
ENGINE_NAME = "Rolls-Royce Trent-1000"
NUMBER_OF_ENGINE = 4
MAX_THRUST_ONE_ENGINE_ASL = 360_400
MAX_TOTAL_THRUST = (
    NUMBER_OF_ENGINE * MAX_THRUST_ONE_ENGINE_ASL
)  # Maximum static thrust from all engines (N)
BPR = 10  # Bypass Ratio

LANDING_THRUST = 0.1 * MAX_TOTAL_THRUST  # Idle thrust for landing (N) at 10%


# Generate altitudes
Hs = np.linspace(0, 1500, 5)

# Set up plot
fig, ax = plt.subplots(figsize=(10, 10))

# Define markers for each altitude
markers = ["o", "v", "^", "x", "8", "+"]

# Plot BFL vs ISA+Temperature for each altitude
for index, landing_altitude in enumerate(Hs):
    dTs = np.linspace(0, 20, 500)
    s_landings = []
    marker = markers[index]
    for dT in dTs:
        # Calculate ISA at current altitude and temperature
        _, _, rho, _, _ = isa(landing_altitude, dT)
        landing_drag = 0.5 * rho * (V_AVG**2) * WING_AREA * CD_LANDING
        landing_lift = 0.5 * rho * (V_AVG**2) * WING_AREA * CL_LANDING

        # Calculate the landing distance
        s_landing = landing_distance(
            LANDING_WEIGHT,
            LANDING_THRUST,
            landing_drag,
            landing_lift,
            OBSTACLE_HEIGHT,
            LANDING_STALL_SPEED,
            GAMMA_APPROACH,
            MIU,
            SIZE,
        )
        s_landings.append(s_landing)

    # Plot BFL vs ISA+Temperature
    ax.plot(
        dTs,
        s_landings,
        # linestyle="--",
        label=f"H = {int(landing_altitude)} m",
        # marker=marker,
        # markersize=10,
    )

# Add vertical line at ISA+20K
ax.vlines(
    15,
    627,
    632,
    linestyles="dashed",
    label="Landing Distance at T= ISA + 15 K",
    color="tab:gray",
)

# Set plot labels and legend
# ax.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
ax.legend()
ax.set_xlabel(r"Temperature (ISA + $\Delta T$), K")
ax.set_ylabel(r"Landing Distance, m")

# Adjust layout and save plot
plt.tight_layout()
fig.savefig("landing_vs_temp_vs_altitude_buing.pdf", dpi=300)

# Show plot
plt.show()
