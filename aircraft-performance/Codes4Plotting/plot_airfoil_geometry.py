#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams["text.usetex"] = True
plt.style.use("bmh")

# Data file name (can be changed for different airfoils)
data_filename = "sc20412.dat"  # Or "npl9510.dat" or "nl722362.dat"

# Load airfoil coordinates from data file
airfoil_data = np.genfromtxt(data_filename, skip_header=3)

# Extract x and y coordinates
x_coordinates = airfoil_data[:, 0]
y_coordinates = airfoil_data[:, 1]

# Find the index where x-coordinate becomes zero for the second time
upper_airfoil_end_index = int(np.where(x_coordinates == 0.0)[0][-1])

# Create a figure and axis for plotting
fig, ax = plt.subplots(figsize=(10, 10))

# Plot upper and lower airfoil sections
ax.plot(x_coordinates[:upper_airfoil_end_index], y_coordinates[:upper_airfoil_end_index], "tab:blue", label="Upper Airfoil")
ax.plot(x_coordinates[upper_airfoil_end_index:], y_coordinates[upper_airfoil_end_index:], "tab:blue", label="Lower Airfoil")
ax.set_aspect("equal")  # Set equal aspect ratio for accurate shape representation

# Set axis labels
ax.set_xlabel(r"$x$")
ax.set_ylabel(r"$y$")

# Add legend to differentiate upper and lower parts
ax.legend()

# Adjust layout to prevent overlapping elements
plt.tight_layout()

# Save plot as PDF with high resolution and tight bounding box
fig.savefig(data_filename[:-4] + ".pdf", dpi=300, bbox_inches="tight")

# Display plot on screen
plt.show()
