# acweights
aceweights is a package aimed to estimate the weight components of an aircraft; estimating weight of the components of an aircraft are usually needed when designing aircraft in conceptual design phase.

acweights implements the following widely used formula to estimate aircraft components.

TODO: 
- [x] Raymer method;
- [x] USAF method;
- [x] Torrenbeek method;
- [x] Cessna Method;
- [ ] Roskam Method;
- [ ] Pretty print output;
- [ ] Save output to spreadsheet template using [pyexcel-ods3](https://github.com/pyexcel/pyexcel-ods3).

## Authors and acknowledgment
- Triwanto Simanjuntak;
- Ressa Octavianty;
- BaDai-50 Design Team.


## License
MIT License
