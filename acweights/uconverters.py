#!/usr/bin/env python3

import numpy as np

# Imperial to SI
FT2M = 0.3048  # feet to meter
LBM2KG = 0.453592  # pound mass to kg
KNOT2MPS = 0.514444  # knot to m/s
LBF2N = 4.44822  # lbf to N
IN2M = 0.0254  # inch to m
IN2MM = 25.4  # inch to mm
NMI2M = 1852  # nautical mile to m
FPM2MPS = 0.00508  # Feet per minute to m/s
FTSQ2MSQ = 0.092903  # Feet square to meter square
FPS2MPS = 0.3048  # Feet per second to m/s
BHP2KW = 0.7457  # BHP to kW
GAL2KG = 3.04  # US Gallons fuel jet A to kg


# SI to imperial
M2FT = 1 / FT2M
KG2LBM = 1 / LBM2KG
MPS2KNOT = 1 / KNOT2MPS
N2LBF = 1 / LBF2N
M2IN = 1 / IN2M
MM2IN = 1 / IN2MM
M2NMI = 1 / NMI2M
MPS2FPM = 1 / FPM2MPS
MSQ2FTSQ = 1 / FTSQ2MSQ
MPS2FPS = 1 / FPS2MPS
KW2BHP = 1 / BHP2KW
KG2GAL = 1 / GAL2KG
