#!/usr/bin/env python

# Statistical method for estimating aircraft weight components using
# formulas developed by Torrenbeek.
# See:

import numpy as np


## -------------------------1. WING----------------------------------------------
def wing_weight(w0, bw, sw, sweep2_wing, nz, tw_max):
    """
    Estimate wing weight using Torrenbeek formula;

    Eq. 8.12

    Keyword Arguments:
    w0           -- Design gross weight (lbf);
    bw           -- Wingspan in (ft);
    SW           -- Trapezoidal wing area in ft2
    wing_sweep_2 -- Wing sweep at 50% MGC;
    nz           -- Ultimate load factor (= 1.5 x limit load factor);
    tw_max       -- Max thickness of the wing root chord in ft.
    """

    w_w = (
        0.00125
        * w0
        * (bw / np.cos(sweep2_wing)) ** 0.75
        * (1 + np.sqrt(6.3 * np.cos(sweep2_wing) / bw))
        * nz**0.55
        * (bw * sw / (tw_max * w0 * np.cos(sweep2_wing))) ** 0.30
    )

    return w_w


## -------------------------2. Horizontal Tail-----------------------------------
def emp_weight(nz, sht, svt):
    """
    Estimate the emmpenage weight using Torenbeek formula.

    Keyword Arguments:
    nz  -- Ultimate load factor (= 1.5 x limit load factor);
    sht -- Trapezoidal HT area in ft2;
    svt -- Trapezoidal VT area in ft2;
    """

    w_emp = 0.04 * (nz * (sht + svt) ** 2) ** 0.75

    return w_emp


## -------------------------3. Vertical Tail-------------------------------------
# Included in empenage

## -------------------------4. Fuselage------------------------------------------
# Torenbeek: No expression given for GA aircraft


## -------------------------5. Main Landing Gear---------------------------------
def mnlg_weight(w0, wing_pos, lg_type, ac_class):
    """
    Estimate main LG weight using torrenbeek formula.

    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    wing_pos -- "low" or "high"
    lg_type  -- "fixed" or "retract"
    ac_class -- "bizjet" or "civil"
    """
    if ac_class == "bizjet":
        A = 33
        B = 0.04
        C = 0.021
        D = 0
    elif ac_class == "civil":
        if lg_type == "fixed":
            A = 20
            B = 0.10
            C = 0.019
            D = 0
        elif lg_type == "retract":
            A = 40
            B = 0.16
            C = 0.019
            D = 1.5 * 1e-5

    if wing_pos == "low":
        w_mnlg = A + B * w0**0.75 + C * w0 + D * w0**1.5
    elif wing_pos == "high":
        w_mnlg = 1.08 * (A + B * w0**0.75 + C * w0 + D * w0**1.5)

    return w_mnlg


## -------------------------6. Nose/Tail Landing Gear----------------------------
def nlg_weight(w0, wing_pos, lg_type, ac_class):
    """
    Estimate tail LG weight using torrenbeek formula.

    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    wing_pos -- "low" or "high"
    lg_type  -- "fixed" or "retract"
    ac_class -- "bizjet" or "civil"
    """
    if ac_class == "bizjet":
        A = 12
        B = 0.06
        C = 0
        D = 0
    elif ac_class == "civil":
        if lg_type == "fixed":
            A = 25
            B = 0
            C = 0.0024
            D = 0
        elif lg_type == "retract":
            A = 20
            B = 0.10
            C = 0
            D = 2.0 * 1e-6

    if wing_pos == "low":
        w_nlg = A + B * w0**0.75 + C * w0 + D * w0**1.5
    elif wing_pos == "high":
        w_nlg = 1.08 * (A + B * w0**0.75 + C * w0 + D * w0**1.5)

    return w_nlg


def tlg_weight(w0, wing_pos, lg_type, ac_class):
    """
    Estimate tail LG weight using torrenbeek formula.

    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    wing_pos -- "low" or "high"
    lg_type  -- "fixed" or "retract"
    ac_class -- "bizjet" or "civil"
    """
    if ac_class == "bizjet":
        A = 0
        B = 0
        C = 0
        D = 0
    elif ac_class == "civil":
        if lg_type == "fixed":
            A = 9
            B = 0
            C = 0.0024
            D = 0
        elif lg_type == "retract":
            A = 5
            B = 0
            C = 0.0031
            D = 0

    if wing_pos == "low":
        w_tlg = A + B * w0**0.75 + C * w0 + D * w0**1.5
    elif wing_pos == "high":
        w_tlg = 1.08 * (A + B * w0**0.75 + C * w0 + D * w0**1.5)

    return w_tlg


## -------------------------7. Nacelle/Cowling Weight----------------------------
def nac_weight(p_or_t_max, n_eng, nac_engine_type):
    """
    Estimate the weight of nacelles or cowlings using Torrenbeek formula.
    For prop engines set tmax = 0, and for jet engines, set pmax=0.

    Keyword Arguments:
    p_or_tmax       -- Maximum rated power per engine in BHP or ESHP/
                       Maximum rated thrust per engine in lbf;;
    n_eng           -- Number of engines
    nac_engine_type -- "stp" (Single-engine tractor propeller), "multihop", "rp" (radial piston),
                   "turboprop", "podjet", "hbpr"
    """
    if nac_engine_type == "stp":
        w_nac = 2.5 * np.sqrt(p_or_t_max)
    elif nac_engine_type == "multihop":
        w_nac = 0.32 * p_or_t_max * n_eng
    elif nac_engine_type == "multirp":
        w_nac = 0.045 * p_or_t_max**1.25 * n_eng
    elif nac_engine_type == "turboprop":
        w_nac = 0.14 * p_or_t_max * n_eng
    elif nac_engine_type == "podjet":
        w_nac = 0.055 * p_or_t_max
    elif nac_engine_type == "hbpr":
        w_nac = 0.065 * p_or_t_max

    return w_nac


## -------------------------8. Uninstalled (Dry) Engine--------------------------
def engine_dry_weight(p_or_t_max, engine_type):
    """
    Estimate uninstalled engine weight when the actual weight are not known.

    Keyword Arguments:
    p_or_t_max  -- If prop engine, then Pmax (BHP), if jet then Tmax (lbf)
    engine_type -- "piston", "prop", "jet"
    """

    if engine_type == "piston":
        w_eng = 50.56 + 1.352 * p_or_t_max
    elif engine_type == "prop":
        w_eng = 71.65 + 0.3658 * p_or_t_max
    elif engine_type == "jet":
        w_eng = 295.5 + 0.1683 * p_or_t_max

    return w_eng


## -------------------------9. Installed Engine----------------------------------
def engine_installed_weight(w_eng, wprop, n_eng, pmax, w_nac):
    """
    Estimate installed engine weight using Torrenbeek formula.

    Table 8.9 of Torrenbeek's book.

    Keyword Arguments:
    w_eng -- Weight of each uninstalled engine in lbf;
    wprop -- Propeller weight (set wprop=0 for jet);
    n_eng -- Number of engines;
    pmax  -- Maximum rated power per engine in BHP;
    w_nac -- Predicted weight of all engine nacelles in lbf.
    """

    w_ei = (w_eng + wprop) * n_eng + 1.03 * n_eng**0.3 * pmax**0.7 + w_nac

    return w_ei


## ------------------------10. Fuel System---------------------------------------
def fuel_sys_weight(
    qtot,
    engine_conf,
    n_tank=1,
    n_eng=1,
):
    """
    Estimate fuel system weight using torrenbeek formula.

    See Torrenbeek (P 286).

    Keyword Arguments:
    qtot        -- Total fuel quantity in US gallons;
    engine_conf -- "single-piston", "multi-piston", "turbo-integral"
                   "turbo-bladder"
    n_tank      -- (default 1) Number of fuel tanks;
    n_eng       -- (default 1) Number of engines.
    """

    if engine_conf == "single-piston":
        w_fs = 2 * qtot**0.667
    elif engine_conf == "multi-piston":
        w_fs = 4.5 * qtot**0.60
    elif engine_conf == "turbo-integral":
        w_fs = 80 * (n_eng + n_tank - 1) + 15 * n_tank**0.5 * qtot**0.333
    elif engine_conf == "turbo-bladder":
        w_fs = 3.2 * qtot**0.727

    return w_fs


## -------------------------11. Flight Control System-----------------------------
def fcs_weight(w0, ctrl_sys_type):
    """
    Estimate flight control system using Torrenbeek formula.

    Keyword Arguments:
    w0            -- Design gross weight (lbf);
    ctrl_sys_type -- "manual-single", "manual", "powered".
    """

    if ctrl_sys_type == "manual-single":
        w_ctrl = 0.23 * w0**0.667
    elif ctrl_sys_type == "manual":
        w_ctrl = 0.44 * w0**0.667
    elif ctrl_sys_type == "powered":
        w_ctrl = 0.64 * w0**0.667

    return w_ctrl


## -------------------------12. Hydraulic-----------------------------------------
def hydraulic_weight(w0):
    """
    Estimate hydraulic system.

    Torrenbeek doesn't provide estimation
    of hydraulic so here snorri's method used.

    The weight of the hydraulic systems for the flight controls
    is usually included in the Flight Control System,
    so the following expression is for the other components.

    Keyword Arguments:
    w0 -- Design gross weight (lbf);
    """

    w_hyd = 0.001 * w0

    return w_hyd


## -------------------------13. Avionics Systems----------------------------------
def avionics_weight(w_uav):
    """
    The expression below assumes analog dials and overpredicts
    the weight of modern electronic flight instrument
    system (EFIS).

    Torrenbeek doesn't provide estimation
    of hydraulic so here snorri's method used.

    Keyword Arguments:
    w_uav -- Weight of the uninstalled avionics in lbf
    """

    w_av = 2.11 * w_uav**0.933

    return w_uav


## -------------------------14. Electrical Systems--------------------------------
def electric_weight(w0, wu, whyd):
    """
    Comprises all electric wiring for lights, instruments,
    avionics, fuel system, climate control, and so forth.

    Using Torrenbeek.

    Keyword Arguments:
    w0   -- Design gross weight (lb);
    wu   -- Target useful load in lb;
    whyd -- Predicted weight of the hydraulics system in lb.
    """

    w_el = 0.0078 * (w0 - wu) ** 1.2 - whyd

    return w_el


## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
def aircond_weight(w0, n_occ, wav, mach):
    """
    Air conditioning includes both cooling and heating of
    the cabin volume. Pressurization system usually consists
    of various equipment (outflow and relief valves, pressure
    regulators, compressors, heat exchangers, and ducting).
    Antiicing systems included are either pneumatic inflat-
    able boots or bleed air heated elements.

    Torrenbeek doesn't provide estimation
    of hydraulic so here snorri's method used.

    Keyword Arguments:
    w0    -- Design gross weight (lbf);
    n_occ -- Number of occupants (crew and passengers);
    wav   -- Predicted weight of the avionics installation;
    mach  -- Mach number.
    """

    w_ac = 0.265 * w0**0.52 * n_occ**0.68 * wav**0.17 * mach**0.08

    return w_ac


## -------------------------16. Furnishing----------------------------------------
# None
def furn_weight():
    """
    Torrenbeek doesn't provide estimation
    of hydraulic so here snorri's method used.
    """
    return 0
