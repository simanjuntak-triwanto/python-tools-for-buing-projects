#!/usr/bin/env python

import numpy as np

# ----------------------------------------------#
# Example: Mriya AN-225 #
# ----------------------------------------------#

from uconverters import *


## -------------------------1. WING----------------------------------------------


# Area (ft^2)
SW = 724.4 * MSQ2FTSQ

# Weight of fuel in wing (lb)
WFW = 365000 * KG2LBM

# Wing Aspect Ratio
ARW = 8.6

# Wing sweep at 25% MGC (radian)
SWEEP4_WING = np.radians(0.4)  # radian

# Wing sweep at 50% MGC (radian)
SWEEP2_WING = np.radians(0.7)  # radian

# Dynamic pressure at cruise (lbf/ft2)
VC = 222.222 * MPS2FPS
RHO_FL32 = 0.026560637391482848
Q = 0.5 * RHO_FL32 * VC**2

# Wing Taper Ratio
TR_WING = 0.25

# Wing thickness-to-chord ratio (maximum)
T2C_WING = 0.2

# Design gross weight (lb)
W0 = 480000 * KG2LBM

# Limit Load Factor
LLF = 2.5
# Ultimate load factor (1.5 x limit load factor)
NZ = 1.5 * LLF

# Max thickness of the wing root chord (ft)
CR = 2.1 * M2FT  # ft
TW_MAX = CR * T2C_WING

# Wing type (only for Cessna Method): "cantilever" or  "strut-braced"
WING_TYPE = "cantilever"

# Wingspan (ft)
BW = 88.4 * M2FT  # np.sqrt(ARW * SW)

# Maximum level airspeed at S-L (knot/KEAS)
VH = 236.644 * MPS2KNOT  # KTAS

# For USAF
# Thickness to chord ratio
T2C_W = 0.25


## -------------------------2. Horizontal Tail-----------------------------------
# Horizontal Tail Area (ft^2)
SHT = 150 * MSQ2FTSQ

# HT Aspect Ratio
AR_HT = 6

# HT sweep at 25% MGC (radian)
SWEEP4_HT = np.radians(0.2)

# HT Taper ratio
TR_HT = 0.43

# Max root chord thickness of HT (ft)
T2C_HT = 0.1
CR_HT = 2.6 * M2FT
THT_MAX = T2C_HT * CR_HT

# For USAF
# Span of HT (ft)
BHT = 30 * M2FT
# Max root chord thickness of HT (ft)
THT_MAX = 7.5


# ## -------------------------3. Vertical Tail-----------------------------------
# Span of VT (ft)
BVT = 11 * M2FT

# FTail: 0 for conventional tail, 1 for T-tail;
F_TAIL = 0


# Vertical Tail Area (ft^2)
SVT = 57.75 * MSQ2FTSQ

# VT sweep at 25% MGC (radian)
SWEEP4_VT = np.radians(30)

# VT Taper ratio
TR_VT = 0.5

# VT Aspect Ratio
AR_VT = 2.1

# For USAF
# Max root chord thickness of HT (ft)
T2C_VT = 0.09
CR_VT = 5.642 * M2FT
TVT_MAX = T2C_VT * CR_VT

# # Max root chord thickness of VT (ft)
# TVT_MAX = 25 + 10 / 12


## -------------------------4. Fuselage------------------------------------------
# Fuselage wetted area in ft2
SFUS = 5437.71 * MSQ2FTSQ

# Horizontal tail arm, from wing c/4 to HT c/4 (ft)
LHT =  98 * M2FT

# Length of fuselage structure (forward bulkhead to aft frame) (ft)
LFS = 84 * M2FT

# Depth of fuselage structure (ft)
DFS = 11 * M2FT

# Fuselage Diameter
DFUS = 8 * M2FT

# Volume of pressurized cabin section (ft^3)
VP = 0.25 * np.pi * DFUS**2 * LFS

# Cabin pressure differential (psi)
DELTA_P = 8

RMAX = np.pi * DFUS

# For USAF
# Fuselage lenght; here we assume LF = LFS, they are usually different.
LF = LFS
# Fuselage width (ft)
WF = 8.5 * M2FT
# Fuselage max depth in ft
DF = DFS

## -------------------------5. Main Landing Gear---------------------------------
# Design landing weight in lb;
WL = W0

# Length of the main landing gear shock strut (ft);
# Shock strut length is the distance between the upper
# attachment point and the center of the wheel axis
LM = 1.5 * M2FT


# (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
NL = 4

# For Torrenbeek
# Wing position: "low" or "high"
WING_POS = "high"

# Landing gear type: "fixed" or "retract"
LG_TYPE = "fixed"

# Class of a/c: "bizjet" or "civil"
AC_CLASS = "civil"


# For USAF
DL = W0


## -------------------------6. Nose/Tail Landing Gear----------------------------
# Length of the nose landing gear shock strut (ft)
LN = 2.1  #!!!!!

## -------------------------7. Nacelle/Cowling Weight----------------------------
# For Cessna
# engine_type -- "rpe" (radial piston engine) or "hop" (horizontally opposed piston engine)
PISTON_ENGINE_TYPE = "rpe"

# Nacelle weight included in installed engine.
# For Torrenbeek
# "stp" (Single-engine tractor propeller), "multihop", "rp" (radial piston),
# "turboprop" (Multi-engine turboprop), "podjet" (Podded turbojet or-fan), "hbpr" (HBPR turbofan on a pylon)
NAC_ENGINE_TYPE = "hbpr"


## -------------------------8. Uninstalled (Dry) Engine--------------------------
# If prop engine, then Pmax (BHP), if jet then Tmax (lbf)
P_OR_T_MAX = 51500
ENGINE_TYPE = "jet"
# Ref value= 9184

## -------------------------9. Installed Engine----------------------------------
# Number of engines
N_ENG = 6  # 6 turbofan engines
# For Torrenbeek
WPROP = 0  #!!!!
TMAX = 51979  # in lb


# Weight of each uninstalled engine in lb
# W_ENG = 9184


## ------------------------10. Fuel System---------------------------------------
# For Cessna
FUEL_SYS_TYPE = "jeta-no-tip"
#

# Fuel quantity in integral tanks (US gallons)
# An integral fuel tank is defined as primary aircraft structure,
# usually wing or fuselage, that is sealed to contain fuel,
# as opposed to a rubberized fuel cell mounted in aircraft structure
QINT = 300000 * KG2GAL
# Total fuel quantity in (US gallons)
# QTOT = QINT + Additional Fuel Tank
QTOT = QINT

# Number of fuel tanks;
# e.g. wing, center, surge, and vent
N_TANK = 10

# For Torreenbeek
# "single-piston", "multi-piston", "turbo-integral", "turbo-bladder"
ENGINE_CONF = "turbo-integral"


## -------------------------11. Flight Control System-----------------------------
# No new input is needed
# For Torreenbeek
# ctrl_sys_type -- "manual-single", "manual", "powered".
CTRL_SYS_TYPE = "powered"

## -------------------------12. Hydraulic-----------------------------------------
# Air speed in subsonic regime: "low", "medium", "high", "light"
HYD_TYPE = "high"

# Mach number (design maximum)
MACH_MAX = 0.82


## -------------------------13. Avionics Systems----------------------------------
# Weight of the uninstalled avionics in lb (typically = 800-1400 lb);
# For smaller a/c for smaller aircraft to weigh 45 to 50 lb.
# This can be found from the avionics manufacturer data sheet.
W_UAV = 1400

## -------------------------14. Electrical Systems--------------------------------
# No new input is needed
# For Torrenbeek
# Target useful load (lb)
WU = 5000 * KG2LBM


## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
N_CABIN_CREW = 4
N_FLIGHT_CREW = 4
N_PAX = 330
N_OCC = N_CABIN_CREW + N_FLIGHT_CREW + N_PAX

# For USAF
N_CREW = N_CABIN_CREW + N_FLIGHT_CREW
## -------------------------16. Furnishing----------------------------------------
# No new input is needed
# For USAF
# Dynamic pressure at max level airspeed, lbf/ft2.
QH = Q
