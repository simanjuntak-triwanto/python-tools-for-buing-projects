#!/usr/bin/env python

# Statistical method for estimating aircraft weight components using
# formulas developed by USAF.
# See:

import numpy as np


## -------------------------1. WING----------------------------------------------
def wing_weight(nz, w0, arw, sweep4_wing, sw, tr_wing, t2c_w, vh):
    """
    Estimate wing weight using USAF formula;
    valid for vh <= 300 KTAS.

    Keyword Arguments:
    nz           -- Ultimate load factor (= 1.5 x limit load factor);
    w0           -- Design gross weight (lbf);
    arw          -- Aspect Ratio of wing;
    sweep4_wing  -- Wing sweep at 25% MGC;
    sw           -- Trapezoidal wing area in (ft2);
    tr_wing      -- Taper ratio of wing;
    t2c_w        -- Wing thickness-to-chord ratio (maximum);
    vh           -- Maximum level airspeed at S-L in KEAS;
    """

    if vh > 300:
        vh = 300
        # raise Exception("USAF formula nly valid for vh <= 300 KTAS")

    w_w = (
        96.948
        * (
            (nz * w0 / 1e5) ** 0.65
            * (arw / np.cos(sweep4_wing) ** 2) ** 0.57
            * (sw / 100) ** 0.61
            * ((1 + tr_wing) / (2 * t2c_w)) ** 0.36
            * np.sqrt(1 + vh / 500)
        )
        * 0.993
    )

    return w_w


## -------------------------2. Horizontal Tail-----------------------------------
def ht_weight(nz, w0, sht, lht, bht, tht_max):
    """
    Estimate the horizontal tail weight using USAF formula.

    Keyword Arguments:
    nz      -- Ultimate load factor (= 1.5 x limit load factor);
    w0      -- Design gross weight (lbf);
    sht     -- Trapezoidal HT area in ft2;
    lht     -- Horizontal tail arm, from wing c/4 to HT c/4 in ft;
    bht     -- HT span in ft;
    tht_max -- Max root chord thickness of HT in ft.
    """

    w_ht = (
        71.927
        * (
            (nz * w0 / 1e5) ** 0.87
            * (sht / 100) ** 1.2
            * (lht / 10) ** 0.483
            * np.sqrt(bht / tht_max)
        )
        * 0.458
    )

    return w_ht


## -------------------------3. Vertical Tail-------------------------------------
def vt_weight(f_tail, nz, w0, svt, bvt, tvt_max):
    """
    Estimate the horizontal tail weight using USAF formula.

    Keyword Arguments:
    f_tail  -- 0 for conventional tail, ¼1 for T-tail;
    nz      -- Ultimate load factor (= 1.5 x limit load factor);
    w0      -- Design gross weight (lbf);
    svt     -- Trapezoidal VT area in ft2;
    bvt     -- VT span in ft;
    tvt_max -- Max root chord thickness of VT in ft.
    """

    w_vt = (
        55.786
        * (1 + 0.2 * f_tail)
        * ((nz * w0 / 1e5) ** 0.87 * (svt / 100) * np.sqrt(bvt / tvt_max)) ** 0.458
    )

    return w_vt


## -------------------------4. Fuselage------------------------------------------
def fus_weight(nz, w0, lf, wf, df, vh):
    """
    Estimate fuselage weight using usaf formula.

    Keyword Arguments:
    nz -- Ultimate load factor (= 1.5 x limit load factor);
    w0 -- Design gross weight (lbf);
    lf -- Fuselage length in ft;
    wf -- Fuselage max width in ft;
    df -- Fuselage max depth in ft;
    vh -- Maximum level airspeed at S-L in KEAS.
    """

    w_fus = (
        200
        * (
            (nz * w0 / 1e5) ** 0.286
            * (lf / 10) ** 0.857
            * ((wf + df) / 10)
            * (vh / 100) ** 0.338
        )
        ** 1.1
    )

    return w_fus


## -------------------------5. Main Landing Gear---------------------------------
def mnlg_weight(wl, lm, nl=4.5):
    """
    Estimate main LG weight using usaf formula;
    valid for vh <= 200 KTAS.

    Keyword Arguments:
    wl -- Design landing weight in lbf;
    lm -- Length of the main landing gear shock strut in ft;
    nl -- (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
    """

    w_mnlg = 0.054 * (nl * wl) ** 0.684 * lm**0.501

    return w_mnlg


## -------------------------6. Nose/Tail Landing Gear----------------------------
## NONE
## Included in the main landing gear

## -------------------------7. Nacelle/Cowling Weight----------------------------
## NONE
## Included in the installed engine


## -------------------------8. Uninstalled (Dry) Engine--------------------------
def engine_dry_weight(p_or_t_max, engine_type):
    """
    Estimate uninstalled engine weight when the actual weight are not known.

    Keyword Arguments:
    p_or_t_max  -- If prop engine, then Pmax (BHP), if jet then Tmax (lbf)
    engine_type -- "piston", "prop", "jet"
    """

    if engine_type == "piston":
        w_eng = 50.56 + 1.352 * p_or_t_max
    elif engine_type == "prop":
        w_eng = 71.65 + 0.3658 * p_or_t_max
    elif engine_type == "jet":
        w_eng = 295.5 + 0.1683 * p_or_t_max

    return w_eng


## -------------------------9. Installed Engine----------------------------------
def engine_installed_weight(w_eng, n_eng):
    """
    Estimate installed engine weight using USAF formula.

    Keyword Arguments:
    w_eng -- Weight of each uninstalled engine in lbf;
    n_eng -- Number of engines;
    """

    w_ei = 2.575 * w_eng * 0.922 * n_eng

    return w_ei


## ------------------------10. Fuel System---------------------------------------
def fuel_sys_weight(qtot, qint, n_tank, n_eng):
    """
    Estimate fuel system weight using USAF formula.

    Keyword Arguments:
    qtot         -- Total fuel quantity in US gallons;
    qint         -- Fuel quantity in integral tanks in US gallons;
    n_tank       -- Number of fuel tanks;
    n_eng        -- Number of engines.
    """

    w_fs = 2.49 * (
        qtot**0.6 * (qtot / (qtot + qint)) ** 0.3 * n_tank**0.2 * n_eng**0.13
    )

    return w_fs


## -------------------------11. Flight Control System-----------------------------
def fcs_weight(w0, ctrl_sys_type):
    """
    Estimate flight control system using USAF formula.

    Keyword Arguments:
    w0            -- Design gross weight (lbf).
    ctrl_sys_type -- "manual", "powered".
    """
    if ctrl_sys_type == "manual":
        w_ctrl = 1.066 * w0**0.626
    elif ctrl_sys_type == "powered":
        w_ctrl = 1.08 * w0**0.7

    return w_ctrl


## -------------------------12. Hydraulic-----------------------------------------
def hydraulic_weight(w0):
    """
    Estimate hydraulic system.

    The weight of the hydraulic systems for the flight controls
    is usually included in the Flight Control System,
    so the following expression is for the other components.

    Keyword Arguments:
    w0 -- Design gross weight (lbf);
    """

    w_hyd = 0.001 * w0

    return w_hyd


## -------------------------13. Avionics Systems----------------------------------
def avionics_weight(w_uav):
    """
    The expression below assumes analog dials and overpredicts
    the weight of modern electronic flight instrument
    system (EFIS).

    Keyword Arguments:
    w_uav -- Weight of the uninstalled avionics in lbf
    """

    w_av = 2.11 * w_uav**0.933

    return w_uav


## -------------------------14. Electrical Systems--------------------------------
def electric_weight(wfs, wav):
    """
    Comprises all electric wiring for lights, instruments,
    avionics, fuel system, climate control, and so forth.

    Using Raymer/USAF formula.

    Keyword Arguments:
    wfs -- Predicted fuel system weight;
    wav -- Predicted weight of the avionics installation;
    """

    w_el = 12.57 * (wfs + wav) ** 0.51

    return w_el


## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
def aircond_weight(w0, n_occ, wav, mach):
    """
    Air conditioning includes both cooling and heating of
    the cabin volume. Pressurization system usually consists
    of various equipment (outflow and relief valves, pressure
    regulators, compressors, heat exchangers, and ducting).
    Antiicing systems included are either pneumatic inflat-
    able boots or bleed air heated elements.

    Keyword Arguments:
    w0    -- Design gross weight (lbf);
    n_occ -- Number of occupants (crew and passengers);
    wav   -- Predicted weight of the avionics installation;
    mach  -- Mach number.
    """

    w_ac = 0.265 * w0**0.52 * n_occ**0.68 * wav**0.17 * mach**0.08

    return w_ac


## -------------------------16. Furnishing----------------------------------------
def furn_weight(n_crew, qh):
    """
    Includes seats, insulation, sound proofing, lighting,
    galley, lavatory, overhead hat-racks, emergency equip-
    ment, and associated electric systems.

    Using usaf formula.

    Keyword Arguments:
    n_crew -- Number of crew;
    qh     -- Dynamic pressure at max level airspeed, lbf/ft2.
    """

    w_furn = 34.5 * n_crew * qh**0.25

    return w_furn
