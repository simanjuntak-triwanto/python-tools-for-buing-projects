#!/usr/bin/env python

# Statistical method for estimating aircraft weight components using
# formulas developed by Cessna.
# See:


import numpy as np


## -------------------------1. WING----------------------------------------------
def wing_weight(nz, w0, sw, arw, wing_type):
    """
    Estimate wing weight using cessna formula;
    valid only VH <= 200 KTAS (Maximum level airspeed at S-L in KEAS).

    Keyword Arguments:
    nz        -- Ultimate load factor (= 1.5 x limit load factor);
    w0        -- Design gross weight (lb);
    sw        -- Trapezoidal wing area in (ft2);
    arw       -- Aspect Ratio of wing;
    wing type -- "cantilever" or "strut-braced"
    """

    if wing_type == "cantilever":
        w_w = 0.04674 * (nz * w0) ** 0.397 * sw**0.360 * arw**1.712
    elif wing_type == "strut-braced":
        w_w = 0.002933 * nz**0.611 * sw**1.018 * arw**2.473

    return w_w


## -------------------------2. Horizontal Tail-----------------------------------
def ht_weight(w0, sht, arht, tht_max):
    """
    Estimate the horizontal tail weight using Cessna formula.;
    valid for vh <= 200 KTAS.

    Keyword Arguments:
    w0      -- Design gross weight (lbf);
    sht     -- Trapezoidal HT area in ft2;
    arht    -- Aspect Ratio of HT;
    tht_max -- Max root chord thickness of HT in ft;
    """

    w_ht = (
        3.184 * w0**0.887 * sht**0.101 * arht**0.138 / (174.04 * tht_max**0.223)
    )

    return w_ht


## -------------------------3. Vertical Tail-------------------------------------
def vt_weight(f_tail, w0, svt, arvt, tvt_max, sweep4_vt):
    """
    Estimate the horizontal tail weight using Cessna formula.

    Keyword Arguments:
    f_tail     -- 0 for conventional tail, ¼1 for T-tail;
    w0         -- Design gross weight (lbf);
    svt        -- Trapezoidal VT area in ft2;
    ar_vt      -- AR of VT;
    tvt_max    -- Max root chord thickness of VT in ft;
    vt_sweep_4 -- VT sweep at 25% MGC.
    """

    w_vt = (
        (1 + 0.2 * f_tail)
        * (1.68 * w0**0.567 * svt**0.1249 * arvt**0.482)
        / (639.95 * tvt_max**0.747 * np.cos(sweep4_vt) ** 0.882)
    )

    return w_vt


## -------------------------4. Fuselage------------------------------------------
def fus_weight(w0, rmax, lfs, wing_pos, nocc=1):
    """
    Estimate fuselage weight using cessna formula;
    valid for vh <= 200 KTAS.

    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    rmax     -- Fuselage maximum perimeter in ft;
    lfs      -- Length of fuselage structure (forward bulkhead to aft frame) in ft;
    wing_pos -- Wing position "low" or "high";
    nocc     -- (default 1 for UAV) Number of occupants (crew and passengers).
    """

    if wing_pos == "low":
        w_fus = 0.04682 * w0**0.692 * nocc**0.374 * lfs ** (0.590 / 100)
    elif wing_pos == "high":
        w_fus = (
            14.86 * w0**0.144 * (lfs / rmax) ** 0.778 * lfs**0.383 * nocc**0.455
        )

    return w_fus


## -------------------------5. Main Landing Gear---------------------------------
def mnlg_weight(w0, wl, lm, nz, wing_pos, nl=4.5):
    """
    Estimate main LG weight using usaf Cesna formula;
    valid for vh <= 200 KTAS.


    Keyword Arguments:
    w0       -- Design gross weight (lbf);
    wl       -- Design landing weight in lbf;
    lm       -- Length of the main landing gear shock strut in ft;
    nz       -- Ultimate load factor (= 1.5 x limit load factor);
    wing_pos -- "low" or "high";
    nl       -- (default 4.5) Ultimate landing load factor (typical range 3.5–5.5).
    """
    w_mnlg = (
        6.2
        + 0.0143 * w0
        + 0.362 * wl**0.417 * nl**0.950 * lm**0.183
        + 0.007157 * wl**0.749 * nz * lm * 0.788
    )

    return w_mnlg


## -------------------------6. Nose/Tail Landing Gear----------------------------
## NONE
# # Included in the main landing gear


## -------------------------7. Nacelle/Cowling Weight----------------------------
## NONE
def nac_weight(pmax, n_eng, piston_engine_type):
    """
    Estimate the weight of nacelles or cowlings using Cessna formula.

    Keyword Arguments:
    pmax        -- Maximum rated power per engine in BHP or ESHP
    n_eng       -- Number of engines
    engine_type -- "rpe" (radial piston engine) or
                   "hop" (horizontally opposed piston engine)
    """
    if piston_engine_type == "rpe":
        w_nac = 0.37 * pmax * n_eng
    elif piston_engine_type == "hop":
        w_nac = 0.24 * pmax * n_eng

    return w_nac


## -------------------------8. Uninstalled (Dry) Engine--------------------------
def engine_dry_weight(p_or_t_max, engine_type):
    """
    Estimate uninstalled engine weight when the actual weight are not known.

    Keyword Arguments:
    p_or_t_max  -- If prop engine, then Pmax (BHP), if jet then Tmax (lbf)
    engine_type -- "piston", "prop", "jet"
    """

    if engine_type == "piston":
        w_eng = 50.56 + 1.352 * p_or_t_max
    elif engine_type == "prop":
        w_eng = 71.65 + 0.3658 * p_or_t_max
    elif engine_type == "jet":
        w_eng = 295.5 + 0.1683 * p_or_t_max

    return w_eng


## -------------------------9. Installed Engine----------------------------------
def engine_installed_weight(pmax, wprop, n_eng, w_nac):
    """
    Estimate installed engine weight using Cessna formula.

    Keyword Arguments:
    pmax  -- Maximum rated power per engine in BHP;
    wprop -- Propeller weight (set wprop=0 for jet);;
    n_eng -- Number of engines;
    w_nac -- Predicted weight of all engine nacelles in lbf.
    """
    w_ei = (1.3 * pmax + wprop) * n_eng + w_nac

    return w_ei


## ------------------------10. Fuel System---------------------------------------
def fuel_sys_weight(qtot, fuel_sys_type):
    """
    Estimate installed fuel weight using Cessna formula.

    Keyword Arguments:
    qtot          -- Total fuel quantity in US gallons;
    fuel_sys_type -- "avgas-no-tip", "jeta-no-tip",
                     "avgas-tip", "jet-a-tip"
    """
    if fuel_sys_type == "avgas-no-tip":
        w_fs = 0.40 * qtot
    elif fuel_sys_type == "jeta-no-tip":
        w_fs = 0.4467 * qtot
    elif fuel_sys_type == "avgas-tip":
        w_fs = 0.70 * qtot
    elif fuel_sys_type == "jet-a-tip":
        w_fs = 0.7817 * qtot

    return w_fs


## -------------------------11. Flight Control System-----------------------------
def fcs_weight(w0):
    """
    Estimate flight control system using cessna formula
    (manual control system).

    Keyword Arguments:
    w0 -- Design gross weight (lbf);
    """
    w_ctrl = 0.0168 * w0

    return w_ctrl


## -------------------------12. Hydraulic-----------------------------------------
def hydraulic_weight(w0):
    """
    Estimate hydraulic system.

    The weight of the hydraulic systems for the flight controls
    is usually included in the Flight Control System,
    so the following expression is for the other components.

    Keyword Arguments:
    w0 -- Design gross weight (lbf);
    """

    w_hyd = 0.001 * w0

    return w_hyd


## -------------------------13. Avionics Systems----------------------------------
def avionics_weight(w_uav):
    """
    The expression below assumes analog dials and overpredicts
    the weight of modern electronic flight instrument
    system (EFIS).

    Keyword Arguments:
    w_uav -- Weight of the uninstalled avionics in lbf
    """

    w_av = 2.11 * w_uav**0.933

    return w_uav


## -------------------------14. Electrical Systems--------------------------------
def electric_weight(w0):
    """
    Comprises all electric wiring for lights, instruments,
    avionics, fuel system, climate control, and so forth.

    Using Cessna formula.

    Keyword Arguments:
    w0 -- Design gross weight (lbf);
    """

    w_el = 0.0268 * w0

    return w_el


## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
def aircond_weight(w0, n_occ, wav, mach):
    """
    Air conditioning includes both cooling and heating of
    the cabin volume. Pressurization system usually consists
    of various equipment (outflow and relief valves, pressure
    regulators, compressors, heat exchangers, and ducting).
    Antiicing systems included are either pneumatic inflat-
    able boots or bleed air heated elements.

    Keyword Arguments:
    w0    -- Design gross weight (lbf);
    n_occ -- Number of occupants (crew and passengers);
    wav   -- Predicted weight of the avionics installation;
    mach  -- Mach number.
    """

    w_ac = 0.265 * w0**0.52 * n_occ**0.68 * wav**0.17 * mach**0.08

    return w_ac


## -------------------------16. Furnishing----------------------------------------
def furn_weight(n_occ, w0):
    """
    Includes seats, insulation, sound proofing, lighting,
    galley, lavatory, overhead hat-racks, emergency equip-
    ment, and associated electric systems.

    Using Cessna formula.

    Keyword Arguments:
    n_occ -- Number of occupants (crew and passengers);
    w0    -- Design gross weight (lbf).
    """

    w_furn = 0.0412 * n_occ**1.145 * w0**0.489

    return w_furn
