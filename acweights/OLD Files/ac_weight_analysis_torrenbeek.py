#!/usr/bin/env python3

import wanalysis.torrenbeek as wtor

from ac1_data_torrenbeek import *

## -------------------------1. WING----------------------------------------------
wing_weight = wtor.wing_weight(W0, BW, SW, SWEEP2_WING, NZ, TW_MAX)

## -------------------------2. Empenage-----------------------------------
emp_weight = wtor.emp_weight(NZ, SHT, SVT)

## -------------------------3. Vertical Tail-------------------------------------
# Vertical Tail + Horizontal Tail = Empenage

## -------------------------4. Fuselage------------------------------------------

## -------------------------5. Main Landing Gear---------------------------------
mnlg_weight = wtor.mnlg_weight(W0, WING_POS, LG_TYPE, AC_CLASS)

## -------------------------6. Nose/Tail Landing Gear----------------------------
nlg_weight = wtor.nlg_weight(W0, WING_POS, LG_TYPE, AC_CLASS)

## -------------------------7. Nacelle/Cowling Weight----------------------------
nac_weight = wtor.nac_weight(P_OR_T_MAX, N_ENG, NAC_ENGINE_TYPE)

## -------------------------8. Uninstalled (Dry) Engine--------------------------
engine_dry_weight = wtor.engine_dry_weight(P_OR_T_MAX, ENGINE_TYPE)

## -------------------------9. Installed Engine----------------------------------
W_NAC = nac_weight
engine_installed_weight = wtor.engine_installed_weight(W_ENG, WPROP, N_ENG, PMAX, W_NAC)

## ------------------------10. Fuel System---------------------------------------
fuel_sys_weight = wtor.fuel_sys_weight(QTOT, ENGINE_CONF, N_TANK, N_ENG)

## -------------------------11. Flight Control System-----------------------------
fcs_weight = wtor.fcs_weight(W0, CTRL_SYS_TYPE)

## -------------------------12. Hydraulic-----------------------------------------
hydraulic_weight = wtor.hydraulic_weight(W0)

## -------------------------13. Avionics Systems----------------------------------
avionics_weight = wtor.avionics_weight(W_UAV)

## -------------------------14. Electrical Systems--------------------------------
WAV = avionics_weight
WHYD = hydraulic_weight
electric_weight = wtor.electric_weight(W0, WU, WHYD)

## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
aircond_weight = wtor.aircond_weight(W0, N_OCC, WAV, MACH_MAX)

## -------------------------16. Furnishing----------------------------------------
furn_weight = wtor.furn_weight()


print(
    int(wing_weight),
    int(emp_weight),
    # int(ht_weight),
    # int(vt_weight),
    # int(fus_weight),
    int(mnlg_weight),
    int(nlg_weight),
    int(nac_weight),
    int(engine_dry_weight),
    int(engine_installed_weight),
    int(fuel_sys_weight),
    int(fcs_weight),
    int(hydraulic_weight),
    int(avionics_weight),
    int(electric_weight),
    int(aircond_weight),
    int(furn_weight),
)
