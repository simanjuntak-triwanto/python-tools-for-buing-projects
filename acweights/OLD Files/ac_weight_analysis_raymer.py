#!/usr/bin/env python3

import wanalysis.raymer as wray
from ac1_data_torrenbeek import *

## -------------------------1. WING----------------------------------------------
wing_weight = wray.wing_weight(SW, WFW, ARW, SWEEP4_WING, Q, TR_WING, T2C_WING, NZ, W0)

## -------------------------2. Horizontal Tail-----------------------------------
ht_weight = wray.ht_weight(NZ, W0, Q, SHT, T2C_WING, ARW, SWEEP4_HT, SWEEP4_WING, TR_HT)

## -------------------------3. Vertical Tail-------------------------------------
vt_weight = wray.vt_weight(F_TAIL, NZ, W0, Q, SVT, T2C_WING, SWEEP4_VT, ARW, TR_VT)

## -------------------------4. Fuselage------------------------------------------
fus_weight = wray.fus_weight(SFUS, NZ, W0, LHT, LFS, DFS, Q, VP, DELTA_P)

## -------------------------5. Main Landing Gear---------------------------------
mnlg_weight = wray.mnlg_weight(WL, LM, NL)

## -------------------------6. Nose/Tail Landing Gear----------------------------
nlg_weight = wray.nlg_weight(WL, LN, nl=4.5)

## -------------------------7. Nacelle/Cowling Weight----------------------------
nac_weight = wray.nac_weight()

## -------------------------8. Uninstalled (Dry) Engine--------------------------
engine_dry_weight = wray.engine_dry_weight(P_OR_T_MAX, ENGINE_TYPE)

## -------------------------9. Installed Engine----------------------------------
engine_installed_weight = wray.engine_installed_weight(W_ENG, N_ENG)

## ------------------------10. Fuel System---------------------------------------
fuel_sys_weight = wray.fuel_sys_weight(QTOT, QINT, N_TANK, N_ENG)

## -------------------------11. Flight Control System-----------------------------
fcs_weight = wray.fcs_weight(LFS, BW, NZ, W0)

## -------------------------12. Hydraulic-----------------------------------------
hydraulic_weight = wray.hydraulic_weight(W0, HYD_TYPE, MACH_MAX)

## -------------------------13. Avionics Systems----------------------------------
avionics_weight = wray.avionics_weight(W_UAV)

## -------------------------14. Electrical Systems--------------------------------
WFS = fuel_sys_weight
WAV = avionics_weight
electric_weight = wray.electric_weight(WFS, WAV)

## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
aircond_weight = wray.aircond_weight(W0, N_OCC, WAV, MACH_MAX)

## -------------------------16. Furnishing----------------------------------------
furn_weight = wray.furn_weight(W0)


print(
    int(wing_weight),
    int(ht_weight),
    int(vt_weight),
    int(fus_weight),
    int(mnlg_weight),
    int(nlg_weight),
    int(nac_weight),
    int(engine_dry_weight),
    int(engine_installed_weight),
    int(fuel_sys_weight),
    int(fcs_weight),
    int(hydraulic_weight),
    int(avionics_weight),
    int(electric_weight),
    int(aircond_weight),
    int(furn_weight),
)
