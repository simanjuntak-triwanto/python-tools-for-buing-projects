#!/usr/bin/env python3

import numpy as np
import wanalysis.cessna as wcess
import wanalysis.raymer as wray
import wanalysis.torrenbeek as wtor
import wanalysis.usaf as wusaf

from badai50_data import *


def weight_components_cessna():
    """
    Estimate weight components using Cessna Method.
    """
    ## -------------------------1. WING----------------------------------------------
    wing_weight = wcess.wing_weight(NZ, W0, SW, ARW, WING_TYPE)

    ## -------------------------2. Horizontal Tail-----------------------------------
    ht_weight = wcess.ht_weight(W0, SHT, AR_HT, THT_MAX)

    # -------------------------3. Vertical Tail-------------------------------------
    vt_weight = wcess.vt_weight(F_TAIL, W0, SVT, AR_VT, TVT_MAX, SWEEP4_VT)

    ## -------------------------4. Fuselage------------------------------------------
    fus_weight = wcess.fus_weight(W0, RMAX, LFS, WING_POS, N_OCC)

    ## -------------------------5. Main Landing Gear---------------------------------
    mnlg_weight = wcess.mnlg_weight(W0, WL, LM, NZ, WING_POS, NL)

    # ## -------------------------6. Nose/Tail Landing Gear----------------------------
    # # Included in the main landing gear

    # ## -------------------------7. Nacelle/Cowling Weight----------------------------
    nac_weight = wcess.nac_weight(PMAX, N_ENG, PISTON_ENGINE_TYPE)

    ## -------------------------8. Uninstalled (Dry) Engine--------------------------
    engine_dry_weight = wcess.engine_dry_weight(P_OR_T_MAX, ENGINE_TYPE)

    # ## -------------------------9. Installed Engine----------------------------------
    W_NAC = nac_weight
    engine_installed_weight = wcess.engine_installed_weight(PMAX, WPROP, N_ENG, W_NAC)

    ## ------------------------10. Fuel System---------------------------------------
    fuel_sys_weight = wcess.fuel_sys_weight(QTOT, FUEL_SYS_TYPE)

    ## -------------------------11. Flight Control System-----------------------------
    fcs_weight = wcess.fcs_weight(W0)

    ## -------------------------12. Hydraulic-----------------------------------------
    hydraulic_weight = wcess.hydraulic_weight(W0)

    ## -------------------------13. Avionics Systems----------------------------------
    avionics_weight = wcess.avionics_weight(W_UAV)

    # ## -------------------------14. Electrical Systems--------------------------------
    WAV = avionics_weight
    electric_weight = wcess.electric_weight(W0)

    ## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
    aircond_weight = wcess.aircond_weight(W0, N_OCC, WAV, MACH_MAX)

    ## -------------------------16. Furnishing----------------------------------------
    furn_weight = wcess.furn_weight(N_OCC, W0)

    emp_weight = ht_weight + vt_weight
    nlg_weight = 0

    weight_components = np.array(
        [
            wing_weight,
            ht_weight,
            vt_weight,
            emp_weight,
            fus_weight,
            mnlg_weight,
            nlg_weight,
            nac_weight,
            engine_dry_weight,
            engine_installed_weight,
            fuel_sys_weight,
            fcs_weight,
            hydraulic_weight,
            avionics_weight,
            electric_weight,
            aircond_weight,
            furn_weight,
        ]
    )

    return weight_components


def weight_components_raymer():
    """
    Estimate weight components using Raymer Method.
    """
    ## -------------------------1. WING----------------------------------------------
    wing_weight = wray.wing_weight(
        SW, WFW, ARW, SWEEP4_WING, Q, TR_WING, T2C_WING, NZ, W0
    )

    ## -------------------------2. Horizontal Tail-----------------------------------
    ht_weight = wray.ht_weight(
        NZ, W0, Q, SHT, T2C_WING, ARW, SWEEP4_HT, SWEEP4_WING, TR_HT
    )

    ## -------------------------3. Vertical Tail-------------------------------------
    vt_weight = wray.vt_weight(F_TAIL, NZ, W0, Q, SVT, T2C_WING, SWEEP4_VT, ARW, TR_VT)

    ## -------------------------4. Fuselage------------------------------------------
    fus_weight = wray.fus_weight(SFUS, NZ, W0, LHT, LFS, DFS, Q, VP, DELTA_P)

    ## -------------------------5. Main Landing Gear---------------------------------
    mnlg_weight = wray.mnlg_weight(WL, LM, NL)

    ## -------------------------6. Nose/Tail Landing Gear----------------------------
    nlg_weight = wray.nlg_weight(WL, LN, nl=4.5)

    ## -------------------------7. Nacelle/Cowling Weight----------------------------
    nac_weight = wray.nac_weight()

    ## -------------------------8. Uninstalled (Dry) Engine--------------------------
    engine_dry_weight = wray.engine_dry_weight(P_OR_T_MAX, ENGINE_TYPE)

    W_ENG = engine_dry_weight
    ## -------------------------9. Installed Engine----------------------------------
    engine_installed_weight = wray.engine_installed_weight(W_ENG, N_ENG)

    ## ------------------------10. Fuel System---------------------------------------
    fuel_sys_weight = wray.fuel_sys_weight(QTOT, QINT, N_TANK, N_ENG)

    ## -------------------------11. Flight Control System-----------------------------
    fcs_weight = wray.fcs_weight(LFS, BW, NZ, W0)

    ## -------------------------12. Hydraulic-----------------------------------------
    hydraulic_weight = wray.hydraulic_weight(W0, HYD_TYPE, MACH_MAX)

    ## -------------------------13. Avionics Systems----------------------------------
    avionics_weight = wray.avionics_weight(W_UAV)

    ## -------------------------14. Electrical Systems--------------------------------
    WFS = fuel_sys_weight
    WAV = avionics_weight
    electric_weight = wray.electric_weight(WFS, WAV)

    ## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
    aircond_weight = wray.aircond_weight(W0, N_OCC, WAV, MACH_MAX)

    ## -------------------------16. Furnishing----------------------------------------
    furn_weight = wray.furn_weight(W0)

    emp_weight = ht_weight + vt_weight
    weight_components = np.array(
        [
            wing_weight,
            ht_weight,
            vt_weight,
            emp_weight,
            fus_weight,
            mnlg_weight,
            nlg_weight,
            nac_weight,
            engine_dry_weight,
            engine_installed_weight,
            fuel_sys_weight,
            fcs_weight,
            hydraulic_weight,
            avionics_weight,
            electric_weight,
            aircond_weight,
            furn_weight,
        ]
    )

    return weight_components


def weight_components_torrenbeek():
    ## -------------------------1. WING----------------------------------------------
    wing_weight = wtor.wing_weight(W0, BW, SW, SWEEP2_WING, NZ, TW_MAX)

    ## -------------------------2. Empenage-----------------------------------
    emp_weight = wtor.emp_weight(NZ, SHT, SVT)

    ## -------------------------3. Vertical Tail-------------------------------------
    # Vertical Tail + Horizontal Tail = Empenage

    ## -------------------------4. Fuselage------------------------------------------

    ## -------------------------5. Main Landing Gear---------------------------------
    mnlg_weight = wtor.mnlg_weight(W0, WING_POS, LG_TYPE, AC_CLASS)

    ## -------------------------6. Nose/Tail Landing Gear----------------------------
    nlg_weight = wtor.nlg_weight(W0, WING_POS, LG_TYPE, AC_CLASS)

    ## -------------------------7. Nacelle/Cowling Weight----------------------------
    nac_weight = wtor.nac_weight(P_OR_T_MAX, N_ENG, NAC_ENGINE_TYPE)

    ## -------------------------8. Uninstalled (Dry) Engine--------------------------
    engine_dry_weight = wtor.engine_dry_weight(P_OR_T_MAX, ENGINE_TYPE)

    W_ENG = engine_dry_weight
    ## -------------------------9. Installed Engine----------------------------------
    W_NAC = nac_weight
    engine_installed_weight = wtor.engine_installed_weight(
        W_ENG, WPROP, N_ENG, PMAX, W_NAC
    )

    ## ------------------------10. Fuel System---------------------------------------
    fuel_sys_weight = wtor.fuel_sys_weight(QTOT, ENGINE_CONF, N_TANK, N_ENG)

    ## -------------------------11. Flight Control System-----------------------------
    fcs_weight = wtor.fcs_weight(W0, CTRL_SYS_TYPE)

    ## -------------------------12. Hydraulic-----------------------------------------
    hydraulic_weight = wtor.hydraulic_weight(W0)

    ## -------------------------13. Avionics Systems----------------------------------
    avionics_weight = wtor.avionics_weight(W_UAV)

    ## -------------------------14. Electrical Systems--------------------------------
    WAV = avionics_weight
    WHYD = hydraulic_weight
    electric_weight = wtor.electric_weight(W0, WU, WHYD)

    ## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
    aircond_weight = wtor.aircond_weight(W0, N_OCC, WAV, MACH_MAX)

    ## -------------------------16. Furnishing----------------------------------------
    furn_weight = wtor.furn_weight()

    ht_weight = 0
    vt_weight = 0
    fus_weight = 0

    weight_components = np.array(
        [
            wing_weight,
            ht_weight,
            vt_weight,
            emp_weight,
            fus_weight,
            mnlg_weight,
            nlg_weight,
            nac_weight,
            engine_dry_weight,
            engine_installed_weight,
            fuel_sys_weight,
            fcs_weight,
            hydraulic_weight,
            avionics_weight,
            electric_weight,
            aircond_weight,
            furn_weight,
        ]
    )

    return weight_components


def weight_components_usaf():
    ## -------------------------1. WING----------------------------------------------
    wing_weight = wusaf.wing_weight(NZ, W0, ARW, SWEEP4_WING, SW, TR_WING, T2C_W, VH)

    ## -------------------------2. Horizontal Tail-----------------------------------
    ht_weight = wusaf.ht_weight(NZ, W0, SHT, LHT, BHT, THT_MAX)

    # -------------------------3. Vertical Tail-------------------------------------
    vt_weight = wusaf.vt_weight(F_TAIL, NZ, W0, SVT, BVT, TVT_MAX)

    ## -------------------------4. Fuselage------------------------------------------
    fus_weight = wusaf.fus_weight(NZ, W0, LF, WF, DF, VH)

    ## -------------------------5. Main Landing Gear---------------------------------
    mnlg_weight = wusaf.mnlg_weight(WL, LM, NL)

    ## -------------------------6. Nose/Tail Landing Gear----------------------------
    # Included in the main landing gear

    # ## -------------------------7. Nacelle/Cowling Weight----------------------------
    # Included in the installed engine

    ## -------------------------8. Uninstalled (Dry) Engine--------------------------
    engine_dry_weight = wusaf.engine_dry_weight(P_OR_T_MAX, ENGINE_TYPE)

    W_ENG = engine_dry_weight
    ## -------------------------9. Installed Engine----------------------------------
    # W_NAC = nac_weight
    engine_installed_weight = wusaf.engine_installed_weight(W_ENG, N_ENG)

    ## ------------------------10. Fuel System---------------------------------------
    fuel_sys_weight = wusaf.fuel_sys_weight(QTOT, QINT, N_TANK, N_ENG)

    ## -------------------------11. Flight Control System-----------------------------
    fcs_weight = wusaf.fcs_weight(W0, CTRL_SYS_TYPE)

    ## -------------------------12. Hydraulic-----------------------------------------
    hydraulic_weight = wusaf.hydraulic_weight(W0)

    ## -------------------------13. Avionics Systems----------------------------------
    avionics_weight = wusaf.avionics_weight(W_UAV)

    # ## -------------------------14. Electrical Systems--------------------------------
    WAV = avionics_weight
    WFS = fuel_sys_weight
    electric_weight = wusaf.electric_weight(WFS, WAV)

    ## ---------15. Air Conditioning, Pressurization, and Antiicing-------------------
    aircond_weight = wusaf.aircond_weight(W0, N_OCC, WAV, MACH_MAX)

    ## -------------------------16. Furnishing----------------------------------------
    furn_weight = wusaf.furn_weight(N_CREW, QH)

    emp_weight = ht_weight + vt_weight
    nlg_weight = 0
    nac_weight = 0
    weight_components = np.array(
        [
            wing_weight,
            ht_weight,
            vt_weight,
            emp_weight,
            fus_weight,
            mnlg_weight,
            nlg_weight,
            nac_weight,
            engine_dry_weight,
            engine_installed_weight,
            fuel_sys_weight,
            fcs_weight,
            hydraulic_weight,
            avionics_weight,
            electric_weight,
            aircond_weight,
            furn_weight,
        ]
    )
    return weight_components


# Estimating all weights
weights_cessna = weight_components_cessna()
weights_raymer = weight_components_raymer()
weights_torrenbeek = weight_components_torrenbeek()
weights_usaf = weight_components_usaf()

weights = np.concatenate((weights_raymer, weights_torrenbeek, weights_usaf)).reshape(
    17, -1, order="F"
)

np.savetxt("weights_summary.csv", weights, delimiter=",", fmt="%i")
